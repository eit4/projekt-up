<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="9.6.2">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="88" name="SimResults" color="9" fill="1" visible="yes" active="yes"/>
<layer number="89" name="SimProbes" color="9" fill="1" visible="yes" active="yes"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="200" name="200bmp" color="1" fill="10" visible="no" active="no"/>
<layer number="201" name="201bmp" color="1" fill="10" visible="yes" active="yes"/>
<layer number="202" name="202bmp" color="2" fill="10" visible="yes" active="yes"/>
<layer number="203" name="203bmp" color="3" fill="10" visible="yes" active="yes"/>
<layer number="250" name="Descript" color="3" fill="1" visible="no" active="no"/>
<layer number="251" name="SMDround" color="12" fill="11" visible="no" active="no"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="arduino">
<description>&lt;b&gt;Arduino Boards&lt;/b&gt;
&lt;ul&gt;
&lt;li&gt;MegaArduino
&lt;li&gt;Arduino Duemilanove (2009)
&lt;/ul&gt;
Based on the following sources:
&lt;br&gt;&lt;br&gt;
Schematic &amp; Reference Design from:
&lt;ul&gt;
&lt;li&gt; http://arduino.cc/en/Main/ArduinoBoardMega
&lt;li&gt;http://arduino.cc/en/Main/ArduinoBoardDuemilanove
&lt;/ul&gt;
Version: 1.0 / 30.08.2009&lt;br&gt;
&lt;author&gt;Created by atami@gmx.net&lt;/author&gt;</description>
<packages>
<package name="ARDUINO2009">
<wire x1="0" y1="0" x2="0" y2="53.34" width="0.127" layer="51"/>
<wire x1="0" y1="53.34" x2="64.516" y2="53.34" width="0.127" layer="51"/>
<wire x1="64.516" y1="53.34" x2="66.04" y2="51.816" width="0.127" layer="51"/>
<wire x1="66.04" y1="51.816" x2="66.04" y2="40.386" width="0.127" layer="51"/>
<wire x1="66.04" y1="40.386" x2="68.58" y2="37.846" width="0.127" layer="51"/>
<wire x1="68.58" y1="37.846" x2="68.58" y2="5.08" width="0.127" layer="51"/>
<wire x1="68.58" y1="5.08" x2="66.04" y2="2.54" width="0.127" layer="51"/>
<wire x1="66.04" y1="2.54" x2="66.04" y2="0" width="0.127" layer="51"/>
<wire x1="66.04" y1="0" x2="0" y2="0" width="0.127" layer="51"/>
<wire x1="38.481" y1="52.07" x2="39.751" y2="52.07" width="0.1524" layer="21"/>
<wire x1="39.751" y1="52.07" x2="40.386" y2="51.435" width="0.1524" layer="21"/>
<wire x1="40.386" y1="51.435" x2="40.386" y2="50.165" width="0.1524" layer="21"/>
<wire x1="40.386" y1="50.165" x2="39.751" y2="49.53" width="0.1524" layer="21"/>
<wire x1="35.306" y1="51.435" x2="35.941" y2="52.07" width="0.1524" layer="21"/>
<wire x1="35.941" y1="52.07" x2="37.211" y2="52.07" width="0.1524" layer="21"/>
<wire x1="37.211" y1="52.07" x2="37.846" y2="51.435" width="0.1524" layer="21"/>
<wire x1="37.846" y1="51.435" x2="37.846" y2="50.165" width="0.1524" layer="21"/>
<wire x1="37.846" y1="50.165" x2="37.211" y2="49.53" width="0.1524" layer="21"/>
<wire x1="37.211" y1="49.53" x2="35.941" y2="49.53" width="0.1524" layer="21"/>
<wire x1="35.941" y1="49.53" x2="35.306" y2="50.165" width="0.1524" layer="21"/>
<wire x1="38.481" y1="52.07" x2="37.846" y2="51.435" width="0.1524" layer="21"/>
<wire x1="37.846" y1="50.165" x2="38.481" y2="49.53" width="0.1524" layer="21"/>
<wire x1="39.751" y1="49.53" x2="38.481" y2="49.53" width="0.1524" layer="21"/>
<wire x1="30.861" y1="52.07" x2="32.131" y2="52.07" width="0.1524" layer="21"/>
<wire x1="32.131" y1="52.07" x2="32.766" y2="51.435" width="0.1524" layer="21"/>
<wire x1="32.766" y1="51.435" x2="32.766" y2="50.165" width="0.1524" layer="21"/>
<wire x1="32.766" y1="50.165" x2="32.131" y2="49.53" width="0.1524" layer="21"/>
<wire x1="32.766" y1="51.435" x2="33.401" y2="52.07" width="0.1524" layer="21"/>
<wire x1="33.401" y1="52.07" x2="34.671" y2="52.07" width="0.1524" layer="21"/>
<wire x1="34.671" y1="52.07" x2="35.306" y2="51.435" width="0.1524" layer="21"/>
<wire x1="35.306" y1="51.435" x2="35.306" y2="50.165" width="0.1524" layer="21"/>
<wire x1="35.306" y1="50.165" x2="34.671" y2="49.53" width="0.1524" layer="21"/>
<wire x1="34.671" y1="49.53" x2="33.401" y2="49.53" width="0.1524" layer="21"/>
<wire x1="33.401" y1="49.53" x2="32.766" y2="50.165" width="0.1524" layer="21"/>
<wire x1="27.686" y1="51.435" x2="28.321" y2="52.07" width="0.1524" layer="21"/>
<wire x1="28.321" y1="52.07" x2="29.591" y2="52.07" width="0.1524" layer="21"/>
<wire x1="29.591" y1="52.07" x2="30.226" y2="51.435" width="0.1524" layer="21"/>
<wire x1="30.226" y1="51.435" x2="30.226" y2="50.165" width="0.1524" layer="21"/>
<wire x1="30.226" y1="50.165" x2="29.591" y2="49.53" width="0.1524" layer="21"/>
<wire x1="29.591" y1="49.53" x2="28.321" y2="49.53" width="0.1524" layer="21"/>
<wire x1="28.321" y1="49.53" x2="27.686" y2="50.165" width="0.1524" layer="21"/>
<wire x1="30.861" y1="52.07" x2="30.226" y2="51.435" width="0.1524" layer="21"/>
<wire x1="30.226" y1="50.165" x2="30.861" y2="49.53" width="0.1524" layer="21"/>
<wire x1="32.131" y1="49.53" x2="30.861" y2="49.53" width="0.1524" layer="21"/>
<wire x1="23.241" y1="52.07" x2="24.511" y2="52.07" width="0.1524" layer="21"/>
<wire x1="24.511" y1="52.07" x2="25.146" y2="51.435" width="0.1524" layer="21"/>
<wire x1="25.146" y1="51.435" x2="25.146" y2="50.165" width="0.1524" layer="21"/>
<wire x1="25.146" y1="50.165" x2="24.511" y2="49.53" width="0.1524" layer="21"/>
<wire x1="25.146" y1="51.435" x2="25.781" y2="52.07" width="0.1524" layer="21"/>
<wire x1="25.781" y1="52.07" x2="27.051" y2="52.07" width="0.1524" layer="21"/>
<wire x1="27.051" y1="52.07" x2="27.686" y2="51.435" width="0.1524" layer="21"/>
<wire x1="27.686" y1="51.435" x2="27.686" y2="50.165" width="0.1524" layer="21"/>
<wire x1="27.686" y1="50.165" x2="27.051" y2="49.53" width="0.1524" layer="21"/>
<wire x1="27.051" y1="49.53" x2="25.781" y2="49.53" width="0.1524" layer="21"/>
<wire x1="25.781" y1="49.53" x2="25.146" y2="50.165" width="0.1524" layer="21"/>
<wire x1="22.606" y1="51.435" x2="22.606" y2="50.165" width="0.1524" layer="21"/>
<wire x1="23.241" y1="52.07" x2="22.606" y2="51.435" width="0.1524" layer="21"/>
<wire x1="22.606" y1="50.165" x2="23.241" y2="49.53" width="0.1524" layer="21"/>
<wire x1="24.511" y1="49.53" x2="23.241" y2="49.53" width="0.1524" layer="21"/>
<wire x1="41.021" y1="52.07" x2="42.291" y2="52.07" width="0.1524" layer="21"/>
<wire x1="42.291" y1="52.07" x2="42.926" y2="51.435" width="0.1524" layer="21"/>
<wire x1="42.926" y1="51.435" x2="42.926" y2="50.165" width="0.1524" layer="21"/>
<wire x1="42.926" y1="50.165" x2="42.291" y2="49.53" width="0.1524" layer="21"/>
<wire x1="41.021" y1="52.07" x2="40.386" y2="51.435" width="0.1524" layer="21"/>
<wire x1="40.386" y1="50.165" x2="41.021" y2="49.53" width="0.1524" layer="21"/>
<wire x1="42.291" y1="49.53" x2="41.021" y2="49.53" width="0.1524" layer="21"/>
<wire x1="60.325" y1="52.07" x2="61.595" y2="52.07" width="0.1524" layer="21"/>
<wire x1="61.595" y1="52.07" x2="62.23" y2="51.435" width="0.1524" layer="21"/>
<wire x1="62.23" y1="51.435" x2="62.23" y2="50.165" width="0.1524" layer="21"/>
<wire x1="62.23" y1="50.165" x2="61.595" y2="49.53" width="0.1524" layer="21"/>
<wire x1="57.15" y1="51.435" x2="57.785" y2="52.07" width="0.1524" layer="21"/>
<wire x1="57.785" y1="52.07" x2="59.055" y2="52.07" width="0.1524" layer="21"/>
<wire x1="59.055" y1="52.07" x2="59.69" y2="51.435" width="0.1524" layer="21"/>
<wire x1="59.69" y1="51.435" x2="59.69" y2="50.165" width="0.1524" layer="21"/>
<wire x1="59.69" y1="50.165" x2="59.055" y2="49.53" width="0.1524" layer="21"/>
<wire x1="59.055" y1="49.53" x2="57.785" y2="49.53" width="0.1524" layer="21"/>
<wire x1="57.785" y1="49.53" x2="57.15" y2="50.165" width="0.1524" layer="21"/>
<wire x1="60.325" y1="52.07" x2="59.69" y2="51.435" width="0.1524" layer="21"/>
<wire x1="59.69" y1="50.165" x2="60.325" y2="49.53" width="0.1524" layer="21"/>
<wire x1="61.595" y1="49.53" x2="60.325" y2="49.53" width="0.1524" layer="21"/>
<wire x1="52.705" y1="52.07" x2="53.975" y2="52.07" width="0.1524" layer="21"/>
<wire x1="53.975" y1="52.07" x2="54.61" y2="51.435" width="0.1524" layer="21"/>
<wire x1="54.61" y1="51.435" x2="54.61" y2="50.165" width="0.1524" layer="21"/>
<wire x1="54.61" y1="50.165" x2="53.975" y2="49.53" width="0.1524" layer="21"/>
<wire x1="54.61" y1="51.435" x2="55.245" y2="52.07" width="0.1524" layer="21"/>
<wire x1="55.245" y1="52.07" x2="56.515" y2="52.07" width="0.1524" layer="21"/>
<wire x1="56.515" y1="52.07" x2="57.15" y2="51.435" width="0.1524" layer="21"/>
<wire x1="57.15" y1="51.435" x2="57.15" y2="50.165" width="0.1524" layer="21"/>
<wire x1="57.15" y1="50.165" x2="56.515" y2="49.53" width="0.1524" layer="21"/>
<wire x1="56.515" y1="49.53" x2="55.245" y2="49.53" width="0.1524" layer="21"/>
<wire x1="55.245" y1="49.53" x2="54.61" y2="50.165" width="0.1524" layer="21"/>
<wire x1="49.53" y1="51.435" x2="50.165" y2="52.07" width="0.1524" layer="21"/>
<wire x1="50.165" y1="52.07" x2="51.435" y2="52.07" width="0.1524" layer="21"/>
<wire x1="51.435" y1="52.07" x2="52.07" y2="51.435" width="0.1524" layer="21"/>
<wire x1="52.07" y1="51.435" x2="52.07" y2="50.165" width="0.1524" layer="21"/>
<wire x1="52.07" y1="50.165" x2="51.435" y2="49.53" width="0.1524" layer="21"/>
<wire x1="51.435" y1="49.53" x2="50.165" y2="49.53" width="0.1524" layer="21"/>
<wire x1="50.165" y1="49.53" x2="49.53" y2="50.165" width="0.1524" layer="21"/>
<wire x1="52.705" y1="52.07" x2="52.07" y2="51.435" width="0.1524" layer="21"/>
<wire x1="52.07" y1="50.165" x2="52.705" y2="49.53" width="0.1524" layer="21"/>
<wire x1="53.975" y1="49.53" x2="52.705" y2="49.53" width="0.1524" layer="21"/>
<wire x1="45.085" y1="52.07" x2="46.355" y2="52.07" width="0.1524" layer="21"/>
<wire x1="46.355" y1="52.07" x2="46.99" y2="51.435" width="0.1524" layer="21"/>
<wire x1="46.99" y1="51.435" x2="46.99" y2="50.165" width="0.1524" layer="21"/>
<wire x1="46.99" y1="50.165" x2="46.355" y2="49.53" width="0.1524" layer="21"/>
<wire x1="46.99" y1="51.435" x2="47.625" y2="52.07" width="0.1524" layer="21"/>
<wire x1="47.625" y1="52.07" x2="48.895" y2="52.07" width="0.1524" layer="21"/>
<wire x1="48.895" y1="52.07" x2="49.53" y2="51.435" width="0.1524" layer="21"/>
<wire x1="49.53" y1="51.435" x2="49.53" y2="50.165" width="0.1524" layer="21"/>
<wire x1="49.53" y1="50.165" x2="48.895" y2="49.53" width="0.1524" layer="21"/>
<wire x1="48.895" y1="49.53" x2="47.625" y2="49.53" width="0.1524" layer="21"/>
<wire x1="47.625" y1="49.53" x2="46.99" y2="50.165" width="0.1524" layer="21"/>
<wire x1="44.45" y1="51.435" x2="44.45" y2="50.165" width="0.1524" layer="21"/>
<wire x1="45.085" y1="52.07" x2="44.45" y2="51.435" width="0.1524" layer="21"/>
<wire x1="44.45" y1="50.165" x2="45.085" y2="49.53" width="0.1524" layer="21"/>
<wire x1="46.355" y1="49.53" x2="45.085" y2="49.53" width="0.1524" layer="21"/>
<wire x1="62.865" y1="52.07" x2="64.135" y2="52.07" width="0.1524" layer="21"/>
<wire x1="64.135" y1="52.07" x2="64.77" y2="51.435" width="0.1524" layer="21"/>
<wire x1="64.77" y1="51.435" x2="64.77" y2="50.165" width="0.1524" layer="21"/>
<wire x1="64.77" y1="50.165" x2="64.135" y2="49.53" width="0.1524" layer="21"/>
<wire x1="62.865" y1="52.07" x2="62.23" y2="51.435" width="0.1524" layer="21"/>
<wire x1="62.23" y1="50.165" x2="62.865" y2="49.53" width="0.1524" layer="21"/>
<wire x1="64.135" y1="49.53" x2="62.865" y2="49.53" width="0.1524" layer="21"/>
<wire x1="45.085" y1="3.81" x2="46.355" y2="3.81" width="0.1524" layer="21"/>
<wire x1="46.355" y1="3.81" x2="46.99" y2="3.175" width="0.1524" layer="21"/>
<wire x1="46.99" y1="1.905" x2="46.355" y2="1.27" width="0.1524" layer="21"/>
<wire x1="41.91" y1="3.175" x2="42.545" y2="3.81" width="0.1524" layer="21"/>
<wire x1="42.545" y1="3.81" x2="43.815" y2="3.81" width="0.1524" layer="21"/>
<wire x1="43.815" y1="3.81" x2="44.45" y2="3.175" width="0.1524" layer="21"/>
<wire x1="44.45" y1="3.175" x2="44.45" y2="1.905" width="0.1524" layer="21"/>
<wire x1="44.45" y1="1.905" x2="43.815" y2="1.27" width="0.1524" layer="21"/>
<wire x1="43.815" y1="1.27" x2="42.545" y2="1.27" width="0.1524" layer="21"/>
<wire x1="42.545" y1="1.27" x2="41.91" y2="1.905" width="0.1524" layer="21"/>
<wire x1="45.085" y1="3.81" x2="44.45" y2="3.175" width="0.1524" layer="21"/>
<wire x1="44.45" y1="1.905" x2="45.085" y2="1.27" width="0.1524" layer="21"/>
<wire x1="46.355" y1="1.27" x2="45.085" y2="1.27" width="0.1524" layer="21"/>
<wire x1="37.465" y1="3.81" x2="38.735" y2="3.81" width="0.1524" layer="21"/>
<wire x1="38.735" y1="3.81" x2="39.37" y2="3.175" width="0.1524" layer="21"/>
<wire x1="39.37" y1="3.175" x2="39.37" y2="1.905" width="0.1524" layer="21"/>
<wire x1="39.37" y1="1.905" x2="38.735" y2="1.27" width="0.1524" layer="21"/>
<wire x1="39.37" y1="3.175" x2="40.005" y2="3.81" width="0.1524" layer="21"/>
<wire x1="40.005" y1="3.81" x2="41.275" y2="3.81" width="0.1524" layer="21"/>
<wire x1="41.275" y1="3.81" x2="41.91" y2="3.175" width="0.1524" layer="21"/>
<wire x1="41.91" y1="3.175" x2="41.91" y2="1.905" width="0.1524" layer="21"/>
<wire x1="41.91" y1="1.905" x2="41.275" y2="1.27" width="0.1524" layer="21"/>
<wire x1="41.275" y1="1.27" x2="40.005" y2="1.27" width="0.1524" layer="21"/>
<wire x1="40.005" y1="1.27" x2="39.37" y2="1.905" width="0.1524" layer="21"/>
<wire x1="34.29" y1="3.175" x2="34.925" y2="3.81" width="0.1524" layer="21"/>
<wire x1="34.925" y1="3.81" x2="36.195" y2="3.81" width="0.1524" layer="21"/>
<wire x1="36.195" y1="3.81" x2="36.83" y2="3.175" width="0.1524" layer="21"/>
<wire x1="36.83" y1="3.175" x2="36.83" y2="1.905" width="0.1524" layer="21"/>
<wire x1="36.83" y1="1.905" x2="36.195" y2="1.27" width="0.1524" layer="21"/>
<wire x1="36.195" y1="1.27" x2="34.925" y2="1.27" width="0.1524" layer="21"/>
<wire x1="34.925" y1="1.27" x2="34.29" y2="1.905" width="0.1524" layer="21"/>
<wire x1="37.465" y1="3.81" x2="36.83" y2="3.175" width="0.1524" layer="21"/>
<wire x1="36.83" y1="1.905" x2="37.465" y2="1.27" width="0.1524" layer="21"/>
<wire x1="38.735" y1="1.27" x2="37.465" y2="1.27" width="0.1524" layer="21"/>
<wire x1="31.75" y1="3.175" x2="31.75" y2="1.905" width="0.1524" layer="21"/>
<wire x1="31.75" y1="3.175" x2="32.385" y2="3.81" width="0.1524" layer="21"/>
<wire x1="32.385" y1="3.81" x2="33.655" y2="3.81" width="0.1524" layer="21"/>
<wire x1="33.655" y1="3.81" x2="34.29" y2="3.175" width="0.1524" layer="21"/>
<wire x1="34.29" y1="3.175" x2="34.29" y2="1.905" width="0.1524" layer="21"/>
<wire x1="34.29" y1="1.905" x2="33.655" y2="1.27" width="0.1524" layer="21"/>
<wire x1="33.655" y1="1.27" x2="32.385" y2="1.27" width="0.1524" layer="21"/>
<wire x1="32.385" y1="1.27" x2="31.75" y2="1.905" width="0.1524" layer="21"/>
<wire x1="62.865" y1="3.81" x2="64.135" y2="3.81" width="0.1524" layer="21"/>
<wire x1="64.135" y1="3.81" x2="64.77" y2="3.175" width="0.1524" layer="21"/>
<wire x1="64.77" y1="3.175" x2="64.77" y2="1.905" width="0.1524" layer="21"/>
<wire x1="64.77" y1="1.905" x2="64.135" y2="1.27" width="0.1524" layer="21"/>
<wire x1="59.69" y1="3.175" x2="60.325" y2="3.81" width="0.1524" layer="21"/>
<wire x1="60.325" y1="3.81" x2="61.595" y2="3.81" width="0.1524" layer="21"/>
<wire x1="61.595" y1="3.81" x2="62.23" y2="3.175" width="0.1524" layer="21"/>
<wire x1="62.23" y1="3.175" x2="62.23" y2="1.905" width="0.1524" layer="21"/>
<wire x1="62.23" y1="1.905" x2="61.595" y2="1.27" width="0.1524" layer="21"/>
<wire x1="61.595" y1="1.27" x2="60.325" y2="1.27" width="0.1524" layer="21"/>
<wire x1="60.325" y1="1.27" x2="59.69" y2="1.905" width="0.1524" layer="21"/>
<wire x1="62.865" y1="3.81" x2="62.23" y2="3.175" width="0.1524" layer="21"/>
<wire x1="62.23" y1="1.905" x2="62.865" y2="1.27" width="0.1524" layer="21"/>
<wire x1="64.135" y1="1.27" x2="62.865" y2="1.27" width="0.1524" layer="21"/>
<wire x1="55.245" y1="3.81" x2="56.515" y2="3.81" width="0.1524" layer="21"/>
<wire x1="56.515" y1="3.81" x2="57.15" y2="3.175" width="0.1524" layer="21"/>
<wire x1="57.15" y1="3.175" x2="57.15" y2="1.905" width="0.1524" layer="21"/>
<wire x1="57.15" y1="1.905" x2="56.515" y2="1.27" width="0.1524" layer="21"/>
<wire x1="57.15" y1="3.175" x2="57.785" y2="3.81" width="0.1524" layer="21"/>
<wire x1="57.785" y1="3.81" x2="59.055" y2="3.81" width="0.1524" layer="21"/>
<wire x1="59.055" y1="3.81" x2="59.69" y2="3.175" width="0.1524" layer="21"/>
<wire x1="59.69" y1="3.175" x2="59.69" y2="1.905" width="0.1524" layer="21"/>
<wire x1="59.69" y1="1.905" x2="59.055" y2="1.27" width="0.1524" layer="21"/>
<wire x1="59.055" y1="1.27" x2="57.785" y2="1.27" width="0.1524" layer="21"/>
<wire x1="57.785" y1="1.27" x2="57.15" y2="1.905" width="0.1524" layer="21"/>
<wire x1="52.07" y1="3.175" x2="52.705" y2="3.81" width="0.1524" layer="21"/>
<wire x1="52.705" y1="3.81" x2="53.975" y2="3.81" width="0.1524" layer="21"/>
<wire x1="53.975" y1="3.81" x2="54.61" y2="3.175" width="0.1524" layer="21"/>
<wire x1="54.61" y1="3.175" x2="54.61" y2="1.905" width="0.1524" layer="21"/>
<wire x1="54.61" y1="1.905" x2="53.975" y2="1.27" width="0.1524" layer="21"/>
<wire x1="53.975" y1="1.27" x2="52.705" y2="1.27" width="0.1524" layer="21"/>
<wire x1="52.705" y1="1.27" x2="52.07" y2="1.905" width="0.1524" layer="21"/>
<wire x1="55.245" y1="3.81" x2="54.61" y2="3.175" width="0.1524" layer="21"/>
<wire x1="54.61" y1="1.905" x2="55.245" y2="1.27" width="0.1524" layer="21"/>
<wire x1="56.515" y1="1.27" x2="55.245" y2="1.27" width="0.1524" layer="21"/>
<wire x1="49.53" y1="3.175" x2="49.53" y2="1.905" width="0.1524" layer="21"/>
<wire x1="49.53" y1="3.175" x2="50.165" y2="3.81" width="0.1524" layer="21"/>
<wire x1="50.165" y1="3.81" x2="51.435" y2="3.81" width="0.1524" layer="21"/>
<wire x1="51.435" y1="3.81" x2="52.07" y2="3.175" width="0.1524" layer="21"/>
<wire x1="52.07" y1="3.175" x2="52.07" y2="1.905" width="0.1524" layer="21"/>
<wire x1="52.07" y1="1.905" x2="51.435" y2="1.27" width="0.1524" layer="21"/>
<wire x1="51.435" y1="1.27" x2="50.165" y2="1.27" width="0.1524" layer="21"/>
<wire x1="50.165" y1="1.27" x2="49.53" y2="1.905" width="0.1524" layer="21"/>
<wire x1="46.99" y1="3.175" x2="46.99" y2="1.905" width="0.1524" layer="21"/>
<pad name="28" x="23.876" y="50.8" drill="1.016" shape="long" rot="R90"/>
<pad name="27" x="26.416" y="50.8" drill="1.016" shape="long" rot="R90"/>
<pad name="26" x="28.956" y="50.8" drill="1.016" shape="long" rot="R90"/>
<pad name="25" x="31.496" y="50.8" drill="1.016" shape="long" rot="R90"/>
<pad name="24" x="34.036" y="50.8" drill="1.016" shape="long" rot="R90"/>
<pad name="23" x="36.576" y="50.8" drill="1.016" shape="long" rot="R90"/>
<pad name="22" x="39.116" y="50.8" drill="1.016" shape="long" rot="R90"/>
<pad name="21" x="41.656" y="50.8" drill="1.016" shape="long" rot="R90"/>
<pad name="20" x="45.72" y="50.8" drill="1.016" shape="long" rot="R90"/>
<pad name="19" x="48.26" y="50.8" drill="1.016" shape="long" rot="R90"/>
<pad name="18" x="50.8" y="50.8" drill="1.016" shape="long" rot="R90"/>
<pad name="17" x="53.34" y="50.8" drill="1.016" shape="long" rot="R90"/>
<pad name="16" x="55.88" y="50.8" drill="1.016" shape="long" rot="R90"/>
<pad name="15" x="58.42" y="50.8" drill="1.016" shape="long" rot="R90"/>
<pad name="14" x="60.96" y="50.8" drill="1.016" shape="long" rot="R90"/>
<pad name="13" x="63.5" y="50.8" drill="1.016" shape="long" rot="R90"/>
<pad name="1" x="33.02" y="2.54" drill="1.016" shape="long" rot="R90"/>
<pad name="2" x="35.56" y="2.54" drill="1.016" shape="long" rot="R90"/>
<pad name="3" x="38.1" y="2.54" drill="1.016" shape="long" rot="R90"/>
<pad name="4" x="40.64" y="2.54" drill="1.016" shape="long" rot="R90"/>
<pad name="5" x="43.18" y="2.54" drill="1.016" shape="long" rot="R90"/>
<pad name="6" x="45.72" y="2.54" drill="1.016" shape="long" rot="R90"/>
<pad name="7" x="50.8" y="2.54" drill="1.016" shape="long" rot="R90"/>
<pad name="8" x="53.34" y="2.54" drill="1.016" shape="long" rot="R90"/>
<pad name="9" x="55.88" y="2.54" drill="1.016" shape="long" rot="R90"/>
<pad name="10" x="58.42" y="2.54" drill="1.016" shape="long" rot="R90"/>
<pad name="11" x="60.96" y="2.54" drill="1.016" shape="long" rot="R90"/>
<pad name="12" x="63.5" y="2.54" drill="1.016" shape="long" rot="R90"/>
<text x="23.1184" y="48.7812" size="1.016" layer="48" font="vector" ratio="10" rot="R270">AREF</text>
<text x="25.654" y="48.768" size="1.016" layer="48" font="vector" ratio="10" rot="R270">GND</text>
<text x="28.194" y="48.768" size="1.016" layer="48" font="vector" ratio="10" rot="R270">13</text>
<text x="30.734" y="48.768" size="1.016" layer="48" font="vector" ratio="10" rot="R270">12</text>
<text x="33.274" y="48.768" size="1.016" layer="48" font="vector" ratio="10" rot="R270">11</text>
<text x="35.814" y="48.768" size="1.016" layer="48" font="vector" ratio="10" rot="R270">10</text>
<text x="38.354" y="48.768" size="1.016" layer="48" font="vector" ratio="10" rot="R270">9</text>
<text x="40.894" y="48.768" size="1.016" layer="48" font="vector" ratio="10" rot="R270">8</text>
<text x="44.958" y="48.768" size="1.016" layer="48" font="vector" ratio="10" rot="R270">7</text>
<text x="47.498" y="48.768" size="1.016" layer="48" font="vector" ratio="10" rot="R270">6</text>
<text x="50.038" y="48.768" size="1.016" layer="48" font="vector" ratio="10" rot="R270">5</text>
<text x="52.578" y="48.768" size="1.016" layer="48" font="vector" ratio="10" rot="R270">4</text>
<text x="55.118" y="48.768" size="1.016" layer="48" font="vector" ratio="10" rot="R270">3</text>
<text x="57.658" y="48.768" size="1.016" layer="48" font="vector" ratio="10" rot="R270">2</text>
<text x="60.198" y="48.768" size="1.016" layer="48" font="vector" ratio="10" rot="R270">RxD 1</text>
<text x="62.738" y="48.768" size="1.016" layer="48" font="vector" ratio="10" rot="R270">TxD 0</text>
<text x="33.7776" y="4.8128" size="1.016" layer="48" font="vector" ratio="10" rot="R90">!Reset</text>
<text x="36.3176" y="4.8128" size="1.016" layer="48" font="vector" ratio="10" rot="R90">+3V3</text>
<text x="38.8576" y="4.8128" size="1.016" layer="48" font="vector" ratio="10" rot="R90">+5V</text>
<text x="41.3976" y="4.8128" size="1.016" layer="48" font="vector" ratio="10" rot="R90">GND</text>
<text x="43.9376" y="4.8128" size="1.016" layer="48" font="vector" ratio="10" rot="R90">GND</text>
<text x="46.4776" y="4.8128" size="1.016" layer="48" font="vector" ratio="10" rot="R90">+Vin</text>
<text x="51.5576" y="4.8128" size="1.016" layer="48" font="vector" ratio="10" rot="R90">A0</text>
<text x="54.0976" y="4.8128" size="1.016" layer="48" font="vector" ratio="10" rot="R90">A1</text>
<text x="56.6376" y="4.8128" size="1.016" layer="48" font="vector" ratio="10" rot="R90">A2</text>
<text x="59.1776" y="4.8128" size="1.016" layer="48" font="vector" ratio="10" rot="R90">A3</text>
<text x="61.7176" y="4.8128" size="1.016" layer="48" font="vector" ratio="10" rot="R90">A4</text>
<text x="64.2576" y="4.8128" size="1.016" layer="48" font="vector" ratio="10" rot="R90">A5</text>
<rectangle x1="38.862" y1="50.546" x2="39.37" y2="51.054" layer="51"/>
<rectangle x1="36.322" y1="50.546" x2="36.83" y2="51.054" layer="51"/>
<rectangle x1="33.782" y1="50.546" x2="34.29" y2="51.054" layer="51"/>
<rectangle x1="31.242" y1="50.546" x2="31.75" y2="51.054" layer="51"/>
<rectangle x1="28.702" y1="50.546" x2="29.21" y2="51.054" layer="51"/>
<rectangle x1="26.162" y1="50.546" x2="26.67" y2="51.054" layer="51"/>
<rectangle x1="23.622" y1="50.546" x2="24.13" y2="51.054" layer="51"/>
<rectangle x1="41.402" y1="50.546" x2="41.91" y2="51.054" layer="51"/>
<rectangle x1="60.706" y1="50.546" x2="61.214" y2="51.054" layer="51"/>
<rectangle x1="58.166" y1="50.546" x2="58.674" y2="51.054" layer="51"/>
<rectangle x1="55.626" y1="50.546" x2="56.134" y2="51.054" layer="51"/>
<rectangle x1="53.086" y1="50.546" x2="53.594" y2="51.054" layer="51"/>
<rectangle x1="50.546" y1="50.546" x2="51.054" y2="51.054" layer="51"/>
<rectangle x1="48.006" y1="50.546" x2="48.514" y2="51.054" layer="51"/>
<rectangle x1="45.466" y1="50.546" x2="45.974" y2="51.054" layer="51"/>
<rectangle x1="63.246" y1="50.546" x2="63.754" y2="51.054" layer="51"/>
<rectangle x1="45.466" y1="2.286" x2="45.974" y2="2.794" layer="51"/>
<rectangle x1="42.926" y1="2.286" x2="43.434" y2="2.794" layer="51"/>
<rectangle x1="40.386" y1="2.286" x2="40.894" y2="2.794" layer="51"/>
<rectangle x1="37.846" y1="2.286" x2="38.354" y2="2.794" layer="51"/>
<rectangle x1="35.306" y1="2.286" x2="35.814" y2="2.794" layer="51"/>
<rectangle x1="32.766" y1="2.286" x2="33.274" y2="2.794" layer="51"/>
<rectangle x1="63.246" y1="2.286" x2="63.754" y2="2.794" layer="51"/>
<rectangle x1="60.706" y1="2.286" x2="61.214" y2="2.794" layer="51"/>
<rectangle x1="58.166" y1="2.286" x2="58.674" y2="2.794" layer="51"/>
<rectangle x1="55.626" y1="2.286" x2="56.134" y2="2.794" layer="51"/>
<rectangle x1="53.086" y1="2.286" x2="53.594" y2="2.794" layer="51"/>
<rectangle x1="50.546" y1="2.286" x2="51.054" y2="2.794" layer="51"/>
<hole x="15.24" y="50.8" drill="3.2"/>
<hole x="66.04" y="35.56" drill="3.2"/>
<hole x="66.04" y="7.62" drill="3.2"/>
<hole x="55.1696" y="27.1138" drill="2"/>
</package>
<package name="ARDUINO2009_1">
<wire x1="0" y1="0" x2="0" y2="53.34" width="0.127" layer="51"/>
<wire x1="0" y1="53.34" x2="64.516" y2="53.34" width="0.127" layer="51"/>
<wire x1="64.516" y1="53.34" x2="66.04" y2="51.816" width="0.127" layer="51"/>
<wire x1="66.04" y1="51.816" x2="66.04" y2="40.386" width="0.127" layer="51"/>
<wire x1="66.04" y1="40.386" x2="68.58" y2="37.846" width="0.127" layer="51"/>
<wire x1="68.58" y1="37.846" x2="68.58" y2="5.08" width="0.127" layer="51"/>
<wire x1="68.58" y1="5.08" x2="66.04" y2="2.54" width="0.127" layer="51"/>
<wire x1="66.04" y1="2.54" x2="66.04" y2="0" width="0.127" layer="51"/>
<wire x1="66.04" y1="0" x2="0" y2="0" width="0.127" layer="51"/>
<wire x1="38.481" y1="52.07" x2="39.751" y2="52.07" width="0.1524" layer="21"/>
<wire x1="39.751" y1="52.07" x2="40.386" y2="51.435" width="0.1524" layer="21"/>
<wire x1="40.386" y1="51.435" x2="40.386" y2="50.165" width="0.1524" layer="21"/>
<wire x1="40.386" y1="50.165" x2="39.751" y2="49.53" width="0.1524" layer="21"/>
<wire x1="35.306" y1="51.435" x2="35.941" y2="52.07" width="0.1524" layer="21"/>
<wire x1="35.941" y1="52.07" x2="37.211" y2="52.07" width="0.1524" layer="21"/>
<wire x1="37.211" y1="52.07" x2="37.846" y2="51.435" width="0.1524" layer="21"/>
<wire x1="37.846" y1="51.435" x2="37.846" y2="50.165" width="0.1524" layer="21"/>
<wire x1="37.846" y1="50.165" x2="37.211" y2="49.53" width="0.1524" layer="21"/>
<wire x1="37.211" y1="49.53" x2="35.941" y2="49.53" width="0.1524" layer="21"/>
<wire x1="35.941" y1="49.53" x2="35.306" y2="50.165" width="0.1524" layer="21"/>
<wire x1="38.481" y1="52.07" x2="37.846" y2="51.435" width="0.1524" layer="21"/>
<wire x1="37.846" y1="50.165" x2="38.481" y2="49.53" width="0.1524" layer="21"/>
<wire x1="39.751" y1="49.53" x2="38.481" y2="49.53" width="0.1524" layer="21"/>
<wire x1="30.861" y1="52.07" x2="32.131" y2="52.07" width="0.1524" layer="21"/>
<wire x1="32.131" y1="52.07" x2="32.766" y2="51.435" width="0.1524" layer="21"/>
<wire x1="32.766" y1="51.435" x2="32.766" y2="50.165" width="0.1524" layer="21"/>
<wire x1="32.766" y1="50.165" x2="32.131" y2="49.53" width="0.1524" layer="21"/>
<wire x1="32.766" y1="51.435" x2="33.401" y2="52.07" width="0.1524" layer="21"/>
<wire x1="33.401" y1="52.07" x2="34.671" y2="52.07" width="0.1524" layer="21"/>
<wire x1="34.671" y1="52.07" x2="35.306" y2="51.435" width="0.1524" layer="21"/>
<wire x1="35.306" y1="51.435" x2="35.306" y2="50.165" width="0.1524" layer="21"/>
<wire x1="35.306" y1="50.165" x2="34.671" y2="49.53" width="0.1524" layer="21"/>
<wire x1="34.671" y1="49.53" x2="33.401" y2="49.53" width="0.1524" layer="21"/>
<wire x1="33.401" y1="49.53" x2="32.766" y2="50.165" width="0.1524" layer="21"/>
<wire x1="27.686" y1="51.435" x2="28.321" y2="52.07" width="0.1524" layer="21"/>
<wire x1="28.321" y1="52.07" x2="29.591" y2="52.07" width="0.1524" layer="21"/>
<wire x1="29.591" y1="52.07" x2="30.226" y2="51.435" width="0.1524" layer="21"/>
<wire x1="30.226" y1="51.435" x2="30.226" y2="50.165" width="0.1524" layer="21"/>
<wire x1="30.226" y1="50.165" x2="29.591" y2="49.53" width="0.1524" layer="21"/>
<wire x1="29.591" y1="49.53" x2="28.321" y2="49.53" width="0.1524" layer="21"/>
<wire x1="28.321" y1="49.53" x2="27.686" y2="50.165" width="0.1524" layer="21"/>
<wire x1="30.861" y1="52.07" x2="30.226" y2="51.435" width="0.1524" layer="21"/>
<wire x1="30.226" y1="50.165" x2="30.861" y2="49.53" width="0.1524" layer="21"/>
<wire x1="32.131" y1="49.53" x2="30.861" y2="49.53" width="0.1524" layer="21"/>
<wire x1="23.241" y1="52.07" x2="24.511" y2="52.07" width="0.1524" layer="21"/>
<wire x1="24.511" y1="52.07" x2="25.146" y2="51.435" width="0.1524" layer="21"/>
<wire x1="25.146" y1="51.435" x2="25.146" y2="50.165" width="0.1524" layer="21"/>
<wire x1="25.146" y1="50.165" x2="24.511" y2="49.53" width="0.1524" layer="21"/>
<wire x1="25.146" y1="51.435" x2="25.781" y2="52.07" width="0.1524" layer="21"/>
<wire x1="25.781" y1="52.07" x2="27.051" y2="52.07" width="0.1524" layer="21"/>
<wire x1="27.051" y1="52.07" x2="27.686" y2="51.435" width="0.1524" layer="21"/>
<wire x1="27.686" y1="51.435" x2="27.686" y2="50.165" width="0.1524" layer="21"/>
<wire x1="27.686" y1="50.165" x2="27.051" y2="49.53" width="0.1524" layer="21"/>
<wire x1="27.051" y1="49.53" x2="25.781" y2="49.53" width="0.1524" layer="21"/>
<wire x1="25.781" y1="49.53" x2="25.146" y2="50.165" width="0.1524" layer="21"/>
<wire x1="22.606" y1="51.435" x2="22.606" y2="50.165" width="0.1524" layer="21"/>
<wire x1="23.241" y1="52.07" x2="22.606" y2="51.435" width="0.1524" layer="21"/>
<wire x1="22.606" y1="50.165" x2="23.241" y2="49.53" width="0.1524" layer="21"/>
<wire x1="24.511" y1="49.53" x2="23.241" y2="49.53" width="0.1524" layer="21"/>
<wire x1="41.021" y1="52.07" x2="42.291" y2="52.07" width="0.1524" layer="21"/>
<wire x1="42.291" y1="52.07" x2="42.926" y2="51.435" width="0.1524" layer="21"/>
<wire x1="42.926" y1="51.435" x2="42.926" y2="50.165" width="0.1524" layer="21"/>
<wire x1="42.926" y1="50.165" x2="42.291" y2="49.53" width="0.1524" layer="21"/>
<wire x1="41.021" y1="52.07" x2="40.386" y2="51.435" width="0.1524" layer="21"/>
<wire x1="40.386" y1="50.165" x2="41.021" y2="49.53" width="0.1524" layer="21"/>
<wire x1="42.291" y1="49.53" x2="41.021" y2="49.53" width="0.1524" layer="21"/>
<wire x1="60.325" y1="52.07" x2="61.595" y2="52.07" width="0.1524" layer="21"/>
<wire x1="61.595" y1="52.07" x2="62.23" y2="51.435" width="0.1524" layer="21"/>
<wire x1="62.23" y1="51.435" x2="62.23" y2="50.165" width="0.1524" layer="21"/>
<wire x1="62.23" y1="50.165" x2="61.595" y2="49.53" width="0.1524" layer="21"/>
<wire x1="57.15" y1="51.435" x2="57.785" y2="52.07" width="0.1524" layer="21"/>
<wire x1="57.785" y1="52.07" x2="59.055" y2="52.07" width="0.1524" layer="21"/>
<wire x1="59.055" y1="52.07" x2="59.69" y2="51.435" width="0.1524" layer="21"/>
<wire x1="59.69" y1="51.435" x2="59.69" y2="50.165" width="0.1524" layer="21"/>
<wire x1="59.69" y1="50.165" x2="59.055" y2="49.53" width="0.1524" layer="21"/>
<wire x1="59.055" y1="49.53" x2="57.785" y2="49.53" width="0.1524" layer="21"/>
<wire x1="57.785" y1="49.53" x2="57.15" y2="50.165" width="0.1524" layer="21"/>
<wire x1="60.325" y1="52.07" x2="59.69" y2="51.435" width="0.1524" layer="21"/>
<wire x1="59.69" y1="50.165" x2="60.325" y2="49.53" width="0.1524" layer="21"/>
<wire x1="61.595" y1="49.53" x2="60.325" y2="49.53" width="0.1524" layer="21"/>
<wire x1="52.705" y1="52.07" x2="53.975" y2="52.07" width="0.1524" layer="21"/>
<wire x1="53.975" y1="52.07" x2="54.61" y2="51.435" width="0.1524" layer="21"/>
<wire x1="54.61" y1="51.435" x2="54.61" y2="50.165" width="0.1524" layer="21"/>
<wire x1="54.61" y1="50.165" x2="53.975" y2="49.53" width="0.1524" layer="21"/>
<wire x1="54.61" y1="51.435" x2="55.245" y2="52.07" width="0.1524" layer="21"/>
<wire x1="55.245" y1="52.07" x2="56.515" y2="52.07" width="0.1524" layer="21"/>
<wire x1="56.515" y1="52.07" x2="57.15" y2="51.435" width="0.1524" layer="21"/>
<wire x1="57.15" y1="51.435" x2="57.15" y2="50.165" width="0.1524" layer="21"/>
<wire x1="57.15" y1="50.165" x2="56.515" y2="49.53" width="0.1524" layer="21"/>
<wire x1="56.515" y1="49.53" x2="55.245" y2="49.53" width="0.1524" layer="21"/>
<wire x1="55.245" y1="49.53" x2="54.61" y2="50.165" width="0.1524" layer="21"/>
<wire x1="49.53" y1="51.435" x2="50.165" y2="52.07" width="0.1524" layer="21"/>
<wire x1="50.165" y1="52.07" x2="51.435" y2="52.07" width="0.1524" layer="21"/>
<wire x1="51.435" y1="52.07" x2="52.07" y2="51.435" width="0.1524" layer="21"/>
<wire x1="52.07" y1="51.435" x2="52.07" y2="50.165" width="0.1524" layer="21"/>
<wire x1="52.07" y1="50.165" x2="51.435" y2="49.53" width="0.1524" layer="21"/>
<wire x1="51.435" y1="49.53" x2="50.165" y2="49.53" width="0.1524" layer="21"/>
<wire x1="50.165" y1="49.53" x2="49.53" y2="50.165" width="0.1524" layer="21"/>
<wire x1="52.705" y1="52.07" x2="52.07" y2="51.435" width="0.1524" layer="21"/>
<wire x1="52.07" y1="50.165" x2="52.705" y2="49.53" width="0.1524" layer="21"/>
<wire x1="53.975" y1="49.53" x2="52.705" y2="49.53" width="0.1524" layer="21"/>
<wire x1="45.085" y1="52.07" x2="46.355" y2="52.07" width="0.1524" layer="21"/>
<wire x1="46.355" y1="52.07" x2="46.99" y2="51.435" width="0.1524" layer="21"/>
<wire x1="46.99" y1="51.435" x2="46.99" y2="50.165" width="0.1524" layer="21"/>
<wire x1="46.99" y1="50.165" x2="46.355" y2="49.53" width="0.1524" layer="21"/>
<wire x1="46.99" y1="51.435" x2="47.625" y2="52.07" width="0.1524" layer="21"/>
<wire x1="47.625" y1="52.07" x2="48.895" y2="52.07" width="0.1524" layer="21"/>
<wire x1="48.895" y1="52.07" x2="49.53" y2="51.435" width="0.1524" layer="21"/>
<wire x1="49.53" y1="51.435" x2="49.53" y2="50.165" width="0.1524" layer="21"/>
<wire x1="49.53" y1="50.165" x2="48.895" y2="49.53" width="0.1524" layer="21"/>
<wire x1="48.895" y1="49.53" x2="47.625" y2="49.53" width="0.1524" layer="21"/>
<wire x1="47.625" y1="49.53" x2="46.99" y2="50.165" width="0.1524" layer="21"/>
<wire x1="44.45" y1="51.435" x2="44.45" y2="50.165" width="0.1524" layer="21"/>
<wire x1="45.085" y1="52.07" x2="44.45" y2="51.435" width="0.1524" layer="21"/>
<wire x1="44.45" y1="50.165" x2="45.085" y2="49.53" width="0.1524" layer="21"/>
<wire x1="46.355" y1="49.53" x2="45.085" y2="49.53" width="0.1524" layer="21"/>
<wire x1="62.865" y1="52.07" x2="64.135" y2="52.07" width="0.1524" layer="21"/>
<wire x1="64.135" y1="52.07" x2="64.77" y2="51.435" width="0.1524" layer="21"/>
<wire x1="64.77" y1="51.435" x2="64.77" y2="50.165" width="0.1524" layer="21"/>
<wire x1="64.77" y1="50.165" x2="64.135" y2="49.53" width="0.1524" layer="21"/>
<wire x1="62.865" y1="52.07" x2="62.23" y2="51.435" width="0.1524" layer="21"/>
<wire x1="62.23" y1="50.165" x2="62.865" y2="49.53" width="0.1524" layer="21"/>
<wire x1="64.135" y1="49.53" x2="62.865" y2="49.53" width="0.1524" layer="21"/>
<wire x1="45.085" y1="3.81" x2="46.355" y2="3.81" width="0.1524" layer="21"/>
<wire x1="46.355" y1="3.81" x2="46.99" y2="3.175" width="0.1524" layer="21"/>
<wire x1="46.99" y1="1.905" x2="46.355" y2="1.27" width="0.1524" layer="21"/>
<wire x1="41.91" y1="3.175" x2="42.545" y2="3.81" width="0.1524" layer="21"/>
<wire x1="42.545" y1="3.81" x2="43.815" y2="3.81" width="0.1524" layer="21"/>
<wire x1="43.815" y1="3.81" x2="44.45" y2="3.175" width="0.1524" layer="21"/>
<wire x1="44.45" y1="3.175" x2="44.45" y2="1.905" width="0.1524" layer="21"/>
<wire x1="44.45" y1="1.905" x2="43.815" y2="1.27" width="0.1524" layer="21"/>
<wire x1="43.815" y1="1.27" x2="42.545" y2="1.27" width="0.1524" layer="21"/>
<wire x1="42.545" y1="1.27" x2="41.91" y2="1.905" width="0.1524" layer="21"/>
<wire x1="45.085" y1="3.81" x2="44.45" y2="3.175" width="0.1524" layer="21"/>
<wire x1="44.45" y1="1.905" x2="45.085" y2="1.27" width="0.1524" layer="21"/>
<wire x1="46.355" y1="1.27" x2="45.085" y2="1.27" width="0.1524" layer="21"/>
<wire x1="37.465" y1="3.81" x2="38.735" y2="3.81" width="0.1524" layer="21"/>
<wire x1="38.735" y1="3.81" x2="39.37" y2="3.175" width="0.1524" layer="21"/>
<wire x1="39.37" y1="3.175" x2="39.37" y2="1.905" width="0.1524" layer="21"/>
<wire x1="39.37" y1="1.905" x2="38.735" y2="1.27" width="0.1524" layer="21"/>
<wire x1="39.37" y1="3.175" x2="40.005" y2="3.81" width="0.1524" layer="21"/>
<wire x1="40.005" y1="3.81" x2="41.275" y2="3.81" width="0.1524" layer="21"/>
<wire x1="41.275" y1="3.81" x2="41.91" y2="3.175" width="0.1524" layer="21"/>
<wire x1="41.91" y1="3.175" x2="41.91" y2="1.905" width="0.1524" layer="21"/>
<wire x1="41.91" y1="1.905" x2="41.275" y2="1.27" width="0.1524" layer="21"/>
<wire x1="41.275" y1="1.27" x2="40.005" y2="1.27" width="0.1524" layer="21"/>
<wire x1="40.005" y1="1.27" x2="39.37" y2="1.905" width="0.1524" layer="21"/>
<wire x1="34.29" y1="3.175" x2="34.925" y2="3.81" width="0.1524" layer="21"/>
<wire x1="34.925" y1="3.81" x2="36.195" y2="3.81" width="0.1524" layer="21"/>
<wire x1="36.195" y1="3.81" x2="36.83" y2="3.175" width="0.1524" layer="21"/>
<wire x1="36.83" y1="3.175" x2="36.83" y2="1.905" width="0.1524" layer="21"/>
<wire x1="36.83" y1="1.905" x2="36.195" y2="1.27" width="0.1524" layer="21"/>
<wire x1="36.195" y1="1.27" x2="34.925" y2="1.27" width="0.1524" layer="21"/>
<wire x1="34.925" y1="1.27" x2="34.29" y2="1.905" width="0.1524" layer="21"/>
<wire x1="37.465" y1="3.81" x2="36.83" y2="3.175" width="0.1524" layer="21"/>
<wire x1="36.83" y1="1.905" x2="37.465" y2="1.27" width="0.1524" layer="21"/>
<wire x1="38.735" y1="1.27" x2="37.465" y2="1.27" width="0.1524" layer="21"/>
<wire x1="31.75" y1="3.175" x2="31.75" y2="1.905" width="0.1524" layer="21"/>
<wire x1="31.75" y1="3.175" x2="32.385" y2="3.81" width="0.1524" layer="21"/>
<wire x1="32.385" y1="3.81" x2="33.655" y2="3.81" width="0.1524" layer="21"/>
<wire x1="33.655" y1="3.81" x2="34.29" y2="3.175" width="0.1524" layer="21"/>
<wire x1="34.29" y1="3.175" x2="34.29" y2="1.905" width="0.1524" layer="21"/>
<wire x1="34.29" y1="1.905" x2="33.655" y2="1.27" width="0.1524" layer="21"/>
<wire x1="33.655" y1="1.27" x2="32.385" y2="1.27" width="0.1524" layer="21"/>
<wire x1="32.385" y1="1.27" x2="31.75" y2="1.905" width="0.1524" layer="21"/>
<wire x1="62.865" y1="3.81" x2="64.135" y2="3.81" width="0.1524" layer="21"/>
<wire x1="64.135" y1="3.81" x2="64.77" y2="3.175" width="0.1524" layer="21"/>
<wire x1="64.77" y1="3.175" x2="64.77" y2="1.905" width="0.1524" layer="21"/>
<wire x1="64.77" y1="1.905" x2="64.135" y2="1.27" width="0.1524" layer="21"/>
<wire x1="59.69" y1="3.175" x2="60.325" y2="3.81" width="0.1524" layer="21"/>
<wire x1="60.325" y1="3.81" x2="61.595" y2="3.81" width="0.1524" layer="21"/>
<wire x1="61.595" y1="3.81" x2="62.23" y2="3.175" width="0.1524" layer="21"/>
<wire x1="62.23" y1="3.175" x2="62.23" y2="1.905" width="0.1524" layer="21"/>
<wire x1="62.23" y1="1.905" x2="61.595" y2="1.27" width="0.1524" layer="21"/>
<wire x1="61.595" y1="1.27" x2="60.325" y2="1.27" width="0.1524" layer="21"/>
<wire x1="60.325" y1="1.27" x2="59.69" y2="1.905" width="0.1524" layer="21"/>
<wire x1="62.865" y1="3.81" x2="62.23" y2="3.175" width="0.1524" layer="21"/>
<wire x1="62.23" y1="1.905" x2="62.865" y2="1.27" width="0.1524" layer="21"/>
<wire x1="64.135" y1="1.27" x2="62.865" y2="1.27" width="0.1524" layer="21"/>
<wire x1="55.245" y1="3.81" x2="56.515" y2="3.81" width="0.1524" layer="21"/>
<wire x1="56.515" y1="3.81" x2="57.15" y2="3.175" width="0.1524" layer="21"/>
<wire x1="57.15" y1="3.175" x2="57.15" y2="1.905" width="0.1524" layer="21"/>
<wire x1="57.15" y1="1.905" x2="56.515" y2="1.27" width="0.1524" layer="21"/>
<wire x1="57.15" y1="3.175" x2="57.785" y2="3.81" width="0.1524" layer="21"/>
<wire x1="57.785" y1="3.81" x2="59.055" y2="3.81" width="0.1524" layer="21"/>
<wire x1="59.055" y1="3.81" x2="59.69" y2="3.175" width="0.1524" layer="21"/>
<wire x1="59.69" y1="3.175" x2="59.69" y2="1.905" width="0.1524" layer="21"/>
<wire x1="59.69" y1="1.905" x2="59.055" y2="1.27" width="0.1524" layer="21"/>
<wire x1="59.055" y1="1.27" x2="57.785" y2="1.27" width="0.1524" layer="21"/>
<wire x1="57.785" y1="1.27" x2="57.15" y2="1.905" width="0.1524" layer="21"/>
<wire x1="52.07" y1="3.175" x2="52.705" y2="3.81" width="0.1524" layer="21"/>
<wire x1="52.705" y1="3.81" x2="53.975" y2="3.81" width="0.1524" layer="21"/>
<wire x1="53.975" y1="3.81" x2="54.61" y2="3.175" width="0.1524" layer="21"/>
<wire x1="54.61" y1="3.175" x2="54.61" y2="1.905" width="0.1524" layer="21"/>
<wire x1="54.61" y1="1.905" x2="53.975" y2="1.27" width="0.1524" layer="21"/>
<wire x1="53.975" y1="1.27" x2="52.705" y2="1.27" width="0.1524" layer="21"/>
<wire x1="52.705" y1="1.27" x2="52.07" y2="1.905" width="0.1524" layer="21"/>
<wire x1="55.245" y1="3.81" x2="54.61" y2="3.175" width="0.1524" layer="21"/>
<wire x1="54.61" y1="1.905" x2="55.245" y2="1.27" width="0.1524" layer="21"/>
<wire x1="56.515" y1="1.27" x2="55.245" y2="1.27" width="0.1524" layer="21"/>
<wire x1="49.53" y1="3.175" x2="49.53" y2="1.905" width="0.1524" layer="21"/>
<wire x1="49.53" y1="3.175" x2="50.165" y2="3.81" width="0.1524" layer="21"/>
<wire x1="50.165" y1="3.81" x2="51.435" y2="3.81" width="0.1524" layer="21"/>
<wire x1="51.435" y1="3.81" x2="52.07" y2="3.175" width="0.1524" layer="21"/>
<wire x1="52.07" y1="3.175" x2="52.07" y2="1.905" width="0.1524" layer="21"/>
<wire x1="52.07" y1="1.905" x2="51.435" y2="1.27" width="0.1524" layer="21"/>
<wire x1="51.435" y1="1.27" x2="50.165" y2="1.27" width="0.1524" layer="21"/>
<wire x1="50.165" y1="1.27" x2="49.53" y2="1.905" width="0.1524" layer="21"/>
<wire x1="46.99" y1="3.175" x2="46.99" y2="1.905" width="0.1524" layer="21"/>
<pad name="28" x="23.876" y="50.8" drill="1.016" shape="long" rot="R90"/>
<pad name="27" x="26.416" y="50.8" drill="1.016" shape="long" rot="R90"/>
<pad name="26" x="28.956" y="50.8" drill="1.016" shape="long" rot="R90"/>
<pad name="25" x="31.496" y="50.8" drill="1.016" shape="long" rot="R90"/>
<pad name="24" x="34.036" y="50.8" drill="1.016" shape="long" rot="R90"/>
<pad name="23" x="36.576" y="50.8" drill="1.016" shape="long" rot="R90"/>
<pad name="22" x="39.116" y="50.8" drill="1.016" shape="long" rot="R90"/>
<pad name="21" x="41.656" y="50.8" drill="1.016" shape="long" rot="R90"/>
<pad name="20" x="45.72" y="50.8" drill="1.016" shape="long" rot="R90"/>
<pad name="19" x="48.26" y="50.8" drill="1.016" shape="long" rot="R90"/>
<pad name="18" x="50.8" y="50.8" drill="1.016" shape="long" rot="R90"/>
<pad name="17" x="53.34" y="50.8" drill="1.016" shape="long" rot="R90"/>
<pad name="16" x="55.88" y="50.8" drill="1.016" shape="long" rot="R90"/>
<pad name="15" x="58.42" y="50.8" drill="1.016" shape="long" rot="R90"/>
<pad name="14" x="60.96" y="50.8" drill="1.016" shape="long" rot="R90"/>
<pad name="13" x="63.5" y="50.8" drill="1.016" shape="long" rot="R90"/>
<pad name="1" x="33.02" y="2.54" drill="1.016" shape="long" rot="R90"/>
<pad name="2" x="35.56" y="2.54" drill="1.016" shape="long" rot="R90"/>
<pad name="3" x="38.1" y="2.54" drill="1.016" shape="long" rot="R90"/>
<pad name="4" x="40.64" y="2.54" drill="1.016" shape="long" rot="R90"/>
<pad name="5" x="43.18" y="2.54" drill="1.016" shape="long" rot="R90"/>
<pad name="6" x="45.72" y="2.54" drill="1.016" shape="long" rot="R90"/>
<pad name="7" x="50.8" y="2.54" drill="1.016" shape="long" rot="R90"/>
<pad name="8" x="53.34" y="2.54" drill="1.016" shape="long" rot="R90"/>
<pad name="9" x="55.88" y="2.54" drill="1.016" shape="long" rot="R90"/>
<pad name="10" x="58.42" y="2.54" drill="1.016" shape="long" rot="R90"/>
<pad name="11" x="60.96" y="2.54" drill="1.016" shape="long" rot="R90"/>
<pad name="12" x="63.5" y="2.54" drill="1.016" shape="long" rot="R90"/>
<text x="23.1184" y="48.7812" size="1.016" layer="48" font="vector" ratio="10" rot="R270">AREF</text>
<text x="25.654" y="48.768" size="1.016" layer="48" font="vector" ratio="10" rot="R270">GND</text>
<text x="28.194" y="48.768" size="1.016" layer="48" font="vector" ratio="10" rot="R270">PB5</text>
<text x="30.734" y="48.768" size="1.016" layer="48" font="vector" ratio="10" rot="R270">PB4</text>
<text x="33.274" y="48.768" size="1.016" layer="48" font="vector" ratio="10" rot="R270">PB3</text>
<text x="35.814" y="48.768" size="1.016" layer="48" font="vector" ratio="10" rot="R270">PB2</text>
<text x="38.354" y="48.768" size="1.016" layer="48" font="vector" ratio="10" rot="R270">PB1</text>
<text x="40.894" y="48.768" size="1.016" layer="48" font="vector" ratio="10" rot="R270">PB0</text>
<text x="44.958" y="48.768" size="1.016" layer="48" font="vector" ratio="10" rot="R270">PD7</text>
<text x="47.498" y="48.768" size="1.016" layer="48" font="vector" ratio="10" rot="R270">PD6</text>
<text x="50.038" y="48.768" size="1.016" layer="48" font="vector" ratio="10" rot="R270">PD5</text>
<text x="52.578" y="48.768" size="1.016" layer="48" font="vector" ratio="10" rot="R270">PD4</text>
<text x="55.118" y="48.768" size="1.016" layer="48" font="vector" ratio="10" rot="R270">PD3</text>
<text x="57.658" y="48.768" size="1.016" layer="48" font="vector" ratio="10" rot="R270">PD2</text>
<text x="60.198" y="48.768" size="1.016" layer="48" font="vector" ratio="10" rot="R270">PD1</text>
<text x="62.738" y="48.768" size="1.016" layer="48" font="vector" ratio="10" rot="R270">PD0</text>
<text x="33.7776" y="4.8128" size="1.016" layer="48" font="vector" ratio="10" rot="R90">!Reset</text>
<text x="36.3176" y="4.8128" size="1.016" layer="48" font="vector" ratio="10" rot="R90">+3V3</text>
<text x="38.8576" y="4.8128" size="1.016" layer="48" font="vector" ratio="10" rot="R90">+5V</text>
<text x="41.3976" y="4.8128" size="1.016" layer="48" font="vector" ratio="10" rot="R90">GND</text>
<text x="43.9376" y="4.8128" size="1.016" layer="48" font="vector" ratio="10" rot="R90">GND</text>
<text x="46.4776" y="4.8128" size="1.016" layer="48" font="vector" ratio="10" rot="R90">+Vin</text>
<text x="51.5576" y="4.8128" size="1.016" layer="48" font="vector" ratio="10" rot="R90">PC0</text>
<text x="54.0976" y="4.8128" size="1.016" layer="48" font="vector" ratio="10" rot="R90">PC1</text>
<text x="56.6376" y="4.8128" size="1.016" layer="48" font="vector" ratio="10" rot="R90">PC2</text>
<text x="59.1776" y="4.8128" size="1.016" layer="48" font="vector" ratio="10" rot="R90">PC3</text>
<text x="61.7176" y="4.8128" size="1.016" layer="48" font="vector" ratio="10" rot="R90">PC4</text>
<text x="64.2576" y="4.8128" size="1.016" layer="48" font="vector" ratio="10" rot="R90">PC5</text>
<rectangle x1="38.862" y1="50.546" x2="39.37" y2="51.054" layer="51"/>
<rectangle x1="36.322" y1="50.546" x2="36.83" y2="51.054" layer="51"/>
<rectangle x1="33.782" y1="50.546" x2="34.29" y2="51.054" layer="51"/>
<rectangle x1="31.242" y1="50.546" x2="31.75" y2="51.054" layer="51"/>
<rectangle x1="28.702" y1="50.546" x2="29.21" y2="51.054" layer="51"/>
<rectangle x1="26.162" y1="50.546" x2="26.67" y2="51.054" layer="51"/>
<rectangle x1="23.622" y1="50.546" x2="24.13" y2="51.054" layer="51"/>
<rectangle x1="41.402" y1="50.546" x2="41.91" y2="51.054" layer="51"/>
<rectangle x1="60.706" y1="50.546" x2="61.214" y2="51.054" layer="51"/>
<rectangle x1="58.166" y1="50.546" x2="58.674" y2="51.054" layer="51"/>
<rectangle x1="55.626" y1="50.546" x2="56.134" y2="51.054" layer="51"/>
<rectangle x1="53.086" y1="50.546" x2="53.594" y2="51.054" layer="51"/>
<rectangle x1="50.546" y1="50.546" x2="51.054" y2="51.054" layer="51"/>
<rectangle x1="48.006" y1="50.546" x2="48.514" y2="51.054" layer="51"/>
<rectangle x1="45.466" y1="50.546" x2="45.974" y2="51.054" layer="51"/>
<rectangle x1="63.246" y1="50.546" x2="63.754" y2="51.054" layer="51"/>
<rectangle x1="45.466" y1="2.286" x2="45.974" y2="2.794" layer="51"/>
<rectangle x1="42.926" y1="2.286" x2="43.434" y2="2.794" layer="51"/>
<rectangle x1="40.386" y1="2.286" x2="40.894" y2="2.794" layer="51"/>
<rectangle x1="37.846" y1="2.286" x2="38.354" y2="2.794" layer="51"/>
<rectangle x1="35.306" y1="2.286" x2="35.814" y2="2.794" layer="51"/>
<rectangle x1="32.766" y1="2.286" x2="33.274" y2="2.794" layer="51"/>
<rectangle x1="63.246" y1="2.286" x2="63.754" y2="2.794" layer="51"/>
<rectangle x1="60.706" y1="2.286" x2="61.214" y2="2.794" layer="51"/>
<rectangle x1="58.166" y1="2.286" x2="58.674" y2="2.794" layer="51"/>
<rectangle x1="55.626" y1="2.286" x2="56.134" y2="2.794" layer="51"/>
<rectangle x1="53.086" y1="2.286" x2="53.594" y2="2.794" layer="51"/>
<rectangle x1="50.546" y1="2.286" x2="51.054" y2="2.794" layer="51"/>
<hole x="15.24" y="50.8" drill="3.2"/>
<hole x="66.04" y="35.56" drill="3.2"/>
<hole x="66.04" y="7.62" drill="3.2"/>
<hole x="55.1696" y="27.1138" drill="2"/>
</package>
<package name="ARDUINO2009_2">
<wire x1="0" y1="0" x2="0" y2="53.34" width="0.127" layer="51"/>
<wire x1="0" y1="53.34" x2="64.516" y2="53.34" width="0.127" layer="51"/>
<wire x1="64.516" y1="53.34" x2="66.04" y2="51.816" width="0.127" layer="51"/>
<wire x1="66.04" y1="51.816" x2="66.04" y2="40.386" width="0.127" layer="51"/>
<wire x1="66.04" y1="40.386" x2="68.58" y2="37.846" width="0.127" layer="51"/>
<wire x1="68.58" y1="37.846" x2="68.58" y2="5.08" width="0.127" layer="51"/>
<wire x1="68.58" y1="5.08" x2="66.04" y2="2.54" width="0.127" layer="51"/>
<wire x1="66.04" y1="2.54" x2="66.04" y2="0" width="0.127" layer="51"/>
<wire x1="66.04" y1="0" x2="0" y2="0" width="0.127" layer="51"/>
<wire x1="38.481" y1="52.07" x2="39.751" y2="52.07" width="0.1524" layer="21"/>
<wire x1="39.751" y1="52.07" x2="40.386" y2="51.435" width="0.1524" layer="21"/>
<wire x1="40.386" y1="51.435" x2="40.386" y2="50.165" width="0.1524" layer="21"/>
<wire x1="40.386" y1="50.165" x2="39.751" y2="49.53" width="0.1524" layer="21"/>
<wire x1="35.306" y1="51.435" x2="35.941" y2="52.07" width="0.1524" layer="21"/>
<wire x1="35.941" y1="52.07" x2="37.211" y2="52.07" width="0.1524" layer="21"/>
<wire x1="37.211" y1="52.07" x2="37.846" y2="51.435" width="0.1524" layer="21"/>
<wire x1="37.846" y1="51.435" x2="37.846" y2="50.165" width="0.1524" layer="21"/>
<wire x1="37.846" y1="50.165" x2="37.211" y2="49.53" width="0.1524" layer="21"/>
<wire x1="37.211" y1="49.53" x2="35.941" y2="49.53" width="0.1524" layer="21"/>
<wire x1="35.941" y1="49.53" x2="35.306" y2="50.165" width="0.1524" layer="21"/>
<wire x1="38.481" y1="52.07" x2="37.846" y2="51.435" width="0.1524" layer="21"/>
<wire x1="37.846" y1="50.165" x2="38.481" y2="49.53" width="0.1524" layer="21"/>
<wire x1="39.751" y1="49.53" x2="38.481" y2="49.53" width="0.1524" layer="21"/>
<wire x1="30.861" y1="52.07" x2="32.131" y2="52.07" width="0.1524" layer="21"/>
<wire x1="32.131" y1="52.07" x2="32.766" y2="51.435" width="0.1524" layer="21"/>
<wire x1="32.766" y1="51.435" x2="32.766" y2="50.165" width="0.1524" layer="21"/>
<wire x1="32.766" y1="50.165" x2="32.131" y2="49.53" width="0.1524" layer="21"/>
<wire x1="32.766" y1="51.435" x2="33.401" y2="52.07" width="0.1524" layer="21"/>
<wire x1="33.401" y1="52.07" x2="34.671" y2="52.07" width="0.1524" layer="21"/>
<wire x1="34.671" y1="52.07" x2="35.306" y2="51.435" width="0.1524" layer="21"/>
<wire x1="35.306" y1="51.435" x2="35.306" y2="50.165" width="0.1524" layer="21"/>
<wire x1="35.306" y1="50.165" x2="34.671" y2="49.53" width="0.1524" layer="21"/>
<wire x1="34.671" y1="49.53" x2="33.401" y2="49.53" width="0.1524" layer="21"/>
<wire x1="33.401" y1="49.53" x2="32.766" y2="50.165" width="0.1524" layer="21"/>
<wire x1="27.686" y1="51.435" x2="28.321" y2="52.07" width="0.1524" layer="21"/>
<wire x1="28.321" y1="52.07" x2="29.591" y2="52.07" width="0.1524" layer="21"/>
<wire x1="29.591" y1="52.07" x2="30.226" y2="51.435" width="0.1524" layer="21"/>
<wire x1="30.226" y1="51.435" x2="30.226" y2="50.165" width="0.1524" layer="21"/>
<wire x1="30.226" y1="50.165" x2="29.591" y2="49.53" width="0.1524" layer="21"/>
<wire x1="29.591" y1="49.53" x2="28.321" y2="49.53" width="0.1524" layer="21"/>
<wire x1="28.321" y1="49.53" x2="27.686" y2="50.165" width="0.1524" layer="21"/>
<wire x1="30.861" y1="52.07" x2="30.226" y2="51.435" width="0.1524" layer="21"/>
<wire x1="30.226" y1="50.165" x2="30.861" y2="49.53" width="0.1524" layer="21"/>
<wire x1="32.131" y1="49.53" x2="30.861" y2="49.53" width="0.1524" layer="21"/>
<wire x1="23.241" y1="52.07" x2="24.511" y2="52.07" width="0.1524" layer="21"/>
<wire x1="24.511" y1="52.07" x2="25.146" y2="51.435" width="0.1524" layer="21"/>
<wire x1="25.146" y1="51.435" x2="25.146" y2="50.165" width="0.1524" layer="21"/>
<wire x1="25.146" y1="50.165" x2="24.511" y2="49.53" width="0.1524" layer="21"/>
<wire x1="25.146" y1="51.435" x2="25.781" y2="52.07" width="0.1524" layer="21"/>
<wire x1="25.781" y1="52.07" x2="27.051" y2="52.07" width="0.1524" layer="21"/>
<wire x1="27.051" y1="52.07" x2="27.686" y2="51.435" width="0.1524" layer="21"/>
<wire x1="27.686" y1="51.435" x2="27.686" y2="50.165" width="0.1524" layer="21"/>
<wire x1="27.686" y1="50.165" x2="27.051" y2="49.53" width="0.1524" layer="21"/>
<wire x1="27.051" y1="49.53" x2="25.781" y2="49.53" width="0.1524" layer="21"/>
<wire x1="25.781" y1="49.53" x2="25.146" y2="50.165" width="0.1524" layer="21"/>
<wire x1="22.606" y1="51.435" x2="22.606" y2="50.165" width="0.1524" layer="21"/>
<wire x1="23.241" y1="52.07" x2="22.606" y2="51.435" width="0.1524" layer="21"/>
<wire x1="22.606" y1="50.165" x2="23.241" y2="49.53" width="0.1524" layer="21"/>
<wire x1="24.511" y1="49.53" x2="23.241" y2="49.53" width="0.1524" layer="21"/>
<wire x1="41.021" y1="52.07" x2="42.291" y2="52.07" width="0.1524" layer="21"/>
<wire x1="42.291" y1="52.07" x2="42.926" y2="51.435" width="0.1524" layer="21"/>
<wire x1="42.926" y1="51.435" x2="42.926" y2="50.165" width="0.1524" layer="21"/>
<wire x1="42.926" y1="50.165" x2="42.291" y2="49.53" width="0.1524" layer="21"/>
<wire x1="41.021" y1="52.07" x2="40.386" y2="51.435" width="0.1524" layer="21"/>
<wire x1="40.386" y1="50.165" x2="41.021" y2="49.53" width="0.1524" layer="21"/>
<wire x1="42.291" y1="49.53" x2="41.021" y2="49.53" width="0.1524" layer="21"/>
<wire x1="60.325" y1="52.07" x2="61.595" y2="52.07" width="0.1524" layer="21"/>
<wire x1="61.595" y1="52.07" x2="62.23" y2="51.435" width="0.1524" layer="21"/>
<wire x1="62.23" y1="51.435" x2="62.23" y2="50.165" width="0.1524" layer="21"/>
<wire x1="62.23" y1="50.165" x2="61.595" y2="49.53" width="0.1524" layer="21"/>
<wire x1="57.15" y1="51.435" x2="57.785" y2="52.07" width="0.1524" layer="21"/>
<wire x1="57.785" y1="52.07" x2="59.055" y2="52.07" width="0.1524" layer="21"/>
<wire x1="59.055" y1="52.07" x2="59.69" y2="51.435" width="0.1524" layer="21"/>
<wire x1="59.69" y1="51.435" x2="59.69" y2="50.165" width="0.1524" layer="21"/>
<wire x1="59.69" y1="50.165" x2="59.055" y2="49.53" width="0.1524" layer="21"/>
<wire x1="59.055" y1="49.53" x2="57.785" y2="49.53" width="0.1524" layer="21"/>
<wire x1="57.785" y1="49.53" x2="57.15" y2="50.165" width="0.1524" layer="21"/>
<wire x1="60.325" y1="52.07" x2="59.69" y2="51.435" width="0.1524" layer="21"/>
<wire x1="59.69" y1="50.165" x2="60.325" y2="49.53" width="0.1524" layer="21"/>
<wire x1="61.595" y1="49.53" x2="60.325" y2="49.53" width="0.1524" layer="21"/>
<wire x1="52.705" y1="52.07" x2="53.975" y2="52.07" width="0.1524" layer="21"/>
<wire x1="53.975" y1="52.07" x2="54.61" y2="51.435" width="0.1524" layer="21"/>
<wire x1="54.61" y1="51.435" x2="54.61" y2="50.165" width="0.1524" layer="21"/>
<wire x1="54.61" y1="50.165" x2="53.975" y2="49.53" width="0.1524" layer="21"/>
<wire x1="54.61" y1="51.435" x2="55.245" y2="52.07" width="0.1524" layer="21"/>
<wire x1="55.245" y1="52.07" x2="56.515" y2="52.07" width="0.1524" layer="21"/>
<wire x1="56.515" y1="52.07" x2="57.15" y2="51.435" width="0.1524" layer="21"/>
<wire x1="57.15" y1="51.435" x2="57.15" y2="50.165" width="0.1524" layer="21"/>
<wire x1="57.15" y1="50.165" x2="56.515" y2="49.53" width="0.1524" layer="21"/>
<wire x1="56.515" y1="49.53" x2="55.245" y2="49.53" width="0.1524" layer="21"/>
<wire x1="55.245" y1="49.53" x2="54.61" y2="50.165" width="0.1524" layer="21"/>
<wire x1="49.53" y1="51.435" x2="50.165" y2="52.07" width="0.1524" layer="21"/>
<wire x1="50.165" y1="52.07" x2="51.435" y2="52.07" width="0.1524" layer="21"/>
<wire x1="51.435" y1="52.07" x2="52.07" y2="51.435" width="0.1524" layer="21"/>
<wire x1="52.07" y1="51.435" x2="52.07" y2="50.165" width="0.1524" layer="21"/>
<wire x1="52.07" y1="50.165" x2="51.435" y2="49.53" width="0.1524" layer="21"/>
<wire x1="51.435" y1="49.53" x2="50.165" y2="49.53" width="0.1524" layer="21"/>
<wire x1="50.165" y1="49.53" x2="49.53" y2="50.165" width="0.1524" layer="21"/>
<wire x1="52.705" y1="52.07" x2="52.07" y2="51.435" width="0.1524" layer="21"/>
<wire x1="52.07" y1="50.165" x2="52.705" y2="49.53" width="0.1524" layer="21"/>
<wire x1="53.975" y1="49.53" x2="52.705" y2="49.53" width="0.1524" layer="21"/>
<wire x1="45.085" y1="52.07" x2="46.355" y2="52.07" width="0.1524" layer="21"/>
<wire x1="46.355" y1="52.07" x2="46.99" y2="51.435" width="0.1524" layer="21"/>
<wire x1="46.99" y1="51.435" x2="46.99" y2="50.165" width="0.1524" layer="21"/>
<wire x1="46.99" y1="50.165" x2="46.355" y2="49.53" width="0.1524" layer="21"/>
<wire x1="46.99" y1="51.435" x2="47.625" y2="52.07" width="0.1524" layer="21"/>
<wire x1="47.625" y1="52.07" x2="48.895" y2="52.07" width="0.1524" layer="21"/>
<wire x1="48.895" y1="52.07" x2="49.53" y2="51.435" width="0.1524" layer="21"/>
<wire x1="49.53" y1="51.435" x2="49.53" y2="50.165" width="0.1524" layer="21"/>
<wire x1="49.53" y1="50.165" x2="48.895" y2="49.53" width="0.1524" layer="21"/>
<wire x1="48.895" y1="49.53" x2="47.625" y2="49.53" width="0.1524" layer="21"/>
<wire x1="47.625" y1="49.53" x2="46.99" y2="50.165" width="0.1524" layer="21"/>
<wire x1="44.45" y1="51.435" x2="44.45" y2="50.165" width="0.1524" layer="21"/>
<wire x1="45.085" y1="52.07" x2="44.45" y2="51.435" width="0.1524" layer="21"/>
<wire x1="44.45" y1="50.165" x2="45.085" y2="49.53" width="0.1524" layer="21"/>
<wire x1="46.355" y1="49.53" x2="45.085" y2="49.53" width="0.1524" layer="21"/>
<wire x1="62.865" y1="52.07" x2="64.135" y2="52.07" width="0.1524" layer="21"/>
<wire x1="64.135" y1="52.07" x2="64.77" y2="51.435" width="0.1524" layer="21"/>
<wire x1="64.77" y1="51.435" x2="64.77" y2="50.165" width="0.1524" layer="21"/>
<wire x1="64.77" y1="50.165" x2="64.135" y2="49.53" width="0.1524" layer="21"/>
<wire x1="62.865" y1="52.07" x2="62.23" y2="51.435" width="0.1524" layer="21"/>
<wire x1="62.23" y1="50.165" x2="62.865" y2="49.53" width="0.1524" layer="21"/>
<wire x1="64.135" y1="49.53" x2="62.865" y2="49.53" width="0.1524" layer="21"/>
<wire x1="45.085" y1="3.81" x2="46.355" y2="3.81" width="0.1524" layer="21"/>
<wire x1="46.355" y1="3.81" x2="46.99" y2="3.175" width="0.1524" layer="21"/>
<wire x1="46.99" y1="1.905" x2="46.355" y2="1.27" width="0.1524" layer="21"/>
<wire x1="41.91" y1="3.175" x2="42.545" y2="3.81" width="0.1524" layer="21"/>
<wire x1="42.545" y1="3.81" x2="43.815" y2="3.81" width="0.1524" layer="21"/>
<wire x1="43.815" y1="3.81" x2="44.45" y2="3.175" width="0.1524" layer="21"/>
<wire x1="44.45" y1="3.175" x2="44.45" y2="1.905" width="0.1524" layer="21"/>
<wire x1="44.45" y1="1.905" x2="43.815" y2="1.27" width="0.1524" layer="21"/>
<wire x1="43.815" y1="1.27" x2="42.545" y2="1.27" width="0.1524" layer="21"/>
<wire x1="42.545" y1="1.27" x2="41.91" y2="1.905" width="0.1524" layer="21"/>
<wire x1="45.085" y1="3.81" x2="44.45" y2="3.175" width="0.1524" layer="21"/>
<wire x1="44.45" y1="1.905" x2="45.085" y2="1.27" width="0.1524" layer="21"/>
<wire x1="46.355" y1="1.27" x2="45.085" y2="1.27" width="0.1524" layer="21"/>
<wire x1="37.465" y1="3.81" x2="38.735" y2="3.81" width="0.1524" layer="21"/>
<wire x1="38.735" y1="3.81" x2="39.37" y2="3.175" width="0.1524" layer="21"/>
<wire x1="39.37" y1="3.175" x2="39.37" y2="1.905" width="0.1524" layer="21"/>
<wire x1="39.37" y1="1.905" x2="38.735" y2="1.27" width="0.1524" layer="21"/>
<wire x1="39.37" y1="3.175" x2="40.005" y2="3.81" width="0.1524" layer="21"/>
<wire x1="40.005" y1="3.81" x2="41.275" y2="3.81" width="0.1524" layer="21"/>
<wire x1="41.275" y1="3.81" x2="41.91" y2="3.175" width="0.1524" layer="21"/>
<wire x1="41.91" y1="3.175" x2="41.91" y2="1.905" width="0.1524" layer="21"/>
<wire x1="41.91" y1="1.905" x2="41.275" y2="1.27" width="0.1524" layer="21"/>
<wire x1="41.275" y1="1.27" x2="40.005" y2="1.27" width="0.1524" layer="21"/>
<wire x1="40.005" y1="1.27" x2="39.37" y2="1.905" width="0.1524" layer="21"/>
<wire x1="34.29" y1="3.175" x2="34.925" y2="3.81" width="0.1524" layer="21"/>
<wire x1="34.925" y1="3.81" x2="36.195" y2="3.81" width="0.1524" layer="21"/>
<wire x1="36.195" y1="3.81" x2="36.83" y2="3.175" width="0.1524" layer="21"/>
<wire x1="36.83" y1="3.175" x2="36.83" y2="1.905" width="0.1524" layer="21"/>
<wire x1="36.83" y1="1.905" x2="36.195" y2="1.27" width="0.1524" layer="21"/>
<wire x1="36.195" y1="1.27" x2="34.925" y2="1.27" width="0.1524" layer="21"/>
<wire x1="34.925" y1="1.27" x2="34.29" y2="1.905" width="0.1524" layer="21"/>
<wire x1="37.465" y1="3.81" x2="36.83" y2="3.175" width="0.1524" layer="21"/>
<wire x1="36.83" y1="1.905" x2="37.465" y2="1.27" width="0.1524" layer="21"/>
<wire x1="38.735" y1="1.27" x2="37.465" y2="1.27" width="0.1524" layer="21"/>
<wire x1="31.75" y1="3.175" x2="31.75" y2="1.905" width="0.1524" layer="21"/>
<wire x1="31.75" y1="3.175" x2="32.385" y2="3.81" width="0.1524" layer="21"/>
<wire x1="32.385" y1="3.81" x2="33.655" y2="3.81" width="0.1524" layer="21"/>
<wire x1="33.655" y1="3.81" x2="34.29" y2="3.175" width="0.1524" layer="21"/>
<wire x1="34.29" y1="3.175" x2="34.29" y2="1.905" width="0.1524" layer="21"/>
<wire x1="34.29" y1="1.905" x2="33.655" y2="1.27" width="0.1524" layer="21"/>
<wire x1="33.655" y1="1.27" x2="32.385" y2="1.27" width="0.1524" layer="21"/>
<wire x1="32.385" y1="1.27" x2="31.75" y2="1.905" width="0.1524" layer="21"/>
<wire x1="62.865" y1="3.81" x2="64.135" y2="3.81" width="0.1524" layer="21"/>
<wire x1="64.135" y1="3.81" x2="64.77" y2="3.175" width="0.1524" layer="21"/>
<wire x1="64.77" y1="3.175" x2="64.77" y2="1.905" width="0.1524" layer="21"/>
<wire x1="64.77" y1="1.905" x2="64.135" y2="1.27" width="0.1524" layer="21"/>
<wire x1="59.69" y1="3.175" x2="60.325" y2="3.81" width="0.1524" layer="21"/>
<wire x1="60.325" y1="3.81" x2="61.595" y2="3.81" width="0.1524" layer="21"/>
<wire x1="61.595" y1="3.81" x2="62.23" y2="3.175" width="0.1524" layer="21"/>
<wire x1="62.23" y1="3.175" x2="62.23" y2="1.905" width="0.1524" layer="21"/>
<wire x1="62.23" y1="1.905" x2="61.595" y2="1.27" width="0.1524" layer="21"/>
<wire x1="61.595" y1="1.27" x2="60.325" y2="1.27" width="0.1524" layer="21"/>
<wire x1="60.325" y1="1.27" x2="59.69" y2="1.905" width="0.1524" layer="21"/>
<wire x1="62.865" y1="3.81" x2="62.23" y2="3.175" width="0.1524" layer="21"/>
<wire x1="62.23" y1="1.905" x2="62.865" y2="1.27" width="0.1524" layer="21"/>
<wire x1="64.135" y1="1.27" x2="62.865" y2="1.27" width="0.1524" layer="21"/>
<wire x1="55.245" y1="3.81" x2="56.515" y2="3.81" width="0.1524" layer="21"/>
<wire x1="56.515" y1="3.81" x2="57.15" y2="3.175" width="0.1524" layer="21"/>
<wire x1="57.15" y1="3.175" x2="57.15" y2="1.905" width="0.1524" layer="21"/>
<wire x1="57.15" y1="1.905" x2="56.515" y2="1.27" width="0.1524" layer="21"/>
<wire x1="57.15" y1="3.175" x2="57.785" y2="3.81" width="0.1524" layer="21"/>
<wire x1="57.785" y1="3.81" x2="59.055" y2="3.81" width="0.1524" layer="21"/>
<wire x1="59.055" y1="3.81" x2="59.69" y2="3.175" width="0.1524" layer="21"/>
<wire x1="59.69" y1="3.175" x2="59.69" y2="1.905" width="0.1524" layer="21"/>
<wire x1="59.69" y1="1.905" x2="59.055" y2="1.27" width="0.1524" layer="21"/>
<wire x1="59.055" y1="1.27" x2="57.785" y2="1.27" width="0.1524" layer="21"/>
<wire x1="57.785" y1="1.27" x2="57.15" y2="1.905" width="0.1524" layer="21"/>
<wire x1="52.07" y1="3.175" x2="52.705" y2="3.81" width="0.1524" layer="21"/>
<wire x1="52.705" y1="3.81" x2="53.975" y2="3.81" width="0.1524" layer="21"/>
<wire x1="53.975" y1="3.81" x2="54.61" y2="3.175" width="0.1524" layer="21"/>
<wire x1="54.61" y1="3.175" x2="54.61" y2="1.905" width="0.1524" layer="21"/>
<wire x1="54.61" y1="1.905" x2="53.975" y2="1.27" width="0.1524" layer="21"/>
<wire x1="53.975" y1="1.27" x2="52.705" y2="1.27" width="0.1524" layer="21"/>
<wire x1="52.705" y1="1.27" x2="52.07" y2="1.905" width="0.1524" layer="21"/>
<wire x1="55.245" y1="3.81" x2="54.61" y2="3.175" width="0.1524" layer="21"/>
<wire x1="54.61" y1="1.905" x2="55.245" y2="1.27" width="0.1524" layer="21"/>
<wire x1="56.515" y1="1.27" x2="55.245" y2="1.27" width="0.1524" layer="21"/>
<wire x1="49.53" y1="3.175" x2="49.53" y2="1.905" width="0.1524" layer="21"/>
<wire x1="49.53" y1="3.175" x2="50.165" y2="3.81" width="0.1524" layer="21"/>
<wire x1="50.165" y1="3.81" x2="51.435" y2="3.81" width="0.1524" layer="21"/>
<wire x1="51.435" y1="3.81" x2="52.07" y2="3.175" width="0.1524" layer="21"/>
<wire x1="52.07" y1="3.175" x2="52.07" y2="1.905" width="0.1524" layer="21"/>
<wire x1="52.07" y1="1.905" x2="51.435" y2="1.27" width="0.1524" layer="21"/>
<wire x1="51.435" y1="1.27" x2="50.165" y2="1.27" width="0.1524" layer="21"/>
<wire x1="50.165" y1="1.27" x2="49.53" y2="1.905" width="0.1524" layer="21"/>
<wire x1="46.99" y1="3.175" x2="46.99" y2="1.905" width="0.1524" layer="21"/>
<pad name="28" x="23.876" y="50.8" drill="1.016" shape="long" rot="R90"/>
<pad name="27" x="26.416" y="50.8" drill="1.016" shape="long" rot="R90"/>
<pad name="26" x="28.956" y="50.8" drill="1.016" shape="long" rot="R90"/>
<pad name="25" x="31.496" y="50.8" drill="1.016" shape="long" rot="R90"/>
<pad name="24" x="34.036" y="50.8" drill="1.016" shape="long" rot="R90"/>
<pad name="23" x="36.576" y="50.8" drill="1.016" shape="long" rot="R90"/>
<pad name="22" x="39.116" y="50.8" drill="1.016" shape="long" rot="R90"/>
<pad name="21" x="41.656" y="50.8" drill="1.016" shape="long" rot="R90"/>
<pad name="20" x="45.72" y="50.8" drill="1.016" shape="long" rot="R90"/>
<pad name="19" x="48.26" y="50.8" drill="1.016" shape="long" rot="R90"/>
<pad name="18" x="50.8" y="50.8" drill="1.016" shape="long" rot="R90"/>
<pad name="17" x="53.34" y="50.8" drill="1.016" shape="long" rot="R90"/>
<pad name="16" x="55.88" y="50.8" drill="1.016" shape="long" rot="R90"/>
<pad name="15" x="58.42" y="50.8" drill="1.016" shape="long" rot="R90"/>
<pad name="14" x="60.96" y="50.8" drill="1.016" shape="long" rot="R90"/>
<pad name="13" x="63.5" y="50.8" drill="1.016" shape="long" rot="R90"/>
<pad name="1" x="33.02" y="2.54" drill="1.016" shape="long" rot="R90"/>
<pad name="2" x="35.56" y="2.54" drill="1.016" shape="long" rot="R90"/>
<pad name="3" x="38.1" y="2.54" drill="1.016" shape="long" rot="R90"/>
<pad name="4" x="40.64" y="2.54" drill="1.016" shape="long" rot="R90"/>
<pad name="5" x="43.18" y="2.54" drill="1.016" shape="long" rot="R90"/>
<pad name="6" x="45.72" y="2.54" drill="1.016" shape="long" rot="R90"/>
<pad name="7" x="50.8" y="2.54" drill="1.016" shape="long" rot="R90"/>
<pad name="8" x="53.34" y="2.54" drill="1.016" shape="long" rot="R90"/>
<pad name="9" x="55.88" y="2.54" drill="1.016" shape="long" rot="R90"/>
<pad name="10" x="58.42" y="2.54" drill="1.016" shape="long" rot="R90"/>
<pad name="11" x="60.96" y="2.54" drill="1.016" shape="long" rot="R90"/>
<pad name="12" x="63.5" y="2.54" drill="1.016" shape="long" rot="R90"/>
<text x="23.1184" y="48.7812" size="1.016" layer="48" font="vector" ratio="10" rot="R270">AREF</text>
<text x="25.654" y="48.768" size="1.016" layer="48" font="vector" ratio="10" rot="R270">GND</text>
<text x="28.194" y="48.768" size="1.016" layer="48" font="vector" ratio="10" rot="R270">PB5</text>
<text x="30.734" y="48.768" size="1.016" layer="48" font="vector" ratio="10" rot="R270">PB4</text>
<text x="33.274" y="48.768" size="1.016" layer="48" font="vector" ratio="10" rot="R270">PB3</text>
<text x="35.814" y="48.768" size="1.016" layer="48" font="vector" ratio="10" rot="R270">PB2</text>
<text x="38.354" y="48.768" size="1.016" layer="48" font="vector" ratio="10" rot="R270">PB1</text>
<text x="40.894" y="48.768" size="1.016" layer="48" font="vector" ratio="10" rot="R270">PB0</text>
<text x="44.958" y="48.768" size="1.016" layer="48" font="vector" ratio="10" rot="R270">PD7</text>
<text x="47.498" y="48.768" size="1.016" layer="48" font="vector" ratio="10" rot="R270">PD6</text>
<text x="50.038" y="48.768" size="1.016" layer="48" font="vector" ratio="10" rot="R270">PD5</text>
<text x="52.578" y="48.768" size="1.016" layer="48" font="vector" ratio="10" rot="R270">PD4</text>
<text x="55.118" y="48.768" size="1.016" layer="48" font="vector" ratio="10" rot="R270">PD3</text>
<text x="57.658" y="48.768" size="1.016" layer="48" font="vector" ratio="10" rot="R270">PD2</text>
<text x="60.198" y="48.768" size="1.016" layer="48" font="vector" ratio="10" rot="R270">PD1</text>
<text x="62.738" y="48.768" size="1.016" layer="48" font="vector" ratio="10" rot="R270">PD0</text>
<text x="33.7776" y="4.8128" size="1.016" layer="48" font="vector" ratio="10" rot="R90">!Reset</text>
<text x="36.3176" y="4.8128" size="1.016" layer="48" font="vector" ratio="10" rot="R90">+3V3</text>
<text x="38.8576" y="4.8128" size="1.016" layer="48" font="vector" ratio="10" rot="R90">+5V</text>
<text x="41.3976" y="4.8128" size="1.016" layer="48" font="vector" ratio="10" rot="R90">GND</text>
<text x="43.9376" y="4.8128" size="1.016" layer="48" font="vector" ratio="10" rot="R90">GND</text>
<text x="46.4776" y="4.8128" size="1.016" layer="48" font="vector" ratio="10" rot="R90">+Vin</text>
<text x="51.5576" y="4.8128" size="1.016" layer="48" font="vector" ratio="10" rot="R90">PC0</text>
<text x="54.0976" y="4.8128" size="1.016" layer="48" font="vector" ratio="10" rot="R90">PC1</text>
<text x="56.6376" y="4.8128" size="1.016" layer="48" font="vector" ratio="10" rot="R90">PC2</text>
<text x="59.1776" y="4.8128" size="1.016" layer="48" font="vector" ratio="10" rot="R90">PC3</text>
<text x="61.7176" y="4.8128" size="1.016" layer="48" font="vector" ratio="10" rot="R90">PC4</text>
<text x="64.2576" y="4.8128" size="1.016" layer="48" font="vector" ratio="10" rot="R90">PC5</text>
<rectangle x1="38.862" y1="50.546" x2="39.37" y2="51.054" layer="51"/>
<rectangle x1="36.322" y1="50.546" x2="36.83" y2="51.054" layer="51"/>
<rectangle x1="33.782" y1="50.546" x2="34.29" y2="51.054" layer="51"/>
<rectangle x1="31.242" y1="50.546" x2="31.75" y2="51.054" layer="51"/>
<rectangle x1="28.702" y1="50.546" x2="29.21" y2="51.054" layer="51"/>
<rectangle x1="26.162" y1="50.546" x2="26.67" y2="51.054" layer="51"/>
<rectangle x1="23.622" y1="50.546" x2="24.13" y2="51.054" layer="51"/>
<rectangle x1="41.402" y1="50.546" x2="41.91" y2="51.054" layer="51"/>
<rectangle x1="60.706" y1="50.546" x2="61.214" y2="51.054" layer="51"/>
<rectangle x1="58.166" y1="50.546" x2="58.674" y2="51.054" layer="51"/>
<rectangle x1="55.626" y1="50.546" x2="56.134" y2="51.054" layer="51"/>
<rectangle x1="53.086" y1="50.546" x2="53.594" y2="51.054" layer="51"/>
<rectangle x1="50.546" y1="50.546" x2="51.054" y2="51.054" layer="51"/>
<rectangle x1="48.006" y1="50.546" x2="48.514" y2="51.054" layer="51"/>
<rectangle x1="45.466" y1="50.546" x2="45.974" y2="51.054" layer="51"/>
<rectangle x1="63.246" y1="50.546" x2="63.754" y2="51.054" layer="51"/>
<rectangle x1="45.466" y1="2.286" x2="45.974" y2="2.794" layer="51"/>
<rectangle x1="42.926" y1="2.286" x2="43.434" y2="2.794" layer="51"/>
<rectangle x1="40.386" y1="2.286" x2="40.894" y2="2.794" layer="51"/>
<rectangle x1="37.846" y1="2.286" x2="38.354" y2="2.794" layer="51"/>
<rectangle x1="35.306" y1="2.286" x2="35.814" y2="2.794" layer="51"/>
<rectangle x1="32.766" y1="2.286" x2="33.274" y2="2.794" layer="51"/>
<rectangle x1="63.246" y1="2.286" x2="63.754" y2="2.794" layer="51"/>
<rectangle x1="60.706" y1="2.286" x2="61.214" y2="2.794" layer="51"/>
<rectangle x1="58.166" y1="2.286" x2="58.674" y2="2.794" layer="51"/>
<rectangle x1="55.626" y1="2.286" x2="56.134" y2="2.794" layer="51"/>
<rectangle x1="53.086" y1="2.286" x2="53.594" y2="2.794" layer="51"/>
<rectangle x1="50.546" y1="2.286" x2="51.054" y2="2.794" layer="51"/>
<hole x="15.24" y="50.8" drill="3.2"/>
<hole x="66.04" y="35.56" drill="3.2"/>
<hole x="66.04" y="7.62" drill="3.2"/>
</package>
</packages>
<symbols>
<symbol name="ARDUINO2009">
<wire x1="-12.7" y1="25.4" x2="12.7" y2="25.4" width="0.254" layer="94"/>
<wire x1="12.7" y1="25.4" x2="12.7" y2="-25.4" width="0.254" layer="94"/>
<wire x1="12.7" y1="-25.4" x2="-12.7" y2="-25.4" width="0.254" layer="94"/>
<wire x1="-12.7" y1="-25.4" x2="-12.7" y2="25.4" width="0.254" layer="94"/>
<text x="-12.7" y="26.67" size="1.6764" layer="95">&gt;Name</text>
<text x="-4.572" y="0.508" size="1.778" layer="94">Arduino</text>
<text x="-2.794" y="-2.794" size="1.778" layer="94">2009</text>
<pin name="13" x="17.78" y="-17.78" length="middle" rot="R180"/>
<pin name="12" x="17.78" y="-15.24" length="middle" rot="R180"/>
<pin name="11" x="17.78" y="-12.7" length="middle" rot="R180"/>
<pin name="10" x="17.78" y="-10.16" length="middle" rot="R180"/>
<pin name="9" x="17.78" y="-7.62" length="middle" rot="R180"/>
<pin name="8" x="17.78" y="-5.08" length="middle" rot="R180"/>
<pin name="7" x="17.78" y="0" length="middle" rot="R180"/>
<pin name="6" x="17.78" y="2.54" length="middle" rot="R180"/>
<pin name="5" x="17.78" y="5.08" length="middle" rot="R180"/>
<pin name="4" x="17.78" y="7.62" length="middle" rot="R180"/>
<pin name="3" x="17.78" y="10.16" length="middle" rot="R180"/>
<pin name="2" x="17.78" y="12.7" length="middle" rot="R180"/>
<pin name="1" x="17.78" y="15.24" length="middle" rot="R180"/>
<pin name="0" x="17.78" y="17.78" length="middle" rot="R180"/>
<pin name="GND@1" x="-5.08" y="-30.48" length="middle" direction="pwr" rot="R90"/>
<pin name="GND@2" x="0" y="-30.48" length="middle" direction="pwr" rot="R90"/>
<pin name="GND@3" x="5.08" y="-30.48" length="middle" direction="pwr" rot="R90"/>
<pin name="AREF" x="-17.78" y="10.16" length="middle"/>
<pin name="+5V" x="-5.08" y="30.48" length="middle" direction="pwr" rot="R270"/>
<pin name="+3V3" x="0" y="30.48" length="middle" direction="pwr" rot="R270"/>
<pin name="+VIN" x="5.08" y="30.48" length="middle" rot="R270"/>
<pin name="A0" x="-17.78" y="5.08" length="middle"/>
<pin name="A4" x="-17.78" y="-5.08" length="middle"/>
<pin name="A3" x="-17.78" y="-2.54" length="middle"/>
<pin name="A2" x="-17.78" y="0" length="middle"/>
<pin name="A1" x="-17.78" y="2.54" length="middle"/>
<pin name="A5" x="-17.78" y="-7.62" length="middle"/>
<pin name="!RESET" x="-17.78" y="17.78" length="middle" function="dot"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="ARDUINO2009_1" prefix="IC" uservalue="yes">
<description>The Arduino Duemilanove ("2009") is a microcontroller board based on the ATmega168  or ATmega328.&lt;br&gt;
- 14 digital input/output pins (of which 6 can be used as PWM outputs) &lt;br&gt;
- 6 analog inputs, a 16 MHz crystal oscillator&lt;br&gt;
- USB connection&lt;br&gt;
- power jack&lt;br&gt;
- ISP header&lt;br&gt;
- reset button &lt;br&gt;
&lt;br&gt;
It contains everything needed to support the microcontroller; simply connect it to a computer with a USB cable or power it with a AC-to-DC adapter or battery to get started
&lt;br&gt;&lt;br&gt;
http://arduino.cc/en/Main/ArduinoBoardDuemilanove</description>
<gates>
<gate name="G$1" symbol="ARDUINO2009" x="0" y="0" addlevel="must"/>
</gates>
<devices>
<device name="0" package="ARDUINO2009">
<connects>
<connect gate="G$1" pin="!RESET" pad="1"/>
<connect gate="G$1" pin="+3V3" pad="2"/>
<connect gate="G$1" pin="+5V" pad="3"/>
<connect gate="G$1" pin="+VIN" pad="6"/>
<connect gate="G$1" pin="0" pad="13"/>
<connect gate="G$1" pin="1" pad="14"/>
<connect gate="G$1" pin="10" pad="23"/>
<connect gate="G$1" pin="11" pad="24"/>
<connect gate="G$1" pin="12" pad="25"/>
<connect gate="G$1" pin="13" pad="26"/>
<connect gate="G$1" pin="2" pad="15"/>
<connect gate="G$1" pin="3" pad="16"/>
<connect gate="G$1" pin="4" pad="17"/>
<connect gate="G$1" pin="5" pad="18"/>
<connect gate="G$1" pin="6" pad="19"/>
<connect gate="G$1" pin="7" pad="20"/>
<connect gate="G$1" pin="8" pad="21"/>
<connect gate="G$1" pin="9" pad="22"/>
<connect gate="G$1" pin="A0" pad="7"/>
<connect gate="G$1" pin="A1" pad="8"/>
<connect gate="G$1" pin="A2" pad="9"/>
<connect gate="G$1" pin="A3" pad="10"/>
<connect gate="G$1" pin="A4" pad="11"/>
<connect gate="G$1" pin="A5" pad="12"/>
<connect gate="G$1" pin="AREF" pad="28"/>
<connect gate="G$1" pin="GND@1" pad="4"/>
<connect gate="G$1" pin="GND@2" pad="5"/>
<connect gate="G$1" pin="GND@3" pad="27"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1" package="ARDUINO2009_1">
<connects>
<connect gate="G$1" pin="!RESET" pad="1"/>
<connect gate="G$1" pin="+3V3" pad="2"/>
<connect gate="G$1" pin="+5V" pad="3"/>
<connect gate="G$1" pin="+VIN" pad="6"/>
<connect gate="G$1" pin="0" pad="13"/>
<connect gate="G$1" pin="1" pad="14"/>
<connect gate="G$1" pin="10" pad="23"/>
<connect gate="G$1" pin="11" pad="24"/>
<connect gate="G$1" pin="12" pad="25"/>
<connect gate="G$1" pin="13" pad="26"/>
<connect gate="G$1" pin="2" pad="15"/>
<connect gate="G$1" pin="3" pad="16"/>
<connect gate="G$1" pin="4" pad="17"/>
<connect gate="G$1" pin="5" pad="18"/>
<connect gate="G$1" pin="6" pad="19"/>
<connect gate="G$1" pin="7" pad="20"/>
<connect gate="G$1" pin="8" pad="21"/>
<connect gate="G$1" pin="9" pad="22"/>
<connect gate="G$1" pin="A0" pad="7"/>
<connect gate="G$1" pin="A1" pad="8"/>
<connect gate="G$1" pin="A2" pad="9"/>
<connect gate="G$1" pin="A3" pad="10"/>
<connect gate="G$1" pin="A4" pad="11"/>
<connect gate="G$1" pin="A5" pad="12"/>
<connect gate="G$1" pin="AREF" pad="28"/>
<connect gate="G$1" pin="GND@1" pad="4"/>
<connect gate="G$1" pin="GND@2" pad="5"/>
<connect gate="G$1" pin="GND@3" pad="27"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="2" package="ARDUINO2009_2">
<connects>
<connect gate="G$1" pin="!RESET" pad="1"/>
<connect gate="G$1" pin="+3V3" pad="2"/>
<connect gate="G$1" pin="+5V" pad="3"/>
<connect gate="G$1" pin="+VIN" pad="6"/>
<connect gate="G$1" pin="0" pad="13"/>
<connect gate="G$1" pin="1" pad="14"/>
<connect gate="G$1" pin="10" pad="23"/>
<connect gate="G$1" pin="11" pad="24"/>
<connect gate="G$1" pin="12" pad="25"/>
<connect gate="G$1" pin="13" pad="26"/>
<connect gate="G$1" pin="2" pad="15"/>
<connect gate="G$1" pin="3" pad="16"/>
<connect gate="G$1" pin="4" pad="17"/>
<connect gate="G$1" pin="5" pad="18"/>
<connect gate="G$1" pin="6" pad="19"/>
<connect gate="G$1" pin="7" pad="20"/>
<connect gate="G$1" pin="8" pad="21"/>
<connect gate="G$1" pin="9" pad="22"/>
<connect gate="G$1" pin="A0" pad="7"/>
<connect gate="G$1" pin="A1" pad="8"/>
<connect gate="G$1" pin="A2" pad="9"/>
<connect gate="G$1" pin="A3" pad="10"/>
<connect gate="G$1" pin="A4" pad="11"/>
<connect gate="G$1" pin="A5" pad="12"/>
<connect gate="G$1" pin="AREF" pad="28"/>
<connect gate="G$1" pin="GND@1" pad="4"/>
<connect gate="G$1" pin="GND@2" pad="5"/>
<connect gate="G$1" pin="GND@3" pad="27"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="keypad_4x4_raw">
<description>4x4 Membrane Keypad</description>
<packages>
<package name="KEYPAD4X4">
<wire x1="38.415" y1="-1.27" x2="39.685" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="39.685" y1="-1.27" x2="40.32" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="40.32" y1="-1.905" x2="40.32" y2="-3.175" width="0.1524" layer="21"/>
<wire x1="40.32" y1="-3.175" x2="39.685" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="35.24" y1="-1.905" x2="35.875" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="35.875" y1="-1.27" x2="37.145" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="37.145" y1="-1.27" x2="37.78" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="37.78" y1="-1.905" x2="37.78" y2="-3.175" width="0.1524" layer="21"/>
<wire x1="37.78" y1="-3.175" x2="37.145" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="37.145" y1="-3.81" x2="35.875" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="35.875" y1="-3.81" x2="35.24" y2="-3.175" width="0.1524" layer="21"/>
<wire x1="38.415" y1="-1.27" x2="37.78" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="37.78" y1="-3.175" x2="38.415" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="39.685" y1="-3.81" x2="38.415" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="30.795" y1="-1.27" x2="32.065" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="32.065" y1="-1.27" x2="32.7" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="32.7" y1="-1.905" x2="32.7" y2="-3.175" width="0.1524" layer="21"/>
<wire x1="32.7" y1="-3.175" x2="32.065" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="32.7" y1="-1.905" x2="33.335" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="33.335" y1="-1.27" x2="34.605" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="34.605" y1="-1.27" x2="35.24" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="35.24" y1="-1.905" x2="35.24" y2="-3.175" width="0.1524" layer="21"/>
<wire x1="35.24" y1="-3.175" x2="34.605" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="34.605" y1="-3.81" x2="33.335" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="33.335" y1="-3.81" x2="32.7" y2="-3.175" width="0.1524" layer="21"/>
<wire x1="27.62" y1="-1.905" x2="28.255" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="28.255" y1="-1.27" x2="29.525" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="29.525" y1="-1.27" x2="30.16" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="30.16" y1="-1.905" x2="30.16" y2="-3.175" width="0.1524" layer="21"/>
<wire x1="30.16" y1="-3.175" x2="29.525" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="29.525" y1="-3.81" x2="28.255" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="28.255" y1="-3.81" x2="27.62" y2="-3.175" width="0.1524" layer="21"/>
<wire x1="30.795" y1="-1.27" x2="30.16" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="30.16" y1="-3.175" x2="30.795" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="32.065" y1="-3.81" x2="30.795" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="23.175" y1="-1.27" x2="24.445" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="24.445" y1="-1.27" x2="25.08" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="25.08" y1="-1.905" x2="25.08" y2="-3.175" width="0.1524" layer="21"/>
<wire x1="25.08" y1="-3.175" x2="24.445" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="25.08" y1="-1.905" x2="25.715" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="25.715" y1="-1.27" x2="26.985" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="26.985" y1="-1.27" x2="27.62" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="27.62" y1="-1.905" x2="27.62" y2="-3.175" width="0.1524" layer="21"/>
<wire x1="27.62" y1="-3.175" x2="26.985" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="26.985" y1="-3.81" x2="25.715" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="25.715" y1="-3.81" x2="25.08" y2="-3.175" width="0.1524" layer="21"/>
<wire x1="22.54" y1="-1.905" x2="22.54" y2="-3.175" width="0.1524" layer="21"/>
<wire x1="23.175" y1="-1.27" x2="22.54" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="22.54" y1="-3.175" x2="23.175" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="24.445" y1="-3.81" x2="23.175" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="40.955" y1="-1.27" x2="42.225" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="42.225" y1="-1.27" x2="42.86" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="42.86" y1="-1.905" x2="42.86" y2="-3.175" width="0.1524" layer="21"/>
<wire x1="42.86" y1="-3.175" x2="42.225" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="40.955" y1="-1.27" x2="40.32" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="40.32" y1="-3.175" x2="40.955" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="42.225" y1="-3.81" x2="40.955" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="64.67" y1="64.94" x2="0" y2="64.94" width="0.127" layer="21"/>
<wire x1="0" y1="64.94" x2="0" y2="0" width="0.127" layer="21"/>
<wire x1="0" y1="0" x2="64.67" y2="0" width="0.127" layer="21"/>
<wire x1="64.67" y1="0" x2="64.67" y2="64.94" width="0.127" layer="21"/>
<pad name="1" x="23.81" y="-2.54" drill="1.016" shape="long" rot="R90"/>
<pad name="2" x="26.35" y="-2.54" drill="1.016" shape="long" rot="R90"/>
<pad name="3" x="28.89" y="-2.54" drill="1.016" shape="long" rot="R90"/>
<pad name="4" x="31.43" y="-2.54" drill="1.016" shape="long" rot="R90"/>
<pad name="5" x="33.97" y="-2.54" drill="1.016" shape="long" rot="R90"/>
<pad name="6" x="36.51" y="-2.54" drill="1.016" shape="long" rot="R90"/>
<pad name="7" x="39.05" y="-2.54" drill="1.016" shape="long" rot="R90"/>
<pad name="8" x="41.59" y="-2.54" drill="1.016" shape="long" rot="R90"/>
<text x="8.7338" y="-2.4712" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="50.97" y="-2.665" size="1.27" layer="27">&gt;VALUE</text>
<text x="33.97" y="-6.35" size="1.27" layer="21" align="bottom-center">r1</text>
<text x="36.51" y="-6.35" size="1.27" layer="21" align="bottom-center">r2</text>
<text x="39.05" y="-6.35" size="1.27" layer="21" align="bottom-center">r3</text>
<text x="41.59" y="-6.35" size="1.27" layer="21" align="bottom-center">r4</text>
<text x="23.81" y="-6.35" size="1.27" layer="21" align="bottom-center">c1</text>
<text x="26.35" y="-6.35" size="1.27" layer="21" align="bottom-center">c2</text>
<text x="28.89" y="-6.35" size="1.27" layer="21" align="bottom-center">c3</text>
<text x="31.43" y="-6.35" size="1.27" layer="21" align="bottom-center">c4</text>
<rectangle x1="41.336" y1="-2.794" x2="41.844" y2="-2.286" layer="51"/>
<rectangle x1="38.796" y1="-2.794" x2="39.304" y2="-2.286" layer="51"/>
<rectangle x1="36.256" y1="-2.794" x2="36.764" y2="-2.286" layer="51"/>
<rectangle x1="33.716" y1="-2.794" x2="34.224" y2="-2.286" layer="51"/>
<rectangle x1="31.176" y1="-2.794" x2="31.684" y2="-2.286" layer="51"/>
<rectangle x1="28.636" y1="-2.794" x2="29.144" y2="-2.286" layer="51"/>
<rectangle x1="26.096" y1="-2.794" x2="26.604" y2="-2.286" layer="51"/>
<rectangle x1="23.556" y1="-2.794" x2="24.064" y2="-2.286" layer="51"/>
<wire x1="7" y1="58" x2="15" y2="58" width="0.127" layer="21"/>
<wire x1="15" y1="58" x2="15" y2="50" width="0.127" layer="21"/>
<wire x1="15" y1="50" x2="7" y2="50" width="0.127" layer="21"/>
<wire x1="36" y1="58" x2="44" y2="58" width="0.127" layer="21"/>
<wire x1="44" y1="58" x2="44" y2="50" width="0.127" layer="21"/>
<wire x1="44" y1="50" x2="36" y2="50" width="0.127" layer="21"/>
<wire x1="36" y1="50" x2="36" y2="58" width="0.127" layer="21"/>
<wire x1="50" y1="58" x2="58" y2="58" width="0.127" layer="21"/>
<wire x1="58" y1="58" x2="58" y2="50" width="0.127" layer="21"/>
<wire x1="58" y1="50" x2="50" y2="50" width="0.127" layer="21"/>
<wire x1="50" y1="50" x2="50" y2="58" width="0.127" layer="21"/>
<wire x1="7" y1="50" x2="7" y2="58" width="0.127" layer="21"/>
<wire x1="21" y1="58" x2="29" y2="58" width="0.127" layer="21"/>
<wire x1="29" y1="58" x2="29" y2="50" width="0.127" layer="21"/>
<wire x1="29" y1="50" x2="21" y2="50" width="0.127" layer="21"/>
<wire x1="21" y1="50" x2="21" y2="58" width="0.127" layer="21"/>
<wire x1="7" y1="15" x2="15" y2="15" width="0.127" layer="21"/>
<wire x1="15" y1="15" x2="15" y2="7" width="0.127" layer="21"/>
<wire x1="15" y1="7" x2="7" y2="7" width="0.127" layer="21"/>
<wire x1="36" y1="15" x2="44" y2="15" width="0.127" layer="21"/>
<wire x1="44" y1="15" x2="44" y2="7" width="0.127" layer="21"/>
<wire x1="44" y1="7" x2="36" y2="7" width="0.127" layer="21"/>
<wire x1="36" y1="7" x2="36" y2="15" width="0.127" layer="21"/>
<wire x1="50" y1="15" x2="58" y2="15" width="0.127" layer="21"/>
<wire x1="58" y1="15" x2="58" y2="7" width="0.127" layer="21"/>
<wire x1="58" y1="7" x2="50" y2="7" width="0.127" layer="21"/>
<wire x1="50" y1="7" x2="50" y2="15" width="0.127" layer="21"/>
<wire x1="7" y1="7" x2="7" y2="15" width="0.127" layer="21"/>
<wire x1="21" y1="15" x2="29" y2="15" width="0.127" layer="21"/>
<wire x1="29" y1="15" x2="29" y2="7" width="0.127" layer="21"/>
<wire x1="29" y1="7" x2="21" y2="7" width="0.127" layer="21"/>
<wire x1="21" y1="7" x2="21" y2="15" width="0.127" layer="21"/>
<wire x1="7" y1="29" x2="15" y2="29" width="0.127" layer="21"/>
<wire x1="15" y1="29" x2="15" y2="21" width="0.127" layer="21"/>
<wire x1="15" y1="21" x2="7" y2="21" width="0.127" layer="21"/>
<wire x1="36" y1="29" x2="44" y2="29" width="0.127" layer="21"/>
<wire x1="44" y1="29" x2="44" y2="21" width="0.127" layer="21"/>
<wire x1="44" y1="21" x2="36" y2="21" width="0.127" layer="21"/>
<wire x1="36" y1="21" x2="36" y2="29" width="0.127" layer="21"/>
<wire x1="50" y1="29" x2="58" y2="29" width="0.127" layer="21"/>
<wire x1="58" y1="29" x2="58" y2="21" width="0.127" layer="21"/>
<wire x1="58" y1="21" x2="50" y2="21" width="0.127" layer="21"/>
<wire x1="50" y1="21" x2="50" y2="29" width="0.127" layer="21"/>
<wire x1="7" y1="21" x2="7" y2="29" width="0.127" layer="21"/>
<wire x1="21" y1="29" x2="29" y2="29" width="0.127" layer="21"/>
<wire x1="29" y1="29" x2="29" y2="21" width="0.127" layer="21"/>
<wire x1="29" y1="21" x2="21" y2="21" width="0.127" layer="21"/>
<wire x1="21" y1="21" x2="21" y2="29" width="0.127" layer="21"/>
<wire x1="7" y1="44" x2="15" y2="44" width="0.127" layer="21"/>
<wire x1="15" y1="44" x2="15" y2="36" width="0.127" layer="21"/>
<wire x1="15" y1="36" x2="7" y2="36" width="0.127" layer="21"/>
<wire x1="36" y1="44" x2="44" y2="44" width="0.127" layer="21"/>
<wire x1="44" y1="44" x2="44" y2="36" width="0.127" layer="21"/>
<wire x1="44" y1="36" x2="36" y2="36" width="0.127" layer="21"/>
<wire x1="36" y1="36" x2="36" y2="44" width="0.127" layer="21"/>
<wire x1="50" y1="44" x2="58" y2="44" width="0.127" layer="21"/>
<wire x1="58" y1="44" x2="58" y2="36" width="0.127" layer="21"/>
<wire x1="58" y1="36" x2="50" y2="36" width="0.127" layer="21"/>
<wire x1="50" y1="36" x2="50" y2="44" width="0.127" layer="21"/>
<wire x1="7" y1="36" x2="7" y2="44" width="0.127" layer="21"/>
<wire x1="21" y1="44" x2="29" y2="44" width="0.127" layer="21"/>
<wire x1="29" y1="44" x2="29" y2="36" width="0.127" layer="21"/>
<wire x1="29" y1="36" x2="21" y2="36" width="0.127" layer="21"/>
<wire x1="21" y1="36" x2="21" y2="44" width="0.127" layer="21"/>
<text x="52" y="52" size="4" layer="21">A</text>
<text x="52" y="38" size="4" layer="21">B</text>
<text x="52" y="23" size="4" layer="21">C</text>
<text x="52" y="9" size="4" layer="21">D</text>
<text x="40" y="9" size="4.5" layer="21" align="bottom-center">#</text>
<text x="24" y="9" size="4" layer="21">0</text>
<text x="11" y="8" size="9" layer="21" align="center">*</text>
<text x="10" y="52" size="4" layer="21">1</text>
<text x="24" y="52" size="4" layer="21">2</text>
<text x="39" y="52" size="4" layer="21">3</text>
<text x="39" y="38" size="4" layer="21">6</text>
<text x="24" y="38" size="4" layer="21">5</text>
<text x="10" y="38" size="4" layer="21">4</text>
<text x="10" y="23" size="4" layer="21">7</text>
<text x="24" y="23" size="4" layer="21">8</text>
<text x="39" y="23" size="4" layer="21">9</text>
</package>
</packages>
<symbols>
<symbol name="KEYPAD4X4">
<wire x1="-17.78" y1="12.7" x2="-17.78" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-17.78" y1="-2.54" x2="-2.54" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-2.54" x2="-2.54" y2="12.7" width="0.254" layer="94"/>
<wire x1="-2.54" y1="12.7" x2="-17.78" y2="12.7" width="0.254" layer="94"/>
<wire x1="-15.24" y1="10.16" x2="-15.24" y2="7.62" width="0.254" layer="94"/>
<wire x1="-15.24" y1="7.62" x2="-15.24" y2="5.08" width="0.254" layer="94"/>
<wire x1="-15.24" y1="5.08" x2="-15.24" y2="2.54" width="0.254" layer="94"/>
<wire x1="-15.24" y1="2.54" x2="-15.24" y2="0" width="0.254" layer="94"/>
<wire x1="-15.24" y1="0" x2="-12.7" y2="0" width="0.254" layer="94"/>
<wire x1="-12.7" y1="0" x2="-10.16" y2="0" width="0.254" layer="94"/>
<wire x1="-10.16" y1="0" x2="-7.62" y2="0" width="0.254" layer="94"/>
<wire x1="-7.62" y1="0" x2="-5.08" y2="0" width="0.254" layer="94"/>
<wire x1="-5.08" y1="0" x2="-5.08" y2="2.54" width="0.254" layer="94"/>
<wire x1="-5.08" y1="2.54" x2="-5.08" y2="5.08" width="0.254" layer="94"/>
<wire x1="-5.08" y1="5.08" x2="-5.08" y2="7.62" width="0.254" layer="94"/>
<wire x1="-5.08" y1="7.62" x2="-5.08" y2="10.16" width="0.254" layer="94"/>
<wire x1="-5.08" y1="10.16" x2="-7.62" y2="10.16" width="0.254" layer="94"/>
<wire x1="-7.62" y1="10.16" x2="-10.16" y2="10.16" width="0.254" layer="94"/>
<wire x1="-10.16" y1="10.16" x2="-12.7" y2="10.16" width="0.254" layer="94"/>
<wire x1="-12.7" y1="10.16" x2="-15.24" y2="10.16" width="0.254" layer="94"/>
<wire x1="-12.7" y1="10.16" x2="-12.7" y2="0" width="0.254" layer="94"/>
<wire x1="-10.16" y1="10.16" x2="-10.16" y2="0" width="0.254" layer="94"/>
<wire x1="-7.62" y1="10.16" x2="-7.62" y2="0" width="0.254" layer="94"/>
<wire x1="-15.24" y1="7.62" x2="-5.08" y2="7.62" width="0.254" layer="94"/>
<wire x1="-15.24" y1="5.08" x2="-5.08" y2="5.08" width="0.254" layer="94"/>
<wire x1="-15.24" y1="2.54" x2="-5.08" y2="2.54" width="0.254" layer="94"/>
<text x="-15.24" y="7.62" size="1.778" layer="94">1</text>
<text x="-12.7" y="7.62" size="1.778" layer="94">2</text>
<text x="-10.16" y="7.62" size="1.778" layer="94">3</text>
<text x="-15.24" y="5.08" size="1.778" layer="94">4</text>
<text x="-12.7" y="5.08" size="1.778" layer="94">5</text>
<text x="-10.16" y="5.08" size="1.778" layer="94">6</text>
<text x="-15.24" y="2.54" size="1.778" layer="94">7</text>
<text x="-12.7" y="2.54" size="1.778" layer="94">8</text>
<text x="-10.16" y="2.54" size="1.778" layer="94">9</text>
<text x="-15.24" y="0" size="1.778" layer="94">*</text>
<text x="-12.7" y="0" size="1.778" layer="94">0</text>
<text x="-10.16" y="0" size="1.778" layer="94">#</text>
<text x="-7.62" y="7.62" size="1.778" layer="94">A</text>
<text x="-7.62" y="5.08" size="1.778" layer="94">B</text>
<text x="-7.62" y="2.54" size="1.778" layer="94">C</text>
<text x="-7.62" y="0" size="1.778" layer="94">D</text>
<text x="-17.78" y="15.24" size="1.27" layer="95">&gt;NAME</text>
<text x="-10.16" y="15.24" size="1.27" layer="96">&gt;VALUE</text>
<pin name="R1" x="2.54" y="10.16" length="middle" rot="R180"/>
<pin name="R2" x="2.54" y="7.62" length="middle" rot="R180"/>
<pin name="R3" x="2.54" y="5.08" length="middle" rot="R180"/>
<pin name="R4" x="2.54" y="2.54" length="middle" rot="R180"/>
<pin name="C1" x="-15.24" y="-7.62" length="middle" rot="R90"/>
<pin name="C2" x="-12.7" y="-7.62" length="middle" rot="R90"/>
<pin name="C3" x="-10.16" y="-7.62" length="middle" rot="R90"/>
<pin name="C4" x="-7.62" y="-7.62" length="middle" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="KEYPAD4X4" uservalue="yes">
<gates>
<gate name="G$1" symbol="KEYPAD4X4" x="0" y="7.62"/>
</gates>
<devices>
<device name="" package="KEYPAD4X4">
<connects>
<connect gate="G$1" pin="C1" pad="1"/>
<connect gate="G$1" pin="C2" pad="2"/>
<connect gate="G$1" pin="C3" pad="3"/>
<connect gate="G$1" pin="C4" pad="4"/>
<connect gate="G$1" pin="R1" pad="5"/>
<connect gate="G$1" pin="R2" pad="6"/>
<connect gate="G$1" pin="R3" pad="7"/>
<connect gate="G$1" pin="R4" pad="8"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="display-lcd" urn="urn:adsk.eagle:library:214">
<description>&lt;b&gt;Hitachi, Data Modul, Tuxgraphics - LCD Displays&lt;/b&gt;&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="TUXGR_20X2" urn="urn:adsk.eagle:footprint:13122/1" library_version="2">
<description>&lt;b&gt;Tuxgraphics LCD display 20x2 characters&lt;/b&gt; reflective, without background light&lt;p&gt;
Source: tuxgr_20x2.pdf</description>
<wire x1="-57.9" y1="18.4" x2="57.9" y2="18.4" width="0.2032" layer="21"/>
<wire x1="57.9" y1="18.4" x2="57.9" y2="-18.4" width="0.2032" layer="21"/>
<wire x1="57.9" y1="-18.4" x2="-57.9" y2="-18.4" width="0.2032" layer="21"/>
<wire x1="-57.9" y1="-18.4" x2="-57.9" y2="18.4" width="0.2032" layer="21"/>
<wire x1="-40.4" y1="9.15" x2="42.4" y2="9.15" width="0.2032" layer="21"/>
<wire x1="42.4" y1="9.15" x2="42.4" y2="-9.15" width="0.2032" layer="21"/>
<wire x1="42.4" y1="-9.15" x2="-40.4" y2="-9.15" width="0.2032" layer="21"/>
<wire x1="-40.4" y1="-9.15" x2="-40.4" y2="9.15" width="0.2032" layer="21"/>
<wire x1="-44.5" y1="15.5" x2="46.5" y2="15.5" width="0.2032" layer="21"/>
<wire x1="46.5" y1="15.5" x2="46.5" y2="-15.5" width="0.2032" layer="21"/>
<wire x1="46.5" y1="-15.5" x2="-44.5" y2="-15.5" width="0.2032" layer="21"/>
<wire x1="-44.5" y1="-15.5" x2="-44.5" y2="15.5" width="0.2032" layer="21"/>
<pad name="1" x="-52.73" y="-7.62" drill="1" diameter="1.6764"/>
<pad name="2" x="-55.27" y="-7.62" drill="1" diameter="1.6764"/>
<pad name="3" x="-52.73" y="-5.08" drill="1" diameter="1.6764"/>
<pad name="4" x="-55.27" y="-5.08" drill="1" diameter="1.6764"/>
<pad name="5" x="-52.73" y="-2.54" drill="1" diameter="1.6764"/>
<pad name="6" x="-55.27" y="-2.54" drill="1" diameter="1.6764"/>
<pad name="7" x="-52.73" y="0" drill="1" diameter="1.6764"/>
<pad name="8" x="-55.27" y="0" drill="1" diameter="1.6764"/>
<pad name="9" x="-52.73" y="2.54" drill="1" diameter="1.6764"/>
<pad name="10" x="-55.27" y="2.54" drill="1" diameter="1.6764"/>
<pad name="11" x="-52.73" y="5.08" drill="1" diameter="1.6764"/>
<pad name="12" x="-55.27" y="5.08" drill="1" diameter="1.6764"/>
<pad name="13" x="-52.73" y="7.62" drill="1" diameter="1.6764"/>
<pad name="14" x="-55.27" y="7.62" drill="1" diameter="1.6764"/>
<pad name="15" x="-52.73" y="10.16" drill="1" diameter="1.6764"/>
<pad name="16" x="-55.27" y="10.16" drill="1" diameter="1.6764"/>
<text x="-53.34" y="-20.32" size="1.27" layer="25">&gt;NAME</text>
<text x="-43.18" y="-20.32" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-35.75" y1="0.2" x2="-32.55" y2="5.75" layer="21"/>
<rectangle x1="-35.75" y1="-5.75" x2="-32.55" y2="-0.2" layer="21"/>
<rectangle x1="-32.05" y1="0.2" x2="-28.85" y2="5.75" layer="21"/>
<rectangle x1="-32.05" y1="-5.75" x2="-28.85" y2="-0.2" layer="21"/>
<rectangle x1="-28.35" y1="0.2" x2="-25.15" y2="5.75" layer="21"/>
<rectangle x1="-28.35" y1="-5.75" x2="-25.15" y2="-0.2" layer="21"/>
<rectangle x1="-24.65" y1="0.2" x2="-21.45" y2="5.75" layer="21"/>
<rectangle x1="-24.65" y1="-5.75" x2="-21.45" y2="-0.2" layer="21"/>
<rectangle x1="-20.95" y1="0.2" x2="-17.75" y2="5.75" layer="21"/>
<rectangle x1="-20.95" y1="-5.75" x2="-17.75" y2="-0.2" layer="21"/>
<rectangle x1="-17.25" y1="0.2" x2="-14.05" y2="5.75" layer="21"/>
<rectangle x1="-17.25" y1="-5.75" x2="-14.05" y2="-0.2" layer="21"/>
<rectangle x1="-13.55" y1="0.2" x2="-10.35" y2="5.75" layer="21"/>
<rectangle x1="-13.55" y1="-5.75" x2="-10.35" y2="-0.2" layer="21"/>
<rectangle x1="-9.85" y1="0.2" x2="-6.65" y2="5.75" layer="21"/>
<rectangle x1="-9.85" y1="-5.75" x2="-6.65" y2="-0.2" layer="21"/>
<rectangle x1="-6.15" y1="0.2" x2="-2.95" y2="5.75" layer="21"/>
<rectangle x1="-6.15" y1="-5.75" x2="-2.95" y2="-0.2" layer="21"/>
<rectangle x1="-2.45" y1="0.2" x2="0.75" y2="5.75" layer="21"/>
<rectangle x1="-2.45" y1="-5.75" x2="0.75" y2="-0.2" layer="21"/>
<rectangle x1="1.25" y1="0.2" x2="4.45" y2="5.75" layer="21"/>
<rectangle x1="1.25" y1="-5.75" x2="4.45" y2="-0.2" layer="21"/>
<rectangle x1="4.95" y1="0.2" x2="8.15" y2="5.75" layer="21"/>
<rectangle x1="4.95" y1="-5.75" x2="8.15" y2="-0.2" layer="21"/>
<rectangle x1="8.65" y1="0.2" x2="11.85" y2="5.75" layer="21"/>
<rectangle x1="8.65" y1="-5.75" x2="11.85" y2="-0.2" layer="21"/>
<rectangle x1="12.35" y1="0.2" x2="15.55" y2="5.75" layer="21"/>
<rectangle x1="12.35" y1="-5.75" x2="15.55" y2="-0.2" layer="21"/>
<rectangle x1="16.05" y1="0.2" x2="19.25" y2="5.75" layer="21"/>
<rectangle x1="16.05" y1="-5.75" x2="19.25" y2="-0.2" layer="21"/>
<rectangle x1="19.75" y1="0.2" x2="22.95" y2="5.75" layer="21"/>
<rectangle x1="19.75" y1="-5.75" x2="22.95" y2="-0.2" layer="21"/>
<rectangle x1="23.45" y1="0.2" x2="26.65" y2="5.75" layer="21"/>
<rectangle x1="23.45" y1="-5.75" x2="26.65" y2="-0.2" layer="21"/>
<rectangle x1="27.15" y1="0.2" x2="30.35" y2="5.75" layer="21"/>
<rectangle x1="27.15" y1="-5.75" x2="30.35" y2="-0.2" layer="21"/>
<rectangle x1="30.85" y1="0.2" x2="34.05" y2="5.75" layer="21"/>
<rectangle x1="30.85" y1="-5.75" x2="34.05" y2="-0.2" layer="21"/>
<rectangle x1="34.55" y1="0.2" x2="37.75" y2="5.75" layer="21"/>
<rectangle x1="34.55" y1="-5.75" x2="37.75" y2="-0.2" layer="21"/>
<hole x="-54" y="-14.5" drill="3.5"/>
<hole x="54" y="-14.5" drill="3.5"/>
<hole x="-54" y="14.5" drill="3.5"/>
<hole x="54" y="14.5" drill="3.5"/>
</package>
</packages>
<packages3d>
<package3d name="TUXGR_20X2" urn="urn:adsk.eagle:package:13138/1" type="box" library_version="2">
<description>Tuxgraphics LCD display 20x2 characters reflective, without background light
Source: tuxgr_20x2.pdf</description>
<packageinstances>
<packageinstance name="TUXGR_20X2"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="TUXGR_20X2" urn="urn:adsk.eagle:symbol:13121/1" library_version="2">
<wire x1="-26.67" y1="-7.62" x2="26.67" y2="-7.62" width="0.2032" layer="94"/>
<wire x1="26.67" y1="-7.62" x2="26.67" y2="12.7" width="0.2032" layer="94"/>
<wire x1="26.67" y1="12.7" x2="-26.67" y2="12.7" width="0.2032" layer="94"/>
<wire x1="-26.67" y1="12.7" x2="-26.67" y2="-7.62" width="0.2032" layer="94"/>
<text x="-17.78" y="10.668" size="1.524" layer="94">LCD DISPLAY 20x2</text>
<text x="-25.4" y="13.97" size="1.778" layer="95">&gt;NAME</text>
<text x="-10.16" y="13.97" size="1.778" layer="96">&gt;VALUE</text>
<rectangle x1="-25.4" y1="6.604" x2="-23.114" y2="10.16" layer="94"/>
<rectangle x1="-25.4" y1="2.54" x2="-23.114" y2="6.096" layer="94"/>
<rectangle x1="-22.86" y1="6.604" x2="-20.574" y2="10.16" layer="94"/>
<rectangle x1="-22.86" y1="2.54" x2="-20.574" y2="6.096" layer="94"/>
<rectangle x1="-20.32" y1="6.604" x2="-18.034" y2="10.16" layer="94"/>
<rectangle x1="-20.32" y1="2.54" x2="-18.034" y2="6.096" layer="94"/>
<rectangle x1="-17.78" y1="6.604" x2="-15.494" y2="10.16" layer="94"/>
<rectangle x1="-17.78" y1="2.54" x2="-15.494" y2="6.096" layer="94"/>
<rectangle x1="-15.24" y1="6.604" x2="-12.954" y2="10.16" layer="94"/>
<rectangle x1="-15.24" y1="2.54" x2="-12.954" y2="6.096" layer="94"/>
<rectangle x1="-12.7" y1="6.604" x2="-10.414" y2="10.16" layer="94"/>
<rectangle x1="-12.7" y1="2.54" x2="-10.414" y2="6.096" layer="94"/>
<rectangle x1="-10.16" y1="6.604" x2="-7.874" y2="10.16" layer="94"/>
<rectangle x1="-10.16" y1="2.54" x2="-7.874" y2="6.096" layer="94"/>
<rectangle x1="-7.62" y1="6.604" x2="-5.334" y2="10.16" layer="94"/>
<rectangle x1="-7.62" y1="2.54" x2="-5.334" y2="6.096" layer="94"/>
<rectangle x1="-5.08" y1="6.604" x2="-2.794" y2="10.16" layer="94"/>
<rectangle x1="-5.08" y1="2.54" x2="-2.794" y2="6.096" layer="94"/>
<rectangle x1="-2.54" y1="6.604" x2="-0.254" y2="10.16" layer="94"/>
<rectangle x1="-2.54" y1="2.54" x2="-0.254" y2="6.096" layer="94"/>
<rectangle x1="0" y1="6.604" x2="2.286" y2="10.16" layer="94"/>
<rectangle x1="0" y1="2.54" x2="2.286" y2="6.096" layer="94"/>
<rectangle x1="2.54" y1="6.604" x2="4.826" y2="10.16" layer="94"/>
<rectangle x1="2.54" y1="2.54" x2="4.826" y2="6.096" layer="94"/>
<rectangle x1="5.08" y1="6.604" x2="7.366" y2="10.16" layer="94"/>
<rectangle x1="5.08" y1="2.54" x2="7.366" y2="6.096" layer="94"/>
<rectangle x1="7.62" y1="6.604" x2="9.906" y2="10.16" layer="94"/>
<rectangle x1="7.62" y1="2.54" x2="9.906" y2="6.096" layer="94"/>
<rectangle x1="10.16" y1="6.604" x2="12.446" y2="10.16" layer="94"/>
<rectangle x1="10.16" y1="2.54" x2="12.446" y2="6.096" layer="94"/>
<rectangle x1="12.7" y1="6.604" x2="14.986" y2="10.16" layer="94"/>
<rectangle x1="12.7" y1="2.54" x2="14.986" y2="6.096" layer="94"/>
<rectangle x1="15.24" y1="6.604" x2="17.526" y2="10.16" layer="94"/>
<rectangle x1="15.24" y1="2.54" x2="17.526" y2="6.096" layer="94"/>
<rectangle x1="17.78" y1="6.604" x2="20.066" y2="10.16" layer="94"/>
<rectangle x1="17.78" y1="2.54" x2="20.066" y2="6.096" layer="94"/>
<rectangle x1="20.32" y1="6.604" x2="22.606" y2="10.16" layer="94"/>
<rectangle x1="20.32" y1="2.54" x2="22.606" y2="6.096" layer="94"/>
<rectangle x1="22.86" y1="6.604" x2="25.146" y2="10.16" layer="94"/>
<rectangle x1="22.86" y1="2.54" x2="25.146" y2="6.096" layer="94"/>
<pin name="GND" x="-22.86" y="-10.16" length="short" direction="pwr" rot="R90"/>
<pin name="VCC" x="-20.32" y="-10.16" length="short" direction="pwr" rot="R90"/>
<pin name="CONTR" x="-17.78" y="-10.16" length="short" direction="in" rot="R90"/>
<pin name="RS" x="-15.24" y="-10.16" length="short" direction="in" rot="R90"/>
<pin name="R/W" x="-12.7" y="-10.16" length="short" direction="in" rot="R90"/>
<pin name="E" x="-10.16" y="-10.16" length="short" direction="in" rot="R90"/>
<pin name="D0" x="-7.62" y="-10.16" length="short" direction="in" rot="R90"/>
<pin name="D1" x="-5.08" y="-10.16" length="short" direction="in" rot="R90"/>
<pin name="D2" x="-2.54" y="-10.16" length="short" direction="in" rot="R90"/>
<pin name="D3" x="0" y="-10.16" length="short" direction="in" rot="R90"/>
<pin name="D4" x="2.54" y="-10.16" length="short" direction="in" rot="R90"/>
<pin name="D5" x="5.08" y="-10.16" length="short" direction="in" rot="R90"/>
<pin name="D6" x="7.62" y="-10.16" length="short" direction="in" rot="R90"/>
<pin name="D7" x="10.16" y="-10.16" length="short" direction="in" rot="R90"/>
<pin name="NC@1" x="12.7" y="-10.16" length="short" direction="nc" rot="R90"/>
<pin name="NC@2" x="15.24" y="-10.16" length="short" direction="nc" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="TUXGR_20X2" urn="urn:adsk.eagle:component:13151/2" prefix="DIS" library_version="2">
<description>&lt;b&gt;Tuxgraphics LCD display 20x2 characters&lt;/b&gt; reflective, without background light&lt;p&gt;
Source: tuxgr_20x2.pdf</description>
<gates>
<gate name="G$1" symbol="TUXGR_20X2" x="0" y="0"/>
</gates>
<devices>
<device name="" package="TUXGR_20X2">
<connects>
<connect gate="G$1" pin="CONTR" pad="3"/>
<connect gate="G$1" pin="D0" pad="7"/>
<connect gate="G$1" pin="D1" pad="8"/>
<connect gate="G$1" pin="D2" pad="9"/>
<connect gate="G$1" pin="D3" pad="10"/>
<connect gate="G$1" pin="D4" pad="11"/>
<connect gate="G$1" pin="D5" pad="12"/>
<connect gate="G$1" pin="D6" pad="13"/>
<connect gate="G$1" pin="D7" pad="14"/>
<connect gate="G$1" pin="E" pad="6"/>
<connect gate="G$1" pin="GND" pad="1"/>
<connect gate="G$1" pin="NC@1" pad="15"/>
<connect gate="G$1" pin="NC@2" pad="16"/>
<connect gate="G$1" pin="R/W" pad="5"/>
<connect gate="G$1" pin="RS" pad="4"/>
<connect gate="G$1" pin="VCC" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:13138/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
<attribute name="POPULARITY" value="0" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="micro-philips" urn="urn:adsk.eagle:library:290">
<description>&lt;b&gt;Philips Microcontroller Devices&lt;/b&gt;&lt;p&gt;
http://www-eu2.semiconductors.com&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="DIL16" urn="urn:adsk.eagle:footprint:20071/1" library_version="4">
<description>&lt;b&gt;Dual In Line&lt;/b&gt;</description>
<wire x1="-10.16" y1="-0.635" x2="-10.16" y2="-2.794" width="0.1524" layer="21"/>
<wire x1="-10.16" y1="-0.635" x2="-10.16" y2="0.635" width="0.1524" layer="21" curve="180"/>
<wire x1="-10.16" y1="-2.794" x2="10.16" y2="-2.794" width="0.1524" layer="21"/>
<wire x1="10.16" y1="-2.794" x2="10.16" y2="2.794" width="0.1524" layer="21"/>
<wire x1="10.16" y1="2.794" x2="-10.16" y2="2.794" width="0.1524" layer="21"/>
<wire x1="-10.16" y1="2.794" x2="-10.16" y2="0.635" width="0.1524" layer="21"/>
<pad name="1" x="-8.89" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="2" x="-6.35" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="3" x="-3.81" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="4" x="-1.27" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="5" x="1.27" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="6" x="3.81" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="7" x="6.35" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="8" x="8.89" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="9" x="8.89" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="10" x="6.35" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="11" x="3.81" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="12" x="1.27" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="13" x="-1.27" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="14" x="-3.81" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="15" x="-6.35" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="16" x="-8.89" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<text x="-10.5664" y="-2.794" size="1.778" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="-8.89" y="-0.9652" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="SO16W" urn="urn:adsk.eagle:footprint:20072/1" library_version="4">
<description>&lt;b&gt;Small Outline Package&lt;/b&gt; 7.6 x 10.6 mm</description>
<wire x1="5.19" y1="-3.7" x2="-5.19" y2="-3.7" width="0.2032" layer="51"/>
<wire x1="-5.19" y1="-3.7" x2="-5.19" y2="-3.2" width="0.2032" layer="51"/>
<wire x1="-5.19" y1="-3.2" x2="-5.19" y2="3.7" width="0.2032" layer="51"/>
<wire x1="-5.19" y1="3.7" x2="5.19" y2="3.7" width="0.2032" layer="51"/>
<wire x1="5.19" y1="-3.2" x2="-5.19" y2="-3.2" width="0.2032" layer="51"/>
<wire x1="5.19" y1="3.7" x2="5.19" y2="-3.2" width="0.2032" layer="51"/>
<wire x1="5.19" y1="-3.2" x2="5.19" y2="-3.7" width="0.2032" layer="51"/>
<smd name="2" x="-3.175" y="-4.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="13" x="-0.635" y="4.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="1" x="-4.445" y="-4.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="3" x="-1.905" y="-4.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="4" x="-0.635" y="-4.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="14" x="-1.905" y="4.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="12" x="0.635" y="4.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="11" x="1.905" y="4.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="6" x="1.905" y="-4.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="9" x="4.445" y="4.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="5" x="0.635" y="-4.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="7" x="3.175" y="-4.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="10" x="3.175" y="4.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="8" x="4.445" y="-4.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="15" x="-3.175" y="4.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="16" x="-4.445" y="4.6" dx="0.6" dy="2.2" layer="1"/>
<text x="-5.588" y="-3.683" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="6.858" y="-3.683" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-4.69" y1="-5.32" x2="-4.2" y2="-3.8" layer="51"/>
<rectangle x1="-3.42" y1="-5.32" x2="-2.93" y2="-3.8" layer="51"/>
<rectangle x1="-2.15" y1="-5.32" x2="-1.66" y2="-3.8" layer="51"/>
<rectangle x1="-0.88" y1="-5.32" x2="-0.39" y2="-3.8" layer="51"/>
<rectangle x1="0.39" y1="-5.32" x2="0.88" y2="-3.8" layer="51"/>
<rectangle x1="1.66" y1="-5.32" x2="2.15" y2="-3.8" layer="51"/>
<rectangle x1="2.93" y1="-5.32" x2="3.42" y2="-3.8" layer="51"/>
<rectangle x1="4.2" y1="-5.32" x2="4.69" y2="-3.8" layer="51"/>
<rectangle x1="4.2" y1="3.8" x2="4.69" y2="5.32" layer="51"/>
<rectangle x1="2.93" y1="3.8" x2="3.42" y2="5.32" layer="51"/>
<rectangle x1="1.66" y1="3.8" x2="2.15" y2="5.32" layer="51"/>
<rectangle x1="0.39" y1="3.8" x2="0.88" y2="5.32" layer="51"/>
<rectangle x1="-0.88" y1="3.8" x2="-0.39" y2="5.32" layer="51"/>
<rectangle x1="-2.15" y1="3.8" x2="-1.66" y2="5.32" layer="51"/>
<rectangle x1="-3.42" y1="3.8" x2="-2.93" y2="5.32" layer="51"/>
<rectangle x1="-4.69" y1="3.8" x2="-4.2" y2="5.32" layer="51"/>
</package>
<package name="SSOP20" urn="urn:adsk.eagle:footprint:20073/1" library_version="4">
<description>&lt;b&gt;plastic shrink small outline package&lt;/b&gt; 20 leads; body width 4.4 mm SOT266-1&lt;p&gt;
Source: http://www.semiconductors.philips.com/acrobat_download/datasheets/PCF8574_4.pdf</description>
<wire x1="3.16" y1="-2.1" x2="-3.16" y2="-2.1" width="0.2032" layer="51"/>
<wire x1="-3.16" y1="-2.1" x2="-3.16" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="-3.16" y1="-1.6" x2="-3.16" y2="2.1" width="0.2032" layer="51"/>
<wire x1="-3.16" y1="2.1" x2="3.16" y2="2.1" width="0.2032" layer="51"/>
<wire x1="3.16" y1="-1.6" x2="-3.16" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="3.16" y1="2.1" x2="3.16" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="3.16" y1="-1.6" x2="3.16" y2="-2.1" width="0.2032" layer="51"/>
<smd name="1" x="-2.925" y="-2.85" dx="0.35" dy="1.2" layer="1"/>
<smd name="2" x="-2.275" y="-2.85" dx="0.35" dy="1.2" layer="1"/>
<smd name="3" x="-1.625" y="-2.85" dx="0.35" dy="1.2" layer="1"/>
<smd name="4" x="-0.975" y="-2.85" dx="0.35" dy="1.2" layer="1"/>
<smd name="5" x="-0.325" y="-2.85" dx="0.35" dy="1.2" layer="1"/>
<smd name="6" x="0.325" y="-2.85" dx="0.35" dy="1.2" layer="1"/>
<smd name="7" x="0.975" y="-2.85" dx="0.35" dy="1.2" layer="1"/>
<smd name="8" x="1.625" y="-2.85" dx="0.35" dy="1.2" layer="1"/>
<smd name="9" x="2.275" y="-2.85" dx="0.35" dy="1.2" layer="1"/>
<smd name="10" x="2.925" y="-2.85" dx="0.35" dy="1.2" layer="1"/>
<smd name="11" x="2.925" y="2.85" dx="0.35" dy="1.2" layer="1" rot="R180"/>
<smd name="12" x="2.275" y="2.85" dx="0.35" dy="1.2" layer="1" rot="R180"/>
<smd name="13" x="1.625" y="2.85" dx="0.35" dy="1.2" layer="1" rot="R180"/>
<smd name="14" x="0.975" y="2.85" dx="0.35" dy="1.2" layer="1" rot="R180"/>
<smd name="15" x="0.325" y="2.85" dx="0.35" dy="1.2" layer="1" rot="R180"/>
<smd name="16" x="-0.325" y="2.85" dx="0.35" dy="1.2" layer="1" rot="R180"/>
<smd name="17" x="-0.975" y="2.85" dx="0.35" dy="1.2" layer="1" rot="R180"/>
<smd name="18" x="-1.625" y="2.85" dx="0.35" dy="1.2" layer="1" rot="R180"/>
<smd name="19" x="-2.275" y="2.85" dx="0.35" dy="1.2" layer="1" rot="R180"/>
<smd name="20" x="-2.925" y="2.85" dx="0.35" dy="1.2" layer="1" rot="R180"/>
<text x="-3.81" y="-1.905" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="-2.54" y="-0.635" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-3.075" y1="-3.3" x2="-2.775" y2="-2.2" layer="51"/>
<rectangle x1="-2.425" y1="-3.3" x2="-2.125" y2="-2.2" layer="51"/>
<rectangle x1="-1.775" y1="-3.3" x2="-1.475" y2="-2.2" layer="51"/>
<rectangle x1="-1.125" y1="-3.3" x2="-0.825" y2="-2.2" layer="51"/>
<rectangle x1="-0.475" y1="-3.3" x2="-0.175" y2="-2.2" layer="51"/>
<rectangle x1="0.175" y1="-3.3" x2="0.475" y2="-2.2" layer="51"/>
<rectangle x1="0.825" y1="-3.3" x2="1.125" y2="-2.2" layer="51"/>
<rectangle x1="1.475" y1="-3.3" x2="1.775" y2="-2.2" layer="51"/>
<rectangle x1="2.125" y1="-3.3" x2="2.425" y2="-2.2" layer="51"/>
<rectangle x1="2.775" y1="-3.3" x2="3.075" y2="-2.2" layer="51"/>
<rectangle x1="2.775" y1="2.2" x2="3.075" y2="3.3" layer="51" rot="R180"/>
<rectangle x1="2.125" y1="2.2" x2="2.425" y2="3.3" layer="51" rot="R180"/>
<rectangle x1="1.475" y1="2.2" x2="1.775" y2="3.3" layer="51" rot="R180"/>
<rectangle x1="0.825" y1="2.2" x2="1.125" y2="3.3" layer="51" rot="R180"/>
<rectangle x1="0.175" y1="2.2" x2="0.475" y2="3.3" layer="51" rot="R180"/>
<rectangle x1="-0.475" y1="2.2" x2="-0.175" y2="3.3" layer="51" rot="R180"/>
<rectangle x1="-1.125" y1="2.2" x2="-0.825" y2="3.3" layer="51" rot="R180"/>
<rectangle x1="-1.775" y1="2.2" x2="-1.475" y2="3.3" layer="51" rot="R180"/>
<rectangle x1="-2.425" y1="2.2" x2="-2.125" y2="3.3" layer="51" rot="R180"/>
<rectangle x1="-3.075" y1="2.2" x2="-2.775" y2="3.3" layer="51" rot="R180"/>
</package>
</packages>
<packages3d>
<package3d name="DIL16" urn="urn:adsk.eagle:package:20171/1" type="box" library_version="4">
<description>Dual In Line</description>
<packageinstances>
<packageinstance name="DIL16"/>
</packageinstances>
</package3d>
<package3d name="SO16W" urn="urn:adsk.eagle:package:20172/1" type="box" library_version="4">
<description>Small Outline Package 7.6 x 10.6 mm</description>
<packageinstances>
<packageinstance name="SO16W"/>
</packageinstances>
</package3d>
<package3d name="SSOP20" urn="urn:adsk.eagle:package:20175/1" type="box" library_version="4">
<description>plastic shrink small outline package 20 leads; body width 4.4 mm SOT266-1
Source: http://www.semiconductors.philips.com/acrobat_download/datasheets/PCF8574_4.pdf</description>
<packageinstances>
<packageinstance name="SSOP20"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="PCF8574" urn="urn:adsk.eagle:symbol:20070/1" library_version="4">
<wire x1="-7.62" y1="15.24" x2="10.16" y2="15.24" width="0.254" layer="94"/>
<wire x1="10.16" y1="15.24" x2="10.16" y2="-17.78" width="0.254" layer="94"/>
<wire x1="10.16" y1="-17.78" x2="-7.62" y2="-17.78" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-17.78" x2="-7.62" y2="15.24" width="0.254" layer="94"/>
<text x="-7.62" y="16.51" size="1.778" layer="95">&gt;NAME</text>
<text x="-7.62" y="-20.32" size="1.778" layer="96">&gt;VALUE</text>
<pin name="SCL" x="-10.16" y="0" length="short" direction="in"/>
<pin name="SDA" x="-10.16" y="2.54" length="short"/>
<pin name="A0" x="-10.16" y="-5.08" length="short" direction="in"/>
<pin name="A1" x="-10.16" y="-7.62" length="short" direction="in"/>
<pin name="A2" x="-10.16" y="-10.16" length="short" direction="in"/>
<pin name="!INT" x="-10.16" y="7.62" length="short" direction="out"/>
<pin name="VSS" x="12.7" y="-15.24" length="short" direction="pwr" rot="R180"/>
<pin name="VDD" x="12.7" y="12.7" length="short" direction="pwr" rot="R180"/>
<pin name="P0" x="12.7" y="7.62" length="short" rot="R180"/>
<pin name="P1" x="12.7" y="5.08" length="short" rot="R180"/>
<pin name="P2" x="12.7" y="2.54" length="short" rot="R180"/>
<pin name="P3" x="12.7" y="0" length="short" rot="R180"/>
<pin name="P4" x="12.7" y="-2.54" length="short" rot="R180"/>
<pin name="P5" x="12.7" y="-5.08" length="short" rot="R180"/>
<pin name="P6" x="12.7" y="-7.62" length="short" rot="R180"/>
<pin name="P7" x="12.7" y="-10.16" length="short" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="PCF8574" urn="urn:adsk.eagle:component:20210/2" prefix="IC" library_version="4">
<description>I2C-Bus &lt;b&gt;Remote 8-bit I/O expander&lt;/b&gt;&lt;p&gt;
http://www.semiconductors.philips.com/acrobat_download/datasheets/PCF8574_4.pdf</description>
<gates>
<gate name="G$1" symbol="PCF8574" x="0" y="0"/>
</gates>
<devices>
<device name="P" package="DIL16">
<connects>
<connect gate="G$1" pin="!INT" pad="13"/>
<connect gate="G$1" pin="A0" pad="1"/>
<connect gate="G$1" pin="A1" pad="2"/>
<connect gate="G$1" pin="A2" pad="3"/>
<connect gate="G$1" pin="P0" pad="4"/>
<connect gate="G$1" pin="P1" pad="5"/>
<connect gate="G$1" pin="P2" pad="6"/>
<connect gate="G$1" pin="P3" pad="7"/>
<connect gate="G$1" pin="P4" pad="9"/>
<connect gate="G$1" pin="P5" pad="10"/>
<connect gate="G$1" pin="P6" pad="11"/>
<connect gate="G$1" pin="P7" pad="12"/>
<connect gate="G$1" pin="SCL" pad="14"/>
<connect gate="G$1" pin="SDA" pad="15"/>
<connect gate="G$1" pin="VDD" pad="16"/>
<connect gate="G$1" pin="VSS" pad="8"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:20171/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="PCF8574PN" constant="no"/>
<attribute name="OC_FARNELL" value="204020" constant="no"/>
<attribute name="OC_NEWARK" value="11N8515" constant="no"/>
<attribute name="POPULARITY" value="3" constant="no"/>
</technology>
</technologies>
</device>
<device name="T" package="SO16W">
<connects>
<connect gate="G$1" pin="!INT" pad="13"/>
<connect gate="G$1" pin="A0" pad="1"/>
<connect gate="G$1" pin="A1" pad="2"/>
<connect gate="G$1" pin="A2" pad="3"/>
<connect gate="G$1" pin="P0" pad="4"/>
<connect gate="G$1" pin="P1" pad="5"/>
<connect gate="G$1" pin="P2" pad="6"/>
<connect gate="G$1" pin="P3" pad="7"/>
<connect gate="G$1" pin="P4" pad="9"/>
<connect gate="G$1" pin="P5" pad="10"/>
<connect gate="G$1" pin="P6" pad="11"/>
<connect gate="G$1" pin="P7" pad="12"/>
<connect gate="G$1" pin="SCL" pad="14"/>
<connect gate="G$1" pin="SDA" pad="15"/>
<connect gate="G$1" pin="VDD" pad="16"/>
<connect gate="G$1" pin="VSS" pad="8"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:20172/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="PCF8574T/3,512" constant="no"/>
<attribute name="OC_FARNELL" value="1201255" constant="no"/>
<attribute name="OC_NEWARK" value="70R6870" constant="no"/>
<attribute name="POPULARITY" value="3" constant="no"/>
</technology>
</technologies>
</device>
<device name="TS" package="SSOP20">
<connects>
<connect gate="G$1" pin="!INT" pad="1"/>
<connect gate="G$1" pin="A0" pad="6"/>
<connect gate="G$1" pin="A1" pad="7"/>
<connect gate="G$1" pin="A2" pad="9"/>
<connect gate="G$1" pin="P0" pad="10"/>
<connect gate="G$1" pin="P1" pad="11"/>
<connect gate="G$1" pin="P2" pad="12"/>
<connect gate="G$1" pin="P3" pad="14"/>
<connect gate="G$1" pin="P4" pad="16"/>
<connect gate="G$1" pin="P5" pad="17"/>
<connect gate="G$1" pin="P6" pad="19"/>
<connect gate="G$1" pin="P7" pad="20"/>
<connect gate="G$1" pin="SCL" pad="2"/>
<connect gate="G$1" pin="SDA" pad="4"/>
<connect gate="G$1" pin="VDD" pad="5"/>
<connect gate="G$1" pin="VSS" pad="15"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:20175/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="PCF8574TS/3,118" constant="no"/>
<attribute name="OC_FARNELL" value="1506644" constant="no"/>
<attribute name="OC_NEWARK" value="70R6873" constant="no"/>
<attribute name="POPULARITY" value="0" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="supply2" urn="urn:adsk.eagle:library:372">
<description>&lt;b&gt;Supply Symbols&lt;/b&gt;&lt;p&gt;
GND, VCC, 0V, +5V, -5V, etc.&lt;p&gt;
Please keep in mind, that these devices are necessary for the
automatic wiring of the supply signals.&lt;p&gt;
The pin name defined in the symbol is identical to the net which is to be wired automatically.&lt;p&gt;
In this library the device names are the same as the pin names of the symbols, therefore the correct signal names appear next to the supply symbols in the schematic.&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="+05V" urn="urn:adsk.eagle:symbol:26987/1" library_version="2">
<wire x1="-0.635" y1="1.27" x2="0.635" y2="1.27" width="0.1524" layer="94"/>
<wire x1="0" y1="0.635" x2="0" y2="1.905" width="0.1524" layer="94"/>
<circle x="0" y="1.27" radius="1.27" width="0.254" layer="94"/>
<text x="-1.905" y="3.175" size="1.778" layer="96">&gt;VALUE</text>
<pin name="+5V" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="+5V" urn="urn:adsk.eagle:component:27032/1" prefix="SUPPLY" library_version="2">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="+5V" symbol="+05V" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="supply1" urn="urn:adsk.eagle:library:371">
<description>&lt;b&gt;Supply Symbols&lt;/b&gt;&lt;p&gt;
 GND, VCC, 0V, +5V, -5V, etc.&lt;p&gt;
 Please keep in mind, that these devices are necessary for the
 automatic wiring of the supply signals.&lt;p&gt;
 The pin name defined in the symbol is identical to the net which is to be wired automatically.&lt;p&gt;
 In this library the device names are the same as the pin names of the symbols, therefore the correct signal names appear next to the supply symbols in the schematic.&lt;p&gt;
 &lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="GND" urn="urn:adsk.eagle:symbol:26925/1" library_version="1">
<wire x1="-1.905" y1="0" x2="1.905" y2="0" width="0.254" layer="94"/>
<text x="-2.54" y="-2.54" size="1.778" layer="96">&gt;VALUE</text>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="GND" urn="urn:adsk.eagle:component:26954/1" prefix="GND" library_version="1">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="rcl" urn="urn:adsk.eagle:library:334">
<description>&lt;b&gt;Resistors, Capacitors, Inductors&lt;/b&gt;&lt;p&gt;
Based on the previous libraries:
&lt;ul&gt;
&lt;li&gt;r.lbr
&lt;li&gt;cap.lbr 
&lt;li&gt;cap-fe.lbr
&lt;li&gt;captant.lbr
&lt;li&gt;polcap.lbr
&lt;li&gt;ipc-smd.lbr
&lt;/ul&gt;
All SMD packages are defined according to the IPC specifications and  CECC&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;&lt;p&gt;
&lt;p&gt;
for Electrolyt Capacitors see also :&lt;p&gt;
www.bccomponents.com &lt;p&gt;
www.panasonic.com&lt;p&gt;
www.kemet.com&lt;p&gt;
http://www.secc.co.jp/pdf/os_e/2004/e_os_all.pdf &lt;b&gt;(SANYO)&lt;/b&gt;
&lt;p&gt;
for trimmer refence see : &lt;u&gt;www.electrospec-inc.com/cross_references/trimpotcrossref.asp&lt;/u&gt;&lt;p&gt;

&lt;table border=0 cellspacing=0 cellpadding=0 width="100%" cellpaddding=0&gt;
&lt;tr valign="top"&gt;

&lt;! &lt;td width="10"&gt;&amp;nbsp;&lt;/td&gt;
&lt;td width="90%"&gt;

&lt;b&gt;&lt;font color="#0000FF" size="4"&gt;TRIM-POT CROSS REFERENCE&lt;/font&gt;&lt;/b&gt;
&lt;P&gt;
&lt;TABLE BORDER=0 CELLSPACING=1 CELLPADDING=2&gt;
  &lt;TR&gt;
    &lt;TD COLSPAN=8&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;RECTANGULAR MULTI-TURN&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;B&gt;
      &lt;FONT SIZE=3 FACE=ARIAL color="#FF0000"&gt;BOURNS&lt;/FONT&gt;
      &lt;/B&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;B&gt;
      &lt;FONT SIZE=3 FACE=ARIAL color="#FF0000"&gt;BI&amp;nbsp;TECH&lt;/FONT&gt;
      &lt;/B&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;B&gt;
      &lt;FONT SIZE=3 FACE=ARIAL color="#FF0000"&gt;DALE-VISHAY&lt;/FONT&gt;
      &lt;/B&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;B&gt;
      &lt;FONT SIZE=3 FACE=ARIAL color="#FF0000"&gt;PHILIPS/MEPCO&lt;/FONT&gt;
      &lt;/B&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;B&gt;
      &lt;FONT SIZE=3 FACE=ARIAL color="#FF0000"&gt;MURATA&lt;/FONT&gt;
      &lt;/B&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;B&gt;
      &lt;FONT SIZE=3 FACE=ARIAL color="#FF0000"&gt;PANASONIC&lt;/FONT&gt;
      &lt;/B&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;B&gt;
      &lt;FONT SIZE=3 FACE=ARIAL color="#FF0000"&gt;SPECTROL&lt;/FONT&gt;
      &lt;/B&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;B&gt;
      &lt;FONT SIZE=3 FACE=ARIAL color="#FF0000"&gt;MILSPEC&lt;/FONT&gt;
      &lt;/B&gt;
    &lt;/TD&gt;&lt;TD&gt;&amp;nbsp;&lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3 &gt;
      3005P&lt;BR&gt;
      3006P&lt;BR&gt;
      3006W&lt;BR&gt;
      3006Y&lt;BR&gt;
      3009P&lt;BR&gt;
      3009W&lt;BR&gt;
      3009Y&lt;BR&gt;
      3057J&lt;BR&gt;
      3057L&lt;BR&gt;
      3057P&lt;BR&gt;
      3057Y&lt;BR&gt;
      3059J&lt;BR&gt;
      3059L&lt;BR&gt;
      3059P&lt;BR&gt;
      3059Y&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      89P&lt;BR&gt;
      89W&lt;BR&gt;
      89X&lt;BR&gt;
      89PH&lt;BR&gt;
      76P&lt;BR&gt;
      89XH&lt;BR&gt;
      78SLT&lt;BR&gt;
      78L&amp;nbsp;ALT&lt;BR&gt;
      56P&amp;nbsp;ALT&lt;BR&gt;
      78P&amp;nbsp;ALT&lt;BR&gt;
      T8S&lt;BR&gt;
      78L&lt;BR&gt;
      56P&lt;BR&gt;
      78P&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      T18/784&lt;BR&gt;
      783&lt;BR&gt;
      781&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      2199&lt;BR&gt;
      1697/1897&lt;BR&gt;
      1680/1880&lt;BR&gt;
      2187&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      8035EKP/CT20/RJ-20P&lt;BR&gt;
      -&lt;BR&gt;
      RJ-20X&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      1211L&lt;BR&gt;
      8012EKQ&amp;nbsp;ALT&lt;BR&gt;
      8012EKR&amp;nbsp;ALT&lt;BR&gt;
      1211P&lt;BR&gt;
      8012EKJ&lt;BR&gt;
      8012EKL&lt;BR&gt;
      8012EKQ&lt;BR&gt;
      8012EKR&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      2101P&lt;BR&gt;
      2101W&lt;BR&gt;
      2101Y&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      2102L&lt;BR&gt;
      2102S&lt;BR&gt;
      2102Y&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      EVMCOG&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      43P&lt;BR&gt;
      43W&lt;BR&gt;
      43Y&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      40L&lt;BR&gt;
      40P&lt;BR&gt;
      40Y&lt;BR&gt;
      70Y-T602&lt;BR&gt;
      70L&lt;BR&gt;
      70P&lt;BR&gt;
      70Y&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      RT/RTR12&lt;BR&gt;
      RT/RTR12&lt;BR&gt;
      RT/RTR12&lt;BR&gt;
      -&lt;BR&gt;
      RJ/RJR12&lt;BR&gt;
      RJ/RJR12&lt;BR&gt;
      RJ/RJR12&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD COLSPAN=8&gt;&amp;nbsp;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD COLSPAN=8&gt;
      &lt;FONT SIZE=4 FACE=ARIAL&gt;&lt;B&gt;SQUARE MULTI-TURN&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
   &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;BOURN&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;BI&amp;nbsp;TECH&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;DALE-VISHAY&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;PHILIPS/MEPCO&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;MURATA&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;PANASONIC&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;SPECTROL&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;MILSPEC&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      3250L&lt;BR&gt;
      3250P&lt;BR&gt;
      3250W&lt;BR&gt;
      3250X&lt;BR&gt;
      3252P&lt;BR&gt;
      3252W&lt;BR&gt;
      3252X&lt;BR&gt;
      3260P&lt;BR&gt;
      3260W&lt;BR&gt;
      3260X&lt;BR&gt;
      3262P&lt;BR&gt;
      3262W&lt;BR&gt;
      3262X&lt;BR&gt;
      3266P&lt;BR&gt;
      3266W&lt;BR&gt;
      3266X&lt;BR&gt;
      3290H&lt;BR&gt;
      3290P&lt;BR&gt;
      3290W&lt;BR&gt;
      3292P&lt;BR&gt;
      3292W&lt;BR&gt;
      3292X&lt;BR&gt;
      3296P&lt;BR&gt;
      3296W&lt;BR&gt;
      3296X&lt;BR&gt;
      3296Y&lt;BR&gt;
      3296Z&lt;BR&gt;
      3299P&lt;BR&gt;
      3299W&lt;BR&gt;
      3299X&lt;BR&gt;
      3299Y&lt;BR&gt;
      3299Z&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      66P&amp;nbsp;ALT&lt;BR&gt;
      66W&amp;nbsp;ALT&lt;BR&gt;
      66X&amp;nbsp;ALT&lt;BR&gt;
      66P&amp;nbsp;ALT&lt;BR&gt;
      66W&amp;nbsp;ALT&lt;BR&gt;
      66X&amp;nbsp;ALT&lt;BR&gt;
      -&lt;BR&gt;
      64W&amp;nbsp;ALT&lt;BR&gt;
      -&lt;BR&gt;
      64P&amp;nbsp;ALT&lt;BR&gt;
      64W&amp;nbsp;ALT&lt;BR&gt;
      64X&amp;nbsp;ALT&lt;BR&gt;
      64P&lt;BR&gt;
      64W&lt;BR&gt;
      64X&lt;BR&gt;
      66X&amp;nbsp;ALT&lt;BR&gt;
      66P&amp;nbsp;ALT&lt;BR&gt;
      66W&amp;nbsp;ALT&lt;BR&gt;
      66P&lt;BR&gt;
      66W&lt;BR&gt;
      66X&lt;BR&gt;
      67P&lt;BR&gt;
      67W&lt;BR&gt;
      67X&lt;BR&gt;
      67Y&lt;BR&gt;
      67Z&lt;BR&gt;
      68P&lt;BR&gt;
      68W&lt;BR&gt;
      68X&lt;BR&gt;
      67Y&amp;nbsp;ALT&lt;BR&gt;
      67Z&amp;nbsp;ALT&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      5050&lt;BR&gt;
      5091&lt;BR&gt;
      5080&lt;BR&gt;
      5087&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      T63YB&lt;BR&gt;
      T63XB&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      5887&lt;BR&gt;
      5891&lt;BR&gt;
      5880&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      T93Z&lt;BR&gt;
      T93YA&lt;BR&gt;
      T93XA&lt;BR&gt;
      T93YB&lt;BR&gt;
      T93XB&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      8026EKP&lt;BR&gt;
      8026EKW&lt;BR&gt;
      8026EKM&lt;BR&gt;
      8026EKP&lt;BR&gt;
      8026EKB&lt;BR&gt;
      8026EKM&lt;BR&gt;
      1309X&lt;BR&gt;
      1309P&lt;BR&gt;
      1309W&lt;BR&gt;
      8024EKP&lt;BR&gt;
      8024EKW&lt;BR&gt;
      8024EKN&lt;BR&gt;
      RJ-9P/CT9P&lt;BR&gt;
      RJ-9W&lt;BR&gt;
      RJ-9X&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      3103P&lt;BR&gt;
      3103Y&lt;BR&gt;
      3103Z&lt;BR&gt;
      3103P&lt;BR&gt;
      3103Y&lt;BR&gt;
      3103Z&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      3105P/3106P&lt;BR&gt;
      3105W/3106W&lt;BR&gt;
      3105X/3106X&lt;BR&gt;
      3105Y/3106Y&lt;BR&gt;
      3105Z/3105Z&lt;BR&gt;
      3102P&lt;BR&gt;
      3102W&lt;BR&gt;
      3102X&lt;BR&gt;
      3102Y&lt;BR&gt;
      3102Z&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      EVMCBG&lt;BR&gt;
      EVMCCG&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      55-1-X&lt;BR&gt;
      55-4-X&lt;BR&gt;
      55-3-X&lt;BR&gt;
      55-2-X&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      50-2-X&lt;BR&gt;
      50-4-X&lt;BR&gt;
      50-3-X&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      64P&lt;BR&gt;
      64W&lt;BR&gt;
      64X&lt;BR&gt;
      64Y&lt;BR&gt;
      64Z&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      RT/RTR22&lt;BR&gt;
      RT/RTR22&lt;BR&gt;
      RT/RTR22&lt;BR&gt;
      RT/RTR22&lt;BR&gt;
      RJ/RJR22&lt;BR&gt;
      RJ/RJR22&lt;BR&gt;
      RJ/RJR22&lt;BR&gt;
      RT/RTR26&lt;BR&gt;
      RT/RTR26&lt;BR&gt;
      RT/RTR26&lt;BR&gt;
      RJ/RJR26&lt;BR&gt;
      RJ/RJR26&lt;BR&gt;
      RJ/RJR26&lt;BR&gt;
      RJ/RJR26&lt;BR&gt;
      RJ/RJR26&lt;BR&gt;
      RJ/RJR26&lt;BR&gt;
      RT/RTR24&lt;BR&gt;
      RT/RTR24&lt;BR&gt;
      RT/RTR24&lt;BR&gt;
      RJ/RJR24&lt;BR&gt;
      RJ/RJR24&lt;BR&gt;
      RJ/RJR24&lt;BR&gt;
      RJ/RJR24&lt;BR&gt;
      RJ/RJR24&lt;BR&gt;
      RJ/RJR24&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD COLSPAN=8&gt;&amp;nbsp;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD COLSPAN=8&gt;
      &lt;FONT SIZE=4 FACE=ARIAL&gt;&lt;B&gt;SINGLE TURN&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;BOURN&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;BI&amp;nbsp;TECH&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;DALE-VISHAY&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;PHILIPS/MEPCO&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;MURATA&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;PANASONIC&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;SPECTROL&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;MILSPEC&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      3323P&lt;BR&gt;
      3323S&lt;BR&gt;
      3323W&lt;BR&gt;
      3329H&lt;BR&gt;
      3329P&lt;BR&gt;
      3329W&lt;BR&gt;
      3339H&lt;BR&gt;
      3339P&lt;BR&gt;
      3339W&lt;BR&gt;
      3352E&lt;BR&gt;
      3352H&lt;BR&gt;
      3352K&lt;BR&gt;
      3352P&lt;BR&gt;
      3352T&lt;BR&gt;
      3352V&lt;BR&gt;
      3352W&lt;BR&gt;
      3362H&lt;BR&gt;
      3362M&lt;BR&gt;
      3362P&lt;BR&gt;
      3362R&lt;BR&gt;
      3362S&lt;BR&gt;
      3362U&lt;BR&gt;
      3362W&lt;BR&gt;
      3362X&lt;BR&gt;
      3386B&lt;BR&gt;
      3386C&lt;BR&gt;
      3386F&lt;BR&gt;
      3386H&lt;BR&gt;
      3386K&lt;BR&gt;
      3386M&lt;BR&gt;
      3386P&lt;BR&gt;
      3386S&lt;BR&gt;
      3386W&lt;BR&gt;
      3386X&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      25P&lt;BR&gt;
      25S&lt;BR&gt;
      25RX&lt;BR&gt;
      82P&lt;BR&gt;
      82M&lt;BR&gt;
      82PA&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      91E&lt;BR&gt;
      91X&lt;BR&gt;
      91T&lt;BR&gt;
      91B&lt;BR&gt;
      91A&lt;BR&gt;
      91V&lt;BR&gt;
      91W&lt;BR&gt;
      25W&lt;BR&gt;
      25V&lt;BR&gt;
      25P&lt;BR&gt;
      -&lt;BR&gt;
      25S&lt;BR&gt;
      25U&lt;BR&gt;
      25RX&lt;BR&gt;
      25X&lt;BR&gt;
      72XW&lt;BR&gt;
      72XL&lt;BR&gt;
      72PM&lt;BR&gt;
      72RX&lt;BR&gt;
      -&lt;BR&gt;
      72PX&lt;BR&gt;
      72P&lt;BR&gt;
      72RXW&lt;BR&gt;
      72RXL&lt;BR&gt;
      72X&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      T7YB&lt;BR&gt;
      T7YA&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      TXD&lt;BR&gt;
      TYA&lt;BR&gt;
      TYP&lt;BR&gt;
      -&lt;BR&gt;
      TYD&lt;BR&gt;
      TX&lt;BR&gt;
      -&lt;BR&gt;
      150SX&lt;BR&gt;
      100SX&lt;BR&gt;
      102T&lt;BR&gt;
      101S&lt;BR&gt;
      190T&lt;BR&gt;
      150TX&lt;BR&gt;
      101&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      101SX&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      ET6P&lt;BR&gt;
      ET6S&lt;BR&gt;
      ET6X&lt;BR&gt;
      RJ-6W/8014EMW&lt;BR&gt;
      RJ-6P/8014EMP&lt;BR&gt;
      RJ-6X/8014EMX&lt;BR&gt;
      TM7W&lt;BR&gt;
      TM7P&lt;BR&gt;
      TM7X&lt;BR&gt;
      -&lt;BR&gt;
      8017SMS&lt;BR&gt;
      -&lt;BR&gt;
      8017SMB&lt;BR&gt;
      8017SMA&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      CT-6W&lt;BR&gt;
      CT-6H&lt;BR&gt;
      CT-6P&lt;BR&gt;
      CT-6R&lt;BR&gt;
      -&lt;BR&gt;
      CT-6V&lt;BR&gt;
      CT-6X&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      8038EKV&lt;BR&gt;
      -&lt;BR&gt;
      8038EKX&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      8038EKP&lt;BR&gt;
      8038EKZ&lt;BR&gt;
      8038EKW&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      3321H&lt;BR&gt;
      3321P&lt;BR&gt;
      3321N&lt;BR&gt;
      1102H&lt;BR&gt;
      1102P&lt;BR&gt;
      1102T&lt;BR&gt;
      RVA0911V304A&lt;BR&gt;
      -&lt;BR&gt;
      RVA0911H413A&lt;BR&gt;
      RVG0707V100A&lt;BR&gt;
      RVA0607V(H)306A&lt;BR&gt;
      RVA1214H213A&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      3104B&lt;BR&gt;
      3104C&lt;BR&gt;
      3104F&lt;BR&gt;
      3104H&lt;BR&gt;
      -&lt;BR&gt;
      3104M&lt;BR&gt;
      3104P&lt;BR&gt;
      3104S&lt;BR&gt;
      3104W&lt;BR&gt;
      3104X&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      EVMQ0G&lt;BR&gt;
      EVMQIG&lt;BR&gt;
      EVMQ3G&lt;BR&gt;
      EVMS0G&lt;BR&gt;
      EVMQ0G&lt;BR&gt;
      EVMG0G&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      EVMK4GA00B&lt;BR&gt;
      EVM30GA00B&lt;BR&gt;
      EVMK0GA00B&lt;BR&gt;
      EVM38GA00B&lt;BR&gt;
      EVMB6&lt;BR&gt;
      EVLQ0&lt;BR&gt;
      -&lt;BR&gt;
      EVMMSG&lt;BR&gt;
      EVMMBG&lt;BR&gt;
      EVMMAG&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      EVMMCS&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      EVMM1&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      EVMM0&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      EVMM3&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      62-3-1&lt;BR&gt;
      62-1-2&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      67R&lt;BR&gt;
      -&lt;BR&gt;
      67P&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      67X&lt;BR&gt;
      63V&lt;BR&gt;
      63S&lt;BR&gt;
      63M&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      63H&lt;BR&gt;
      63P&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      63X&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      RJ/RJR50&lt;BR&gt;
      RJ/RJR50&lt;BR&gt;
      RJ/RJR50&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
&lt;/TABLE&gt;
&lt;P&gt;&amp;nbsp;&lt;P&gt;
&lt;TABLE BORDER=0 CELLSPACING=1 CELLPADDING=3&gt;
  &lt;TR&gt;
    &lt;TD COLSPAN=7&gt;
      &lt;FONT color="#0000FF" SIZE=4 FACE=ARIAL&gt;&lt;B&gt;SMD TRIM-POT CROSS REFERENCE&lt;/B&gt;&lt;/FONT&gt;
      &lt;P&gt;
      &lt;FONT SIZE=4 FACE=ARIAL&gt;&lt;B&gt;MULTI-TURN&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;BOURNS&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;BI&amp;nbsp;TECH&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;DALE-VISHAY&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;PHILIPS/MEPCO&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;PANASONIC&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;TOCOS&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;AUX/KYOCERA&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      3224G&lt;BR&gt;
      3224J&lt;BR&gt;
      3224W&lt;BR&gt;
      3269P&lt;BR&gt;
      3269W&lt;BR&gt;
      3269X&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      44G&lt;BR&gt;
      44J&lt;BR&gt;
      44W&lt;BR&gt;
      84P&lt;BR&gt;
      84W&lt;BR&gt;
      84X&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      ST63Z&lt;BR&gt;
      ST63Y&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      ST5P&lt;BR&gt;
      ST5W&lt;BR&gt;
      ST5X&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD COLSPAN=7&gt;&amp;nbsp;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD COLSPAN=7&gt;
      &lt;FONT SIZE=4 FACE=ARIAL&gt;&lt;B&gt;SINGLE TURN&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;BOURNS&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;BI&amp;nbsp;TECH&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;DALE-VISHAY&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;PHILIPS/MEPCO&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;PANASONIC&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;TOCOS&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;AUX/KYOCERA&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      3314G&lt;BR&gt;
      3314J&lt;BR&gt;
      3364A/B&lt;BR&gt;
      3364C/D&lt;BR&gt;
      3364W/X&lt;BR&gt;
      3313G&lt;BR&gt;
      3313J&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      23B&lt;BR&gt;
      23A&lt;BR&gt;
      21X&lt;BR&gt;
      21W&lt;BR&gt;
      -&lt;BR&gt;
      22B&lt;BR&gt;
      22A&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      ST5YL/ST53YL&lt;BR&gt;
      ST5YJ/5T53YJ&lt;BR&gt;
      ST-23A&lt;BR&gt;
      ST-22B&lt;BR&gt;
      ST-22&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      ST-4B&lt;BR&gt;
      ST-4A&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      ST-3B&lt;BR&gt;
      ST-3A&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      EVM-6YS&lt;BR&gt;
      EVM-1E&lt;BR&gt;
      EVM-1G&lt;BR&gt;
      EVM-1D&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      G4B&lt;BR&gt;
      G4A&lt;BR&gt;
      TR04-3S1&lt;BR&gt;
      TRG04-2S1&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      DVR-43A&lt;BR&gt;
      CVR-42C&lt;BR&gt;
      CVR-42A/C&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
&lt;/TABLE&gt;
&lt;P&gt;
&lt;FONT SIZE=4 FACE=ARIAL&gt;&lt;B&gt;ALT =&amp;nbsp;ALTERNATE&lt;/B&gt;&lt;/FONT&gt;
&lt;P&gt;

&amp;nbsp;
&lt;P&gt;
&lt;/td&gt;
&lt;/tr&gt;
&lt;/table&gt;</description>
<packages>
<package name="PT-10" urn="urn:adsk.eagle:footprint:23297/1" library_version="11">
<description>&lt;b&gt;Trimm Potentiometer&lt;/b&gt;</description>
<wire x1="-3.175" y1="-4.064" x2="3.175" y2="-4.064" width="0.127" layer="21" curve="-288.924644"/>
<wire x1="3.175" y1="-4.064" x2="3.175" y2="-5.715" width="0.127" layer="51"/>
<wire x1="-3.175" y1="-5.715" x2="-3.175" y2="-4.064" width="0.127" layer="51"/>
<wire x1="-2.794" y1="-5.715" x2="-2.794" y2="-3.175" width="0.127" layer="51"/>
<wire x1="-2.794" y1="-3.175" x2="-1.905" y2="-2.54" width="0.127" layer="21"/>
<wire x1="-1.905" y1="-2.54" x2="-0.635" y2="-3.175" width="0.127" layer="21"/>
<wire x1="-0.635" y1="-3.175" x2="-0.635" y2="-4.445" width="0.127" layer="21"/>
<wire x1="-0.635" y1="-4.445" x2="-1.27" y2="-4.445" width="0.127" layer="21"/>
<wire x1="-1.27" y1="-4.445" x2="-1.27" y2="-5.715" width="0.127" layer="21"/>
<wire x1="1.27" y1="-5.715" x2="1.27" y2="-4.445" width="0.127" layer="21"/>
<wire x1="1.27" y1="-4.445" x2="0.635" y2="-4.445" width="0.127" layer="21"/>
<wire x1="0.635" y1="-4.445" x2="0.635" y2="-3.175" width="0.127" layer="21"/>
<wire x1="0.635" y1="-3.175" x2="1.905" y2="-2.54" width="0.127" layer="21"/>
<wire x1="1.905" y1="-2.54" x2="2.794" y2="-3.175" width="0.127" layer="21"/>
<wire x1="2.794" y1="-3.175" x2="2.794" y2="-5.715" width="0.127" layer="51"/>
<wire x1="-3.175" y1="-5.715" x2="3.175" y2="-5.715" width="0.127" layer="51"/>
<wire x1="0.889" y1="1.397" x2="-0.127" y2="0.381" width="0.127" layer="21"/>
<wire x1="-0.127" y1="0.381" x2="-0.508" y2="0.762" width="0.127" layer="21"/>
<wire x1="-0.508" y1="0.762" x2="-0.508" y2="0" width="0.127" layer="21"/>
<wire x1="-0.508" y1="0" x2="-1.397" y2="-0.889" width="0.127" layer="21"/>
<wire x1="1.397" y1="0.889" x2="0.381" y2="-0.127" width="0.127" layer="21"/>
<wire x1="0.381" y1="-0.127" x2="0.762" y2="-0.508" width="0.127" layer="21"/>
<wire x1="0.762" y1="-0.508" x2="0" y2="-0.508" width="0.127" layer="21"/>
<wire x1="0" y1="-0.508" x2="-0.889" y2="-1.397" width="0.127" layer="21"/>
<circle x="0" y="0" radius="1.8034" width="0.127" layer="21"/>
<circle x="0" y="0" radius="2.794" width="0.0508" layer="21"/>
<pad name="1" x="-2.54" y="-5.08" drill="1.016" diameter="1.778" shape="octagon"/>
<pad name="2" x="2.54" y="-5.08" drill="1.016" diameter="1.778" shape="octagon"/>
<pad name="3" x="0" y="5.08" drill="1.016" diameter="1.778" shape="octagon"/>
<text x="-3.175" y="6.35" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.048" y="-7.62" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="PT-10S" urn="urn:adsk.eagle:footprint:23298/1" library_version="11">
<description>&lt;b&gt;Trimm Potentiometer&lt;/b&gt;</description>
<wire x1="-1.905" y1="-1.27" x2="-1.27" y2="-1.905" width="0.127" layer="51"/>
<wire x1="-1.27" y1="-1.905" x2="1.27" y2="-1.905" width="0.127" layer="21"/>
<wire x1="1.27" y1="-1.905" x2="1.905" y2="-1.27" width="0.127" layer="51"/>
<wire x1="-4.445" y1="-1.27" x2="4.445" y2="-1.27" width="0.127" layer="51"/>
<wire x1="-0.635" y1="3.175" x2="-0.635" y2="2.54" width="0.127" layer="21"/>
<wire x1="0.635" y1="2.54" x2="0.635" y2="3.175" width="0.127" layer="21"/>
<wire x1="-4.445" y1="3.175" x2="4.445" y2="3.175" width="0.127" layer="21"/>
<wire x1="5.08" y1="2.54" x2="4.445" y2="3.175" width="0.127" layer="21"/>
<wire x1="5.08" y1="2.54" x2="5.08" y2="-0.635" width="0.127" layer="21"/>
<wire x1="-4.445" y1="-1.27" x2="-5.08" y2="-0.635" width="0.127" layer="21"/>
<wire x1="-4.445" y1="3.175" x2="-5.08" y2="2.54" width="0.127" layer="21"/>
<wire x1="-5.08" y1="2.54" x2="5.08" y2="2.54" width="0.127" layer="21"/>
<wire x1="-5.08" y1="-0.635" x2="5.08" y2="-0.635" width="0.127" layer="51"/>
<wire x1="-5.08" y1="-0.635" x2="-5.08" y2="2.54" width="0.127" layer="21"/>
<wire x1="5.08" y1="-0.635" x2="4.445" y2="-1.27" width="0.127" layer="21"/>
<pad name="1" x="-2.54" y="-1.27" drill="1.016" diameter="1.778" shape="octagon"/>
<pad name="2" x="2.54" y="-1.27" drill="1.016" diameter="1.778" shape="octagon"/>
<pad name="3" x="0" y="1.27" drill="1.016" diameter="1.778" shape="octagon"/>
<text x="-2.54" y="3.7846" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.54" y="-4.2164" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="PT-SPIN" urn="urn:adsk.eagle:footprint:23299/1" library_version="11">
<description>&lt;b&gt;Trimm Potentiometer&lt;/b&gt;</description>
<wire x1="-8.89" y1="-2.54" x2="8.89" y2="-2.54" width="0.127" layer="21"/>
<wire x1="8.89" y1="-2.54" x2="8.89" y2="2.54" width="0.127" layer="21"/>
<wire x1="8.89" y1="2.54" x2="-8.89" y2="2.54" width="0.127" layer="21"/>
<wire x1="-8.89" y1="2.54" x2="-8.89" y2="-2.54" width="0.127" layer="21"/>
<pad name="3" x="-1.27" y="1.27" drill="0.8128" diameter="1.524" shape="octagon"/>
<pad name="2" x="6.35" y="-1.27" drill="0.8128" diameter="1.524" shape="octagon"/>
<pad name="1" x="-6.35" y="-1.27" drill="0.8128" diameter="1.524" shape="octagon"/>
<text x="-3.175" y="3.175" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-1.778" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-9.525" y1="-1.27" x2="-8.89" y2="1.27" layer="21"/>
<rectangle x1="-10.16" y1="0.381" x2="-9.525" y2="1.27" layer="21"/>
<rectangle x1="-10.16" y1="-1.27" x2="-9.525" y2="-0.381" layer="21"/>
</package>
</packages>
<packages3d>
<package3d name="PT-10" urn="urn:adsk.eagle:package:23786/1" type="box" library_version="11">
<description>Trimm Potentiometer</description>
<packageinstances>
<packageinstance name="PT-10"/>
</packageinstances>
</package3d>
<package3d name="PT-10S" urn="urn:adsk.eagle:package:23785/1" type="box" library_version="11">
<description>Trimm Potentiometer</description>
<packageinstances>
<packageinstance name="PT-10S"/>
</packageinstances>
</package3d>
<package3d name="PT-SPIN" urn="urn:adsk.eagle:package:23782/1" type="box" library_version="11">
<description>Trimm Potentiometer</description>
<packageinstances>
<packageinstance name="PT-SPIN"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="POTEURO" urn="urn:adsk.eagle:symbol:23296/3" library_version="11">
<wire x1="-2.54" y1="1.27" x2="-2.54" y2="-1.27" width="0.254" layer="94"/>
<wire x1="2.54" y1="-1.27" x2="2.54" y2="1.27" width="0.254" layer="94"/>
<wire x1="2.54" y1="1.27" x2="-2.54" y2="1.27" width="0.254" layer="94"/>
<wire x1="0" y1="-1.778" x2="2.667" y2="3.81" width="0.1524" layer="94"/>
<wire x1="0" y1="-1.778" x2="0" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="-1.27" x2="2.54" y2="-1.27" width="0.254" layer="94"/>
<text x="2.54" y="-5.08" size="1.778" layer="95">&gt;NAME</text>
<text x="2.54" y="-7.62" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="-5.08" y="0" visible="pad" length="short" direction="pas"/>
<pin name="2" x="5.08" y="0" visible="pad" length="short" direction="pas" rot="R180"/>
<pin name="3" x="0" y="-5.08" visible="pad" length="short" direction="pas" rot="R90"/>
<polygon width="0.1524" layer="94">
<vertex x="2.54" y="2.54"/>
<vertex x="2.667" y="3.81"/>
<vertex x="1.778" y="2.921"/>
</polygon>
</symbol>
</symbols>
<devicesets>
<deviceset name="POTENTIOMETER_" urn="urn:adsk.eagle:component:23800/3" prefix="R" uservalue="yes" library_version="11">
<description>&lt;b&gt;Potentiometer&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="POTEURO" x="5.08" y="0"/>
</gates>
<devices>
<device name="PT-10" package="PT-10">
<connects>
<connect gate="1" pin="1" pad="1"/>
<connect gate="1" pin="2" pad="2"/>
<connect gate="1" pin="3" pad="3"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23786/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="15" constant="no"/>
<attribute name="SPICEMODEL" value="POT" constant="no"/>
<attribute name="SPICEPREFIX" value="X" constant="no"/>
</technology>
</technologies>
</device>
<device name="PT-10S" package="PT-10S">
<connects>
<connect gate="1" pin="1" pad="1"/>
<connect gate="1" pin="2" pad="2"/>
<connect gate="1" pin="3" pad="3"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23785/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="2" constant="no"/>
<attribute name="SPICEMODEL" value="POT" constant="no"/>
<attribute name="SPICEPREFIX" value="X" constant="no"/>
</technology>
</technologies>
</device>
<device name="PT_SPIN" package="PT-SPIN">
<connects>
<connect gate="1" pin="1" pad="1"/>
<connect gate="1" pin="2" pad="2"/>
<connect gate="1" pin="3" pad="3"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23782/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEMODEL" value="POT" constant="no"/>
<attribute name="SPICEPREFIX" value="X" constant="no"/>
</technology>
</technologies>
</device>
</devices>
<spice>
<pinmapping spiceprefix="X">
<pinmap gate="1" pin="1" pinorder="1"/>
<pinmap gate="1" pin="2" pinorder="2"/>
<pinmap gate="1" pin="3" pinorder="3"/>
</pinmapping>
<model name="POT">
********************************************
* Autodesk Eagle - Spice Model File
* Date: 9/25/17
* Variable resistor PIN1 ---/\/\/\/\--- PIN2
*                               |
*                              PIN3
********************************************
.subckt POT 1 2 3
R1 1 3 {val*var/100}
R2 3 2 {val-val*var/100}
.ends POT</model>
</spice>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="IC1" library="arduino" deviceset="ARDUINO2009_1" device="0"/>
<part name="U$2" library="keypad_4x4_raw" deviceset="KEYPAD4X4" device=""/>
<part name="DIS1" library="display-lcd" library_urn="urn:adsk.eagle:library:214" deviceset="TUXGR_20X2" device="" package3d_urn="urn:adsk.eagle:package:13138/1"/>
<part name="I2C" library="micro-philips" library_urn="urn:adsk.eagle:library:290" deviceset="PCF8574" device="P" package3d_urn="urn:adsk.eagle:package:20171/1" value="PCF8574T"/>
<part name="GND1" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="R1" library="rcl" library_urn="urn:adsk.eagle:library:334" deviceset="POTENTIOMETER_" device="PT-10" package3d_urn="urn:adsk.eagle:package:23786/1"/>
<part name="GND3" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND2" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="SUPPLY2" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="+5V" device=""/>
</parts>
<sheets>
<sheet>
<plain>
</plain>
<instances>
<instance part="IC1" gate="G$1" x="55.88" y="45.72" smashed="yes">
<attribute name="NAME" x="43.18" y="72.39" size="1.6764" layer="95"/>
</instance>
<instance part="U$2" gate="G$1" x="124.46" y="71.12" smashed="yes">
<attribute name="VALUE" x="114.3" y="86.36" size="1.27" layer="96"/>
</instance>
<instance part="DIS1" gate="G$1" x="-27.94" y="96.52" smashed="yes"/>
<instance part="I2C" gate="G$1" x="10.16" y="43.18" smashed="yes" rot="MR0">
<attribute name="NAME" x="17.78" y="59.69" size="1.778" layer="95" rot="MR0"/>
<attribute name="VALUE" x="17.78" y="22.86" size="1.778" layer="96" rot="MR0"/>
</instance>
<instance part="GND1" gate="1" x="-50.8" y="12.7" smashed="yes">
<attribute name="VALUE" x="-53.34" y="10.16" size="1.778" layer="96"/>
</instance>
<instance part="R1" gate="1" x="-48.26" y="38.1" smashed="yes" rot="R90">
<attribute name="NAME" x="-43.18" y="40.64" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="-40.64" y="40.64" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="GND3" gate="1" x="-5.08" y="10.16" smashed="yes">
<attribute name="VALUE" x="-7.62" y="7.62" size="1.778" layer="96"/>
</instance>
<instance part="GND2" gate="1" x="50.8" y="7.62" smashed="yes">
<attribute name="VALUE" x="48.26" y="5.08" size="1.778" layer="96"/>
</instance>
<instance part="SUPPLY2" gate="+5V" x="50.8" y="88.9" smashed="yes">
<attribute name="VALUE" x="48.895" y="92.075" size="1.778" layer="96"/>
</instance>
</instances>
<busses>
</busses>
<nets>
<net name="N$3" class="0">
<segment>
<pinref part="U$2" gate="G$1" pin="R1"/>
<wire x1="137.16" y1="48.26" x2="137.16" y2="81.28" width="0.1524" layer="91"/>
<wire x1="137.16" y1="81.28" x2="127" y2="81.28" width="0.1524" layer="91"/>
<pinref part="IC1" gate="G$1" pin="6"/>
<wire x1="73.66" y1="48.26" x2="137.16" y2="48.26" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$5" class="0">
<segment>
<pinref part="U$2" gate="G$1" pin="R2"/>
<wire x1="127" y1="78.74" x2="134.62" y2="78.74" width="0.1524" layer="91"/>
<wire x1="134.62" y1="78.74" x2="134.62" y2="45.72" width="0.1524" layer="91"/>
<pinref part="IC1" gate="G$1" pin="7"/>
<wire x1="134.62" y1="45.72" x2="73.66" y2="45.72" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$6" class="0">
<segment>
<pinref part="U$2" gate="G$1" pin="R3"/>
<wire x1="127" y1="76.2" x2="132.08" y2="76.2" width="0.1524" layer="91"/>
<wire x1="132.08" y1="76.2" x2="132.08" y2="40.64" width="0.1524" layer="91"/>
<pinref part="IC1" gate="G$1" pin="8"/>
<wire x1="132.08" y1="40.64" x2="73.66" y2="40.64" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$7" class="0">
<segment>
<pinref part="U$2" gate="G$1" pin="R4"/>
<wire x1="127" y1="73.66" x2="129.54" y2="73.66" width="0.1524" layer="91"/>
<wire x1="129.54" y1="73.66" x2="129.54" y2="38.1" width="0.1524" layer="91"/>
<pinref part="IC1" gate="G$1" pin="9"/>
<wire x1="129.54" y1="38.1" x2="73.66" y2="38.1" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$8" class="0">
<segment>
<pinref part="U$2" gate="G$1" pin="C4"/>
<wire x1="116.84" y1="63.5" x2="116.84" y2="50.8" width="0.1524" layer="91"/>
<pinref part="IC1" gate="G$1" pin="5"/>
<wire x1="116.84" y1="50.8" x2="73.66" y2="50.8" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$9" class="0">
<segment>
<pinref part="U$2" gate="G$1" pin="C3"/>
<wire x1="114.3" y1="63.5" x2="114.3" y2="53.34" width="0.1524" layer="91"/>
<pinref part="IC1" gate="G$1" pin="4"/>
<wire x1="114.3" y1="53.34" x2="73.66" y2="53.34" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$10" class="0">
<segment>
<pinref part="U$2" gate="G$1" pin="C2"/>
<wire x1="111.76" y1="63.5" x2="111.76" y2="55.88" width="0.1524" layer="91"/>
<pinref part="IC1" gate="G$1" pin="3"/>
<wire x1="111.76" y1="55.88" x2="73.66" y2="55.88" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$11" class="0">
<segment>
<pinref part="U$2" gate="G$1" pin="C1"/>
<wire x1="109.22" y1="63.5" x2="109.22" y2="58.42" width="0.1524" layer="91"/>
<pinref part="IC1" gate="G$1" pin="2"/>
<wire x1="109.22" y1="58.42" x2="73.66" y2="58.42" width="0.1524" layer="91"/>
</segment>
</net>
<net name="GND" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="GND@1"/>
<pinref part="GND2" gate="1" pin="GND"/>
<wire x1="50.8" y1="15.24" x2="50.8" y2="10.16" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="I2C" gate="G$1" pin="VSS"/>
<wire x1="-2.54" y1="27.94" x2="-5.08" y2="27.94" width="0.1524" layer="91"/>
<pinref part="GND3" gate="1" pin="GND"/>
<wire x1="-5.08" y1="27.94" x2="-5.08" y2="12.7" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R1" gate="1" pin="1"/>
<wire x1="-48.26" y1="33.02" x2="-48.26" y2="30.48" width="0.1524" layer="91"/>
<wire x1="-48.26" y1="30.48" x2="-50.8" y2="30.48" width="0.1524" layer="91"/>
<pinref part="GND1" gate="1" pin="GND"/>
<wire x1="-50.8" y1="30.48" x2="-50.8" y2="27.94" width="0.1524" layer="91"/>
<pinref part="DIS1" gate="G$1" pin="GND"/>
<wire x1="-50.8" y1="27.94" x2="-50.8" y2="15.24" width="0.1524" layer="91"/>
<wire x1="-50.8" y1="86.36" x2="-50.8" y2="45.72" width="0.1524" layer="91"/>
<wire x1="-50.8" y1="45.72" x2="-55.88" y2="45.72" width="0.1524" layer="91"/>
<wire x1="-55.88" y1="45.72" x2="-55.88" y2="27.94" width="0.1524" layer="91"/>
<wire x1="-55.88" y1="27.94" x2="-50.8" y2="27.94" width="0.1524" layer="91"/>
<junction x="-50.8" y="27.94"/>
</segment>
</net>
<net name="N$1" class="0">
<segment>
<pinref part="DIS1" gate="G$1" pin="D7"/>
<pinref part="I2C" gate="G$1" pin="P7"/>
<wire x1="-17.78" y1="86.36" x2="-17.78" y2="33.02" width="0.1524" layer="91"/>
<wire x1="-17.78" y1="33.02" x2="-2.54" y2="33.02" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$4" class="0">
<segment>
<pinref part="DIS1" gate="G$1" pin="D6"/>
<pinref part="I2C" gate="G$1" pin="P6"/>
<wire x1="-20.32" y1="86.36" x2="-20.32" y2="35.56" width="0.1524" layer="91"/>
<wire x1="-20.32" y1="35.56" x2="-2.54" y2="35.56" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$12" class="0">
<segment>
<pinref part="DIS1" gate="G$1" pin="D5"/>
<pinref part="I2C" gate="G$1" pin="P5"/>
<wire x1="-22.86" y1="86.36" x2="-22.86" y2="38.1" width="0.1524" layer="91"/>
<wire x1="-22.86" y1="38.1" x2="-2.54" y2="38.1" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$13" class="0">
<segment>
<pinref part="DIS1" gate="G$1" pin="D4"/>
<pinref part="I2C" gate="G$1" pin="P4"/>
<wire x1="-25.4" y1="86.36" x2="-25.4" y2="40.64" width="0.1524" layer="91"/>
<wire x1="-25.4" y1="40.64" x2="-2.54" y2="40.64" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$14" class="0">
<segment>
<pinref part="DIS1" gate="G$1" pin="NC@1"/>
<pinref part="I2C" gate="G$1" pin="P3"/>
<wire x1="-15.24" y1="86.36" x2="-15.24" y2="43.18" width="0.1524" layer="91"/>
<wire x1="-15.24" y1="43.18" x2="-2.54" y2="43.18" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$15" class="0">
<segment>
<pinref part="DIS1" gate="G$1" pin="E"/>
<pinref part="I2C" gate="G$1" pin="P2"/>
<wire x1="-38.1" y1="86.36" x2="-38.1" y2="45.72" width="0.1524" layer="91"/>
<wire x1="-38.1" y1="45.72" x2="-2.54" y2="45.72" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$16" class="0">
<segment>
<pinref part="DIS1" gate="G$1" pin="R/W"/>
<pinref part="I2C" gate="G$1" pin="P1"/>
<wire x1="-40.64" y1="86.36" x2="-40.64" y2="48.26" width="0.1524" layer="91"/>
<wire x1="-40.64" y1="48.26" x2="-2.54" y2="48.26" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$17" class="0">
<segment>
<pinref part="DIS1" gate="G$1" pin="RS"/>
<pinref part="I2C" gate="G$1" pin="P0"/>
<wire x1="-43.18" y1="86.36" x2="-43.18" y2="50.8" width="0.1524" layer="91"/>
<wire x1="-43.18" y1="50.8" x2="-2.54" y2="50.8" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$22" class="0">
<segment>
<pinref part="DIS1" gate="G$1" pin="CONTR"/>
<wire x1="-45.72" y1="86.36" x2="-45.72" y2="45.72" width="0.1524" layer="91"/>
<wire x1="-45.72" y1="45.72" x2="-40.64" y2="45.72" width="0.1524" layer="91"/>
<wire x1="-40.64" y1="45.72" x2="-40.64" y2="38.1" width="0.1524" layer="91"/>
<pinref part="R1" gate="1" pin="3"/>
<wire x1="-40.64" y1="38.1" x2="-43.18" y2="38.1" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$2" class="0">
<segment>
<pinref part="I2C" gate="G$1" pin="SCL"/>
<wire x1="20.32" y1="43.18" x2="27.94" y2="43.18" width="0.1524" layer="91"/>
<wire x1="27.94" y1="43.18" x2="27.94" y2="38.1" width="0.1524" layer="91"/>
<pinref part="IC1" gate="G$1" pin="A5"/>
<wire x1="27.94" y1="38.1" x2="38.1" y2="38.1" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$18" class="0">
<segment>
<pinref part="I2C" gate="G$1" pin="SDA"/>
<wire x1="20.32" y1="45.72" x2="30.48" y2="45.72" width="0.1524" layer="91"/>
<wire x1="30.48" y1="45.72" x2="30.48" y2="40.64" width="0.1524" layer="91"/>
<pinref part="IC1" gate="G$1" pin="A4"/>
<wire x1="30.48" y1="40.64" x2="38.1" y2="40.64" width="0.1524" layer="91"/>
</segment>
</net>
<net name="+5V" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="+5V"/>
<wire x1="50.8" y1="86.36" x2="50.8" y2="83.82" width="0.1524" layer="91"/>
<pinref part="I2C" gate="G$1" pin="VDD"/>
<wire x1="50.8" y1="83.82" x2="50.8" y2="76.2" width="0.1524" layer="91"/>
<wire x1="-2.54" y1="55.88" x2="-7.62" y2="55.88" width="0.1524" layer="91"/>
<wire x1="-7.62" y1="55.88" x2="-7.62" y2="83.82" width="0.1524" layer="91"/>
<wire x1="50.8" y1="83.82" x2="-7.62" y2="83.82" width="0.1524" layer="91"/>
<junction x="50.8" y="83.82"/>
<wire x1="-48.26" y1="55.88" x2="-7.62" y2="55.88" width="0.1524" layer="91"/>
<junction x="-7.62" y="55.88"/>
<pinref part="DIS1" gate="G$1" pin="VCC"/>
<pinref part="R1" gate="1" pin="2"/>
<wire x1="-48.26" y1="86.36" x2="-48.26" y2="55.88" width="0.1524" layer="91"/>
<wire x1="-48.26" y1="55.88" x2="-48.26" y2="43.18" width="0.1524" layer="91"/>
<junction x="-48.26" y="55.88"/>
<pinref part="SUPPLY2" gate="+5V" pin="+5V"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
<compatibility>
<note version="8.2" severity="warning">
Since Version 8.2, EAGLE supports online libraries. The ids
of those online libraries will not be understood (or retained)
with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports URNs for individual library
assets (packages, symbols, and devices). The URNs of those assets
will not be understood (or retained) with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports the association of 3D packages
with devices in libraries, schematics, and board files. Those 3D
packages will not be understood (or retained) with this version.
</note>
<note version="8.4" severity="warning">
Since Version 8.4, EAGLE supports properties for SPICE simulation. 
Probes in schematics and SPICE mapping objects found in parts and library devices
will not be understood with this version. Update EAGLE to the latest version
for full support of SPICE simulation. 
</note>
</compatibility>
</eagle>
