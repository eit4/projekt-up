#include "LiquidCrystal_I2C.h"
#include "Keypad.h"

const int ROW_NUM = 4;  //Ilość wierszy klawiatury
const int COLUMN_NUM = 4;  //Ilość kolumn klawiatury 

//Podpisanie klawiszy
char keys[ROW_NUM][COLUMN_NUM] = 
{
  {'1','2','3', '+'},
  {'4','5','6', '-'},
  {'7','8','9', '*'},
  {'C','0','=', '/'}          
};

byte pin_rows[ROW_NUM] = {9, 8, 7, 6}; //Numery pinów w arduino dla wierszy
byte pin_column[COLUMN_NUM] = {5, 4, 3, 2}; //Numery pinów w arduino dla kolumn

Keypad keypad = Keypad( makeKeymap(keys), pin_rows, pin_column, ROW_NUM, COLUMN_NUM );//Inicjalizacja klawiatury

LiquidCrystal_I2C lcd(0x27,20,4);

//Zmienne do przechowywania wartości
String input;
int counter;
long fn;
char op;
const int maxLength=5;

void setup() 
{
  //Konfiguruje napis, który ma się pojawić na początku odpalenia kalkulatora
  lcd.init();                      
  lcd.backlight();
  lcd.setCursor(0,0);
  lcd.print("PROJEKT:KALKULATOR");
  lcd.setCursor(0,1);
  lcd.print("EIT_3_ROK");
  lcd.setCursor(0,2);
  lcd.print("KRZYSZTOF");
  lcd.setCursor(0,3);
  lcd.print("GROCHOWIECKI");
  delay(3000);
  lcd.clear();
}

void loop() 
{
  char key = keypad.getKey(); //Zapisywanie wartości naciśniętego klawisza w postaci znaku
  if(key)
  {
    if(IsNumber(key))
    {
      ProcessNumber(key);
    }
    else if(IsOperator(key))
    {
      ProcessOperator(key);
    }
    else if(key == '=')
    {
      Calculate(key);
    }
    else if(key == 'C')
    {
      Reset();
    }
  }
}

void ClearScreen()// Czyszczenie ekranu
{
  lcd.setCursor(0,0);
  lcd.print("                  ");
  lcd.print("                  ");
  lcd.print("                  ");
  lcd.print("                  ");
}

void Reset()
{
  counter = 0;
  op = ' ';
  input.remove(0);
  ClearScreen();
}

void ProcessNumber(char key)
{
  if(counter >= maxLength)
  return;
  
  counter++;
  input += key;
  
  if(op == ' ')  
   lcd.setCursor(2,0);
   else
    lcd.setCursor(2,1);

   lcd.print(input);
}

void ProcessOperator(char key)
{
  if(input.length() <= 0)
  return;

  if(op != ' ')
  return;

  counter = 0;
  op = key;

  lcd.setCursor(0,1);
  lcd.print(op);

  fn = input.toInt();
  input.remove(0);
}

void Calculate(char key)
{
  if(input.length() <= 0)
  return;

  long sn = input.toInt();
  long result = DoOperation(op,fn,sn);

  lcd.setCursor(0,3);
  lcd.print("= ");
  lcd.print(result);
}

bool IsNumber(char key)
{
  if(key >= '0' && key <= '9')
   return true;

   return false;
}

bool IsOperator(char key)
{
  if(key == '+' || key == '-' || key == '*'||key == '/' )
    return true;

  return false;
}

long DoOperation(char key , long a ,long b) //Operacje matematyczne
{
 if(key == '+')
   return a + b;

 if(key == '-')
   return a - b;

  if(key == '*')
   return a * b;

  if(key == '/')
   return a / b;
}
